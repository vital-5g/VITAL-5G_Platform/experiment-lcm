![vital-5g-logo](https://www.vital5g.eu/wp-content/uploads/2020/12/vital-logo-web.png) 
 # Experiment Lifecycle Manager


[VITAL-5G D2.1](https://www.vital5g.eu/wp-content/uploads/2022/01/) 

*The Experiment Lifecycle Manager (Experiment LCM) is the VITAL-5G component responsible for the creation of experiments and the execution of test cases to experimentally evaluate the functionalities and performances of vertical services composed of Network Applications and running in the 5G-enabled testbeds. The Experiment LCM offers a REST-based API on its NBI to allow VITAL-5G users, and in particular experimenters, to request new experiments, configure and launch their execution, retrieve information on their status and results, and finally terminate them.*

## Software architecture
The following figure illustrates the software architecture of this module

![vital-5g-catalogue-software-architecture](docs/vital5g-elcm-software-architecture.png)

The source code is available in the [SRC](src/) folder of the repository. 

## Deployment 

A docker-compose based deployment is detailed in [Readme](install/)

## Folder structure
* [src](src/): Contains the source code of this module
* [API](API/): OpenAPI specification of the interfaces by this module and Postman collections

## License
This module has been developed by [Nextworks](https://www.nextworks.it) and licensed under the open source [Apache License v2.0](https://www.apache.org/licenses/LICENSE-2.0)


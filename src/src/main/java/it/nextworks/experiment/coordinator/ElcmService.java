package it.nextworks.experiment.coordinator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
//import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.elements.VsDescriptor;
import it.nextworks.catalogue.elements.exp.ExpBlueprint;
import it.nextworks.catalogue.elements.exp.ExpDescriptor;
import it.nextworks.catalogue.elements.tc.TestCaseBlueprint;
import it.nextworks.catalogue.exceptions.UnAuthorizedRequestException;
import it.nextworks.experiment.sbi.ConfiguratorService;
import it.nextworks.experiment.sbi.mano.NfvoLcmService;
import it.nextworks.lcm.vs.elements.VerticalServiceInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.nextworks.experiment.configuration.*;
import it.nextworks.experiment.manager.ExperimentExecutionInstanceManager;
import it.nextworks.experiment.model.*;
import it.nextworks.experiment.nbi.NotFoundException;
import it.nextworks.experiment.rabbit.*;
import it.nextworks.experiment.repository.ExperimentExecutionRepository;
import it.nextworks.experiment.repository.ExperimentLcmRepository;
import it.nextworks.experiment.sbi.OpenOnlineCatalogueService;
import it.nextworks.experiment.sbi.VerticalServiceLcmService;
import it.nextworks.experiment.sbi.interfaces.ConfiguratorServiceProviderInterface;
import it.nextworks.experiment.sbi.interfaces.OpenOnlineCatalogueProviderInterface;
import it.nextworks.experiment.sbi.interfaces.SiteOrchestratorProviderInterface;
import it.nextworks.experiment.sbi.interfaces.ValidatorServiceProviderInterface;
import it.nextworks.experiment.sbi.interfaces.VerticalServiceLcmProviderInterface;
import it.nextworks.experiment.sbi.mano.OsmDriver;
import it.nextworks.experiment.sbi.monitoring.MonitoringService;
import it.nextworks.experiment.sbi.rav.ResultAnalyticsDriver;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.NotExistingEntityException;
import it.nextworks.experiment.sbi.mano.NfvoLcmService;

//import it.nextworks.experiment.sbi.ELCMWsWrapper;
import it.nextworks.lcm.nfvo.osm.Osm10Client;

import javax.annotation.PostConstruct;
import javax.persistence.EntityNotFoundException;

import java.util.*;


@Service
public class ElcmService {

    private static final Logger log = LoggerFactory.getLogger(ElcmService.class);

    @Value("${spring.rabbitmq.host}")
    private String rabbitHost;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    @Qualifier(ConfigurationParameters.elcmQueueExchange)
    private TopicExchange messageExchange;

    //Key: experimentLcmID; Value: ELCMIM
    private Map<String, ExperimentExecutionInstanceManager> experimentExecutionInstances = new HashMap<>();
    
    @Autowired
    private VerticalServiceLcmProviderInterface vslcmService;
    
    @Autowired
    private OpenOnlineCatalogueProviderInterface catalogueService;
    
    @Autowired
    private SiteOrchestratorProviderInterface siteOrchestrationService;

    @Autowired
    private MonitoringService monService;

    @Autowired
    private ValidatorServiceProviderInterface validationService;

    @Autowired
    private ConfiguratorService configuratorService;
    
    @Autowired
    private NfvoLcmService nfvoLcmService;

    @Autowired
    private ExperimentLcmRepository experimentLcmRepository;

    @Autowired
    private ExperimentExecutionRepository experimentExecutionRepository;

    //private ELCMWsWrapper experimentWebsocketWrapper;

//    @Autowired
//    private EemSubscriptionService subscriptionService;

//    @Value("${eem.jenkins.validation.url}")
//    private String validationBaseUrl;

    @PostConstruct
    private void initStoredExperimentLcm() throws FailedOperationException{
        //Loads Experiment Executions stored and initializes the corresponding EEIM
        //List<ExperimentExecution> experimentExecutions = experimentExecutionRepository.findAll();
        //for(ExperimentExecution experimentExecution : experimentExecutions)
        //    if(!experimentExecution.getState().equals(ExecutionStatus.TERMINATED) && !experimentExecution.getState().equals(ExecutionStatus.REJECTED) && !experimentExecution.getState().equals(ExecutionStatus.FAILED)){
        //        initNewExperimentExecutionInstanceManager(experimentExecution.getExecutionId(), experimentExecution.getExperimentLcmId());
        //        log.info("Experiment Execution with Id {} restored in state {}", experimentExecution.getExecutionId(), experimentExecution.getState());
        //        log.debug("{}", experimentExecution.toString());
        //    }
    }

    // GET LIST OF Experiments 
    // Filtering only by status and vsId
    public List<ExperimentLcm> getExperimentLcmsInstances(ExperimentStatus status, Testbed testbed, String vsId, String user, String useCase){
        log.info("Received request for getting Experiment LCM list");
       	return filterExperimentLcmList(status, testbed, vsId, user, useCase);
    }

    // POST NEW EXPERIMENT REQUEST
    public synchronized String createExperimentLcmInstance(ExperimentInstanceRequest experimentInstanceRequest) throws FailedOperationException, EntityNotFoundException {
    	log.info("Received request for new Experiment LCM");
    	//@TODO: Check if vsiId and ExpDescriptor exist
       VerticalServiceInstance verticalServiceInstance = vslcmService.getVerticalServiceInstance(experimentInstanceRequest.getVerticalServiceInstanceId());
    	if(verticalServiceInstance == null) {
    		log.debug("VSI with id {} does not exist", experimentInstanceRequest.getVerticalServiceInstanceId());
    		throw new EntityNotFoundException(String.format("Vertical service instance with Id %s does not exist", experimentInstanceRequest.getVerticalServiceInstanceId()));
    	}
    	//Check if vsId is already in use
    	List<ExperimentLcm> expLcmOptList = experimentLcmRepository.findByVsId(experimentInstanceRequest.getVerticalServiceInstanceId());
    	for(ExperimentLcm exLcm: expLcmOptList){
    		if(exLcm.getStatus().equals(ExperimentStatus.LOCKED) || exLcm.getStatus().equals(ExperimentStatus.CREATED)) {
    		log.error("CONFLICT: Experiment LCM for vsiId {} already exists", experimentInstanceRequest.getVerticalServiceInstanceId());
    		throw new FailedOperationException(String.format("Experiment LCM for vertical service instance with Id %s already exists", experimentInstanceRequest.getVerticalServiceInstanceId()));
    		}
    	} 

        ExperimentLcm expLcm = new ExperimentLcm();
        expLcm.setTestbed(Testbed.fromValue(verticalServiceInstance.getTestbed().toString()));
        expLcm.setNetworkServiceInstanceId(verticalServiceInstance.getNetworkServiceInstanceId());
        String elcmId = UUID.randomUUID().toString();
        log.info("New ExperimentLCM with elcmId {} created for vertical service {}", elcmId, experimentInstanceRequest.getVerticalServiceInstanceId());
        expLcm.elcmId(elcmId).status(ExperimentStatus.CREATED)
                             .vsId(experimentInstanceRequest.getVerticalServiceInstanceId())
                             .experimentDescriptorId(experimentInstanceRequest.getExperimentDescriptorId())
                             .experimentConfigurationParameters(experimentInstanceRequest.getExperimentConfigurationParameters());
        experimentLcmRepository.saveAndFlush(expLcm);
        //@TODO NOTIFY VERTICAL SERVICE
        log.info("Locking VSI with id  {} for experiment with id {}", elcmId, experimentInstanceRequest.getVerticalServiceInstanceId());
        vslcmService.useVerticalServiceInstance(elcmId, experimentInstanceRequest.getVerticalServiceInstanceId());

        return elcmId;
    }
    
    // GET SINGLE EXPERIMENT LCM identified by ID
    public ExperimentLcm getExperimentLcmInstance(String elcmId) throws NotExistingEntityException {
		log.info("Received request for getting Experiment LCM  with Id {}", elcmId);
		Optional<ExperimentLcm> experimentLcmOptional = experimentLcmRepository.findByElcmId(elcmId);
		if(!experimentLcmOptional.isPresent())
			throw new NotExistingEntityException(String.format("Experiment LCM with Id %s not found", elcmId));
		log.debug("{}", experimentLcmOptional.get().toString());
		return experimentLcmOptional.get();
    }
    
    //TERMINATE experiment lcm identified by ID
    public void terminateExperimentLcmInstance(String elcmId, Boolean Force) throws NotExistingEntityException, FailedOperationException {
    	// If elcmId not exists, NotExistingEntityException
    	Optional<ExperimentLcm> expLcmopt = experimentLcmRepository.findByElcmId(elcmId);
    	if (!expLcmopt.isPresent()) {
    		log.error("Experiment LCM with Id {} not exists", elcmId);
    		throw new NotExistingEntityException(String.format("Experiment LCM with Id %s not exists", elcmId));
    	}
    	ExperimentLcm expLcm = expLcmopt.get();
        if (Force) {
            log.info("Force flag set to TRUE");
        } else {
            log.info("Force flag set to FALSE");
        }
    	if (expLcm.getStatus() == ExperimentStatus.LOCKED && !Force) {
    		log.error("Experiment LCM with Id {} is in LOCKED stauts and cannot be terminated until the experiment execution does", elcmId);
    		throw new FailedOperationException(String.format("Experiment LCM with Id %s is in LOCKED stauts and cannot be terminated until the experiment execution does", elcmId));
    	}
   		expLcm.status(ExperimentStatus.TERMINATED);
   		experimentLcmRepository.saveAndFlush(expLcm);
   		//@TODO NOTIFY VERTICAL SERVICE
   		log.info("Releasing VSI with id  {} for experiment with id {}", elcmId, expLcm.getVsId());
    	vslcmService.releaseVerticalServiceInstance(elcmId, expLcm.getVsId());
    }

    //DELETE experiment lcm identified by ID
    public void deleteExperimentLcmInstance(String elcmId) throws NotExistingEntityException, FailedOperationException {
    	// If elcmId not exists, NotExistingEntityException
    	Optional<ExperimentLcm> expLcmopt = experimentLcmRepository.findByElcmId(elcmId);
    	if (!expLcmopt.isPresent()) {
    		log.error("Experiment LCM with Id {} not exists", elcmId);
    		throw new NotExistingEntityException(String.format("Experiment LCM with Id %s not exists", elcmId));
    	}
    	ExperimentLcm expLcm = expLcmopt.get();
    	if (expLcm.getStatus() != ExperimentStatus.TERMINATED) {
    		log.error("Experiment LCM with Id {} is not in TERMINATED stauts and cannot be delete until the experiment execution does", elcmId);
    		throw new FailedOperationException(String.format("Experiment LCM with Id %s is not in TERMINATED stauts and cannot be delete until the experiment execution does", elcmId));
    	}
        experimentLcmRepository.delete(expLcm);
        log.info("Deleted Exp {}", expLcm);
   		//expLcm.status(ExperimentStatus.TERMINATED);
   		//experimentLcmRepository.saveAndFlush(expLcm);
   		////@TODO NOTIFY VERTICAL SERVICE
   		//log.info("Releasing VSI with id  {} for experiment with id {}", elcmId, expLcm.getVsId());
    	//vslcmService.releaseVerticalServiceInstance(elcmId, expLcm.getVsId());
    }
    
    // GET LIST OF EXPERIMENT EXECUTIONS FOR A GIVEN EXPERIMENT LCM
    public List<ExperimentExecution> getExperimentExecutions(String elcmId) throws NotExistingEntityException{
        log.info("Received request for getting Experiment Executions list");
        Optional<ExperimentLcm> expLcmopt = experimentLcmRepository.findByElcmId(elcmId);
        if(!expLcmopt.isPresent()) {
        	throw new NotExistingEntityException(String.format("Experiment LCM with Id %s does not exist", elcmId));
        }
        Optional<List<ExperimentExecution>> experimentExecutionsOpt = experimentExecutionRepository.findByExperimentLcmId(elcmId);
        return experimentExecutionsOpt.orElseGet(ArrayList::new);
    }
    
    // CREATE NEW EXPERIMENT EXECUTION FOR GIVEN EXPERIMENT LCM
    public synchronized String createExperimentExecutionInstance(String elcmId, ExperimentExecutionRequest request) throws FailedOperationException, NotExistingEntityException{
        log.info("Received request for new Experiment Execution for Experiment LCM {}", elcmId);
    	String executionId = UUID.randomUUID().toString();
        ExperimentExecution experimentExecution = new ExperimentExecution();

        if(request.getExperimentConfigurationParameters() != null)
        	experimentExecution.configurationParameters(request.getExperimentConfigurationParameters());
        //check if elcm exists
        Optional<ExperimentLcm> expLcmOpt = experimentLcmRepository.findByElcmId(elcmId);

        if(!expLcmOpt.isPresent()) {
        	log.error("Experiment LCM identified by {} not found", elcmId);
        	throw new NotExistingEntityException(String.format("Experiment LCM with Id %s not exists", elcmId));
        } else {
        	ExperimentLcm expLcm = expLcmOpt.get();
            experimentExecution.setTestbed(expLcm.getTestbed());
            switch(expLcm.getStatus()) {
            case LOCKED: case REJECTED: case CREATING: case TERMINATED: {
            	log.error("Experiment status is in {} mode and cannot start a new experiment execution", expLcm.getStatus().toString());
            	throw new FailedOperationException(String.format("Experiment status is in {} mode and cannot start a new experiment execution", expLcm.getStatus().toString()));
            }
            case CREATED: {
            	log.info("Experiment is in {} mode. New execution with id {} is created", expLcm.getStatus().toString(), executionId);
                experimentExecution.diagnostic(request.isDiagnostic()).duration(request.getDuration()).experimentLcmId(elcmId);
            	experimentExecution.executionId(executionId).state(ExecutionStatus.CREATED).executionName(request.getExecutionName());
            	break;
            } default: {
            	log.error("Experiment is in {} mode. This status for the experiment is not supported", expLcm.getStatus().toString());
            	throw new FailedOperationException(String.format("Experiment is in {} mode. This status for the experiment is not supported", expLcm.getStatus().toString()));
            }
            
            }
        	expLcm.addExecutionsItem(experimentExecution);
            experimentExecutionRepository.saveAndFlush(experimentExecution);
            experimentLcmRepository.saveAndFlush(expLcm);
            initNewExperimentExecutionInstanceManager(executionId, elcmId);
            log.info("Experiment Execution with Id {} created and stored", experimentExecution.getExecutionId());

            log.info("Running Experiment Execution with Id {}", executionId);

            String topic = "lifecycle.configure." + executionId;
            InternalMessage internalMessage = new ConfigureExperimentInternalMessage();;
            try {
                sendMessageToQueue(internalMessage, topic);
            } catch (JsonProcessingException e) {
                log.error("Error while translating internal scheduling message in Json format");
                throw new FailedOperationException("Internal error with queues");
            }
            
        }
        log.debug("{}", experimentExecution.toString());
        return executionId;
    }

    // GET EXPERIMENT EXEC
    public ExperimentExecution getExperimentExecutionInstance(String elcmId, String executionId) throws NotExistingEntityException {
    	log.info("Received request for quering Experiment Execution with Id {}", executionId);
    	Optional<ExperimentLcm> expLcmopt = experimentLcmRepository.findByElcmId(elcmId);
    	if(!expLcmopt.isPresent()) {
    		throw new NotExistingEntityException(String.format("Experiment LCM with Id %s is not found", elcmId));
    	}
    	Optional<ExperimentExecution> execution = experimentExecutionRepository.findByExecutionId(executionId);
    	if(!execution.isPresent() || !execution.get().getExperimentLcmId().equals(elcmId)) {
    		throw new NotExistingEntityException(String.format("Experiment Execution with Id %s is not found for experiment LCM {} {}", executionId, elcmId));
    	}
    	return execution.get();
    }
    // RUN NEW EXPERIMENT
    public synchronized void runExperimentExecution(String executionId) throws FailedOperationException, NotExistingEntityException, MalformattedElementException {
      log.info("Received request for running Experiment Execution with Id {}, nothing to do", executionId);
      // find ExperimentExecution
      //Optional<ExperimentExecution> experimentExecutionOptional = experimentExecutionRepository.findByExecutionId(executionId);
      //if(!experimentExecutionOptional.isPresent())
      //    throw new NotExistingEntityException(String.format("Experiment Execution with Id %s not found", executionId));
      //ExperimentExecution experimentExecution = experimentExecutionOptional.get();
      //// If status is not INIT, experiment is already running
      //if(!experimentExecution.getState().equals(ExecutionStatus.CREATED) && !experimentExecution.getState().equals(ExecutionStatus.TERMINATED))
      //    throw new FailedOperationException(String.format("Experiment Execution with Id %s is not in INIT state", executionId));
      //  //TODO: how to define this???? Is this needed????
      //  //      experimentExecutionInstances.get(executionId).
      //String topic = "lifecycle.configure." + executionId;
      //InternalMessage internalMessage = new ConfigureExperimentInternalMessage();;
      //try {
      //    sendMessageToQueue(internalMessage, topic);
      //} catch (JsonProcessingException e) {
      //    log.error("Error while translating internal scheduling message in Json format");
      //    throw new FailedOperationException("Internal error with queues");
      //}
  }
       
    // STOP EXPERIMENT EXECUTION
	public synchronized void stopExperimentExecution(String executionId) throws FailedOperationException, NotExistingEntityException{
	  log.info("Received request for aborting Experiment Execution with Id {}", executionId);
	  Optional<ExperimentExecution> experimentExecutionOptional = experimentExecutionRepository.findByExecutionId(executionId);
	  if(!experimentExecutionOptional.isPresent())
	      throw new NotExistingEntityException(String.format("Experiment Execution with Id %s not found", executionId));
	  ExperimentExecution experimentExecution = experimentExecutionOptional.get();
	  if(!experimentExecution.getState().equals(ExecutionStatus.RUNNING))
	      throw new FailedOperationException(String.format("Experiment Execution with Id %s is not in RUNNING state", executionId));
	
	  String topic = "lifecycle.stop." + executionId;
	  InternalMessage internalMessage = new StopExperimentInternalMessage();
	  try {
	      sendMessageToQueue(internalMessage, topic);
	  } catch (JsonProcessingException e) {
	      log.error("Error while translating internal scheduling message in Json format");
	      throw new FailedOperationException("Internal error with queues");
	  }
	}

    public void ExperimentExecutionCompleted(String executionId) throws NotExistingEntityException, FailedOperationException, MalformattedElementException {
        log.info("Received request for aborting Experiment Execution with Id {}", executionId);
        Optional<ExperimentExecution> experimentExecutionOptional = experimentExecutionRepository.findByExecutionId(executionId);
        if(!experimentExecutionOptional.isPresent())
            throw new NotExistingEntityException(String.format("Experiment Execution with Id %s not found", executionId));
        ExperimentExecution experimentExecution = experimentExecutionOptional.get();
        if(!experimentExecution.getState().equals(ExecutionStatus.RUNNING_MANUAL))
            throw new FailedOperationException(String.format("Experiment Execution with Id %s is not in RUNNING_MANUAL state", executionId));

        String topic = "lifecycle.executionresult." + executionId;
        InternalMessage internalMessage = new ExecutionResultInternalMessage();
        try {
            sendMessageToQueue(internalMessage, topic);
        } catch (JsonProcessingException e) {
            log.error("Error while translating internal scheduling message in Json format");
            throw new FailedOperationException("Internal error with queues");
        }

    }




//    public synchronized void abortExperimentExecution(String experimentExecutionId) throws FailedOperationException, NotExistingEntityException{
//        log.info("Received request for aborting Experiment Execution with Id {}", experimentExecutionId);
//        Optional<ExperimentExecution> experimentExecutionOptional = experimentExecutionRepository.findByExecutionId(experimentExecutionId);
//        if(!experimentExecutionOptional.isPresent())
//            throw new NotExistingEntityException(String.format("Experiment Execution with Id %s not found", experimentExecutionId));
//        ExperimentExecution experimentExecution = experimentExecutionOptional.get();
//        if(!experimentExecution.getState().equals(ExperimentState.RUNNING) && !experimentExecution.getState().equals(ExperimentState.RUNNING_STEP) && !experimentExecution.getState().equals(ExperimentState.PAUSED))
//            throw new FailedOperationException(String.format("Experiment Execution with Id %s is neither in RUNNING or RUNNING_STEP or PAUSED state", experimentExecutionId));
//
//        String topic = "lifecycle.abort." + experimentExecutionId;
//        InternalMessage internalMessage = new AbortExperimentInternalMessage();
//        try {
//            sendMessageToQueue(internalMessage, topic);
//        } catch (JsonProcessingException e) {
//            log.error("Error while translating internal scheduling message in Json format");
//            throw new FailedOperationException("Internal error with queues");
//        }
//    }
//
//
//    public synchronized void pauseExperimentExecution(String experimentExecutionId) throws FailedOperationException, NotExistingEntityException{
//        log.info("Received request for pausing Experiment Execution with Id {}", experimentExecutionId);
//        Optional<ExperimentExecution> experimentExecutionOptional = experimentExecutionRepository.findByExecutionId(experimentExecutionId);
//        if(!experimentExecutionOptional.isPresent())
//            throw new NotExistingEntityException(String.format("Experiment Execution with Id %s not found", experimentExecutionId));
//        ExperimentExecution experimentExecution = experimentExecutionOptional.get();
//        if(!experimentExecution.getState().equals(ExperimentState.RUNNING))
//            throw new FailedOperationException(String.format("Experiment Execution with Id %s is not in RUNNING state", experimentExecutionId));
//
//        String topic = "lifecycle.pause." + experimentExecutionId;
//        InternalMessage internalMessage = new PauseExperimentInternalMessage();
//        try {
//            sendMessageToQueue(internalMessage, topic);
//        } catch (JsonProcessingException e) {
//            log.error("Error while translating internal scheduling message in Json format");
//            throw new FailedOperationException("Internal error with queues");
//        }
//    }
//
//    public synchronized void stepExperimentExecution(String experimentExecutionId) throws FailedOperationException, NotExistingEntityException{
//        log.info("Received request for performing a step of the Experiment Execution with Id {}", experimentExecutionId);
//        Optional<ExperimentExecution> experimentExecutionOptional = experimentExecutionRepository.findByExecutionId(experimentExecutionId);
//        if(!experimentExecutionOptional.isPresent())
//            throw new NotExistingEntityException(String.format("Experiment Execution with Id %s not found", experimentExecutionId));
//        ExperimentExecution experimentExecution = experimentExecutionOptional.get();
//        if(!experimentExecution.getState().equals(ExperimentState.PAUSED))
//            throw new FailedOperationException(String.format("Experiment Execution with Id %s is not in PAUSED state", experimentExecutionId));
//
//        String topic = "lifecycle.step." + experimentExecutionId;
//        InternalMessage internalMessage = new StepExperimentInternalMessage();
//        try {
//            sendMessageToQueue(internalMessage, topic);
//        } catch (JsonProcessingException e) {
//            log.error("Error while translating internal scheduling message in Json format");
//            throw new FailedOperationException("Internal error with queues");
//        }
//    }
//
//    public List<ExperimentExecutionSubscription> getExperimentExecutionSubscriptions() throws FailedOperationException{
//        log.info("Received request for getting Experiment Execution Subscription list");
//        return subscriptionService.getExperimentExecutionSubscriptions();
//    }
//
//    public ExperimentExecutionSubscription getExperimentExecutionSubscription(String subscriptionId) throws FailedOperationException, NotExistingEntityException{
//        log.info("Received request for getting Experiment Execution Subscription with Id {}", subscriptionId);
//        return subscriptionService.getExperimentExecutionSubscription(subscriptionId);
//    }
//
//    public synchronized String subscribe(ExperimentExecutionSubscriptionRequest subscriptionRequest) throws FailedOperationException, NotExistingEntityException, MalformattedElementException{
//        subscriptionRequest.isValid();
//        String executionId = subscriptionRequest.getExecutionId();
//        if(executionId.equals("*")){
//            log.info("Received subscribe request to all Experiment Executions");
//        }else {
//            log.info("Received subscribe request to Experiment Execution with Id {}", executionId);
//            Optional<ExperimentExecution> experimentExecution = experimentExecutionRepository.findByExecutionId(executionId);
//            if (!experimentExecution.isPresent())
//                throw new NotExistingEntityException(String.format("Experiment Execution with Id %s not found", executionId));
//        }
//        return subscriptionService.subscribe(subscriptionRequest);
//    }
//
//    public synchronized void unsubscribe(String subscriptionId) throws FailedOperationException, NotExistingEntityException{
//        log.info("Received unsubscribe request to Experiment Execution Subscription with Id {}", subscriptionId);
//        subscriptionService.unsubscribe(subscriptionId);
//    }

    private void initNewExperimentExecutionInstanceManager(String executionId, String experimentId) throws FailedOperationException {
        log.info("Initializing new Experiment Execution Instance Manager with Id {} for experiment lcm {}", executionId, experimentId);
        Optional<ExperimentLcm> elcmExp = experimentLcmRepository.findByElcmId(experimentId);
        VerticalServiceInstance vsi = vslcmService.getVerticalServiceInstance(elcmExp.get().getVsId());
        if (vsi == null) {
            throw new FailedOperationException(String.format("Vertical Service Instance with Id %s not found", elcmExp.get().getVsId()));
        }
        log.debug("Trying to get VSD_ID {}", vsi.getVsdId());
        // Get VSD_ID
        VsDescriptor vsd = null;
        try {
            vsd = catalogueService.getVsd(UUID.fromString(vsi.getVsdId().toString()));
        } catch (it.nextworks.catalogue.exceptions.NotExistingEntityException |
                 it.nextworks.catalogue.exceptions.FailedOperationException |
                 it.nextworks.catalogue.exceptions.MalformattedElementException | UnAuthorizedRequestException e) {
            log.error("Error retrieving VSD " + e);
            throw new FailedOperationException(e);
        }
        if (vsd == null) {
            throw new FailedOperationException(String.format("Vertical Service Instance with Id %s not found", elcmExp.get().getVsId()));
        }
        // GET EXPD
        ExpDescriptor expd = null;
        try {
            expd = catalogueService.getExpDescriptor(UUID.fromString(elcmExp.get().getExperimentDescriptorId()));
        }catch (it.nextworks.catalogue.exceptions.FailedOperationException |
                it.nextworks.catalogue.exceptions.MalformattedElementException |
                it.nextworks.catalogue.exceptions.NotExistingEntityException e) {
            log.error("Error retrieving EDPD " + e);
            throw new FailedOperationException(e);
        }
        if (expd == null ){
            throw new FailedOperationException(String.format("Experiment Descriptor with Id %s not found", elcmExp.get().getExperimentDescriptorId()));
        }
        // GET EXPB
        log.debug("Trying to get EXPB {}", expd.getExpBlueprintId());
        ExpBlueprint expb = null;
        try {
            expb = catalogueService.getExperimentBlueprint(UUID.fromString(expd.getExpBlueprintId().toString()));
        }catch (
                it.nextworks.catalogue.exceptions.NotExistingEntityException |
                it.nextworks.catalogue.exceptions.MalformattedElementException e) {
            log.error("Error retrieving EXPB " + e);
            throw new FailedOperationException(e);
        }
        if (expb == null )
            throw new FailedOperationException(String.format("Experiment Blueprint with Id %s not found", expd.getExpBlueprintId().toString()));

        // GET TCB from EXPB
        log.debug("Trying to get TCB {}", expb.getTcBlueprintId());
        TestCaseBlueprint tcb = null;
        try {
            tcb = catalogueService.getTestCaseBlueprint(UUID.fromString(expb.getTcBlueprintId().toString()));
        } catch (
                 it.nextworks.catalogue.exceptions.NotExistingEntityException |
                 it.nextworks.catalogue.exceptions.UnAuthorizedRequestException |
                 it.nextworks.catalogue.exceptions.MalformattedElementException e) {
            log.error("Error retrieving TCB " + e);
            throw new FailedOperationException(e);
        }
        if (tcb == null )
            throw new FailedOperationException(String.format("Testcase Blueprint with Id %s not found", expb.getTcBlueprintId().toString()));

        ExperimentExecutionInstanceManager eeim;
        try {
            eeim = new ExperimentExecutionInstanceManager(
            												executionId, 
            												experimentId, 
            												vslcmService, 
            												catalogueService,
            												siteOrchestrationService,
            												monService,
            												validationService, 
            												configuratorService, 
            												experimentExecutionRepository,
                                                            nfvoLcmService,
            												experimentLcmRepository,
                                                            vsi,
                                                            vsd,
                                                            expd,
                                                            expb,
                                                            tcb);
        }catch (NotExistingEntityException e) {
            throw new FailedOperationException(String.format("Initialization of Experiment Execution Instance Manager with Id %s failed : %s", executionId, e.getMessage()));
        }
        createQueue(executionId, eeim);
        experimentExecutionInstances.put(executionId, eeim);
        log.debug("Experiment Execution Instance Manager with Id {} initialized", executionId);
    }

    private void createQueue(String experimentExecutionId, ExperimentExecutionInstanceManager eeim) {
        String queueName = ConfigurationParameters.elcmQueueInNamePrefix + experimentExecutionId;
        log.debug("Creating new Queue " + queueName + " in rabbit host " + rabbitHost);
        CachingConnectionFactory cf = new CachingConnectionFactory();
        cf.setAddresses(rabbitHost);
        cf.setConnectionTimeout(5);
        RabbitAdmin rabbitAdmin = new RabbitAdmin(cf);
        Queue queue = new Queue(queueName, false, false, true);
        rabbitAdmin.declareQueue(queue);
        rabbitAdmin.declareExchange(messageExchange);
        rabbitAdmin.declareBinding(BindingBuilder.bind(queue).to(messageExchange).with("lifecycle.*." + experimentExecutionId));
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(cf);
        MessageListenerAdapter adapter = new MessageListenerAdapter(eeim, "receiveMessage");
        container.setMessageListener(adapter);
        container.setQueueNames(queueName);
        container.start();
        log.debug("Queue created");
    }

    private void sendMessageToQueue(InternalMessage msg, String topic) throws JsonProcessingException {
        ObjectMapper mapper = buildObjectMapper();
        String json = mapper.writeValueAsString(msg);
        rabbitTemplate.convertAndSend(messageExchange.getName(), topic, json);
    }

    private ObjectMapper buildObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }
    
    private List<ExperimentLcm> filterExperimentLcmList(ExperimentStatus status, Testbed testbed, String vsId, String user, String useCase){
    	List<ExperimentLcm> expLcmList = experimentLcmRepository.findAll();
    	if (status != null) {
    		List<ExperimentLcm> tmp = new ArrayList<>();
    		for (ExperimentLcm expLcm : expLcmList) {
    			if (status == expLcm.getStatus()) 
    				tmp.add(expLcm);
    		}
    		expLcmList = tmp;
    	}
    	if (vsId != null) {
    		List<ExperimentLcm> tmp = new ArrayList<>();
    		for (ExperimentLcm expLcm : expLcmList) {
    			if (vsId == expLcm.getVsId()) 
    				tmp.add(expLcm);
    		}
    		expLcmList = tmp;
    	}
    	if (testbed != null)
    		log.info("Testbed filtering is not available yet");	
    	if (user != null)
    		log.info("User filtering is not available yet");	
    	if (useCase != null)
    		log.info("UseCase filtering is not available yet");	
    	return expLcmList;
    }


}

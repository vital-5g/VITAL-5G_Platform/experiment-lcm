package it.nextworks.experiment.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets ExperimentStatus
 */
public enum ExperimentStatus {
    CREATING("CREATING"),
    CREATED("CREATED"),
    LOCKED("LOCKED"),
    REJECTED("REJECTED"),
    TERMINATED("TERMINATED");

  private String value;

  ExperimentStatus(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static ExperimentStatus fromValue(String text) {
    for (ExperimentStatus b : ExperimentStatus.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

package it.nextworks.experiment.model;

import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets ExecutionStatus
 */
public enum ExecutionStatus {
	CREATED("CREATED"),
    CONFIGURING("CONFIGURING"),
    RUNNING("RUNNING"),

    RUNNING_MANUAL("RUNNING_MANUAL"),
    VALIDATING("VALIDATING"),
    CLEANING("CLEANING"),
    ROLLING_BACK("ROLLING_BACK"),
    REJECTED("REJECTED"),
    //TERMINATING("TERMINATING"),
    TERMINATED("TERMINATED"),
    FAILED("FAILED");

  private String value;

  ExecutionStatus(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static ExecutionStatus fromValue(String text) {
    for (ExecutionStatus b : ExecutionStatus.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

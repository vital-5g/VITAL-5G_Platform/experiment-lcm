package it.nextworks.experiment.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.v3.oas.annotations.media.Schema;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.validation.annotation.Validated;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ExperimentExecution
 */
@Entity
public class ExperimentExecution   {
	
  @Id
  @GeneratedValue
  @JsonIgnore
  private UUID id;
	  
  @JsonIgnore
  @ManyToOne
  private ExperimentLcm experiment;

  private String currentOperationId;

  public void setCurrentOperationId(String currentOperationId) {
    this.currentOperationId = currentOperationId;
  }

  public String getCurrentOperationId() {
    return currentOperationId;
  }

  private Testbed testbed;

  public void setTestbed(Testbed testbed) {
    this.testbed = testbed;
  }

  //  private ExecutionResult executionResult;


  
  public UUID getId() {
	return id;
}


public void setId(UUID id) {
	this.id = id;
}


public ExperimentLcm getExperiment() {
	return experiment;
}


public void setExperiment(ExperimentLcm experiment) {
	this.experiment = experiment;
}


//
//public ExecutionResult getExecutionResult() {
//	return executionResult;
//}
//
//
//public void setExecutionResult(ExecutionResult executionResult) {
//	this.executionResult = executionResult;
//}


public void setExecutionId(String executionId) {
	this.executionId = executionId;
}

  @JsonProperty("state")
  private ExecutionStatus state = null;
  
  @JsonProperty("experimentLcmId")
  private String experimentLcmId = null;

  @JsonProperty("executionId")
  private String executionId = null;

  @JsonProperty("executionName")
  private String executionName = null;
  
  @JsonProperty("duration")
  private Integer duration = null;
  
  @JsonProperty("diagnostic")
  private Boolean diagnostic = null;

  @JsonProperty("configurationParameters")
  private KeyValuePair configurationParameters = new KeyValuePair();

  @JsonProperty("reportUrl")
  private String reportUrl = null;

	@JsonIgnore
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.REMOVE)
	@JoinColumn(name="execution_result")
	@OnDelete(action = OnDeleteAction.CASCADE)
  private ExecutionResult result = null;

  @JsonProperty("monitoring")
  private String monitoring = null;
  
  @JsonInclude(JsonInclude.Include.NON_NULL)
  @JsonProperty("errorMessage")
  private String errorMessage;

  
  public ExperimentExecution state(ExecutionStatus state) {
    this.state = state;
    return this;
  }
 
  
  /**
   * Get state
   * @return state
   **/
  @Schema(required = true, description = "")
      @NotNull

    @Valid
    public ExecutionStatus getState() {
    return state;
  }

  public void setState(ExecutionStatus state) {
    this.state = state;
  }

  public ExperimentExecution executionId(String executionId) {
    this.executionId = executionId;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getExecutionId() {
    return executionId;
  }

  public void setId(String executionId) {
    this.executionId = executionId;
  }

  public ExperimentExecution executionName(String executionName) {
    this.executionName = executionName;
    return this;
  }
  

  public Integer getDuration() {
	  return this.duration;
  }
  
  public ExperimentExecution duration(Integer duration) {
	  this.duration = duration;
	  return this;
  }
  
  public void setDuration(Integer duration) {
	  this.duration = duration;
  }

  public Boolean getDiagnostic() {
	  return this.diagnostic;
  }
  
  
  public ExperimentExecution diagnostic (Boolean diagnostic) {
	  this.diagnostic = diagnostic;
	  return this;
  }
  
  public void setDiagnostic(Boolean diagnostic) {
	  this.diagnostic = diagnostic;
  }
  /**
   * Get executionName
   * @return executionName
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getExecutionName() {
    return executionName;
  }

  public void setExecutionName(String executionName) {
    this.executionName = executionName;
  }

  public ExperimentExecution configurationParameters(KeyValuePair configurationParameters) {
    this.configurationParameters = configurationParameters;
    return this;
  }

  /**
   * Get configurationParameters
   * @return configurationParameters
   **/
  @Schema(description = "")
  
    @Valid
    public KeyValuePair getConfigurationParameters() {
    return configurationParameters;
  }

  public void setConfigurationParameters(KeyValuePair configurationParameters) {
    this.configurationParameters = configurationParameters;
  }

  public ExperimentExecution reportUrl(String reportUrl) {
    this.reportUrl = reportUrl;
    return this;
  }

  /**
   * URL containing the report of the execution
   * @return reportUrl
   **/
  @Schema(description = "URL containing the report of the execution")
  
    public String getReportUrl() {
    return reportUrl;
  }

  public void setReportUrl(String reportUrl) {
    this.reportUrl = reportUrl;
  }

  public ExperimentExecution result(ExecutionResult result) {
    this.result = result;
    return this;
  }

  /**
   * Get result
   * @return result
   **/
  @Schema(description = "")
  
    @Valid
    public ExecutionResult getResult() {
    return result;
  }

  public void setResult(ExecutionResult result) {
    this.result = result;
  }

  public ExperimentExecution monitoring(String monitoring) {
    this.monitoring = monitoring;
    return this;
  }

  /**
   * URL containing the monitoring of the execution
   * @return monitoring
   **/
  @Schema(description = "URL containing the monitoring of the execution")
  
    public String getMonitoring() {
    return monitoring;
  }

  public void setMonitoring(String monitoring) {
    this.monitoring = monitoring;
  }

  public ExperimentExecution experimentLcmId(String id) {
	  this.experimentLcmId = id;
	  return this;
  }
  
  public String getExperimentLcmId() {
	  return this.experimentLcmId;
  }
  
  public void setExperimentLcmId(String id) {
	  this.experimentLcmId = id;
  }
  
  public ExperimentExecution errorMessage(String errorMessage) {
	  this.errorMessage = errorMessage;
	  return this;
  }
  
  public String getErrorMessage() {
	  return this.errorMessage;
  }
  
  public void setErrorMessage(String errorMessage) {
	  this.errorMessage = errorMessage;
  }
  

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ExperimentExecution experimentExecution = (ExperimentExecution) o;
    return Objects.equals(this.state, experimentExecution.state) &&
        Objects.equals(this.executionId, experimentExecution.executionId) &&
        Objects.equals(this.executionName, experimentExecution.executionName) &&
        Objects.equals(this.configurationParameters, experimentExecution.configurationParameters) &&
        Objects.equals(this.reportUrl, experimentExecution.reportUrl) &&
        Objects.equals(this.result, experimentExecution.result) &&
        Objects.equals(this.monitoring, experimentExecution.monitoring);
  }

  @Override
  public int hashCode() {
    return Objects.hash(state, executionId, executionName, configurationParameters, reportUrl, result, monitoring);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ExperimentExecution {\n");
    
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    executionId: ").append(toIndentedString(executionId)).append("\n");
    sb.append("    executionName: ").append(toIndentedString(executionName)).append("\n");
    sb.append("    configurationParameters: ").append(toIndentedString(configurationParameters)).append("\n");
    sb.append("    reportUrl: ").append(toIndentedString(reportUrl)).append("\n");
    sb.append("    result: ").append(toIndentedString(result)).append("\n");
    sb.append("    monitoring: ").append(toIndentedString(monitoring)).append("\n");
    sb.append("    errorMessage: ").append(toIndentedString(errorMessage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

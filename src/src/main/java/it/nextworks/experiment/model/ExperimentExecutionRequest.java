package it.nextworks.experiment.model;

import java.util.HashMap;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.v3.oas.annotations.media.Schema;
import it.nextworks.experiment.model.KeyValuePair;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;

import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ExperimentExecutionRequest
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-03-15T14:52:44.684+01:00[Europe/Rome]")


public class ExperimentExecutionRequest   {
  @JsonProperty("experimentLcmId")
  private String experimentLcmId = null;

  @JsonProperty("experimentConfigurationParameters")
  private KeyValuePair experimentConfigurationParameters = new KeyValuePair();

  @JsonProperty("duration")
  private Integer duration = null;
  

  @JsonProperty("executionName")
  private String executionName = null;
  
  

  @JsonProperty("diagnostic")
  private Boolean diagnostic = null;

  public ExperimentExecutionRequest experimentLcmId(String experimentLcmId) {
    this.experimentLcmId = experimentLcmId;
    return this;
  }

  /**
   * Get experimentInstanceId
   * @return experimentInstanceId
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getExperimentLcmId() {
    return experimentLcmId;
  }

  public void setExperimentLcmId(String experimentLcmId) {
    this.experimentLcmId = experimentLcmId;
  }

  public ExperimentExecutionRequest experimentConfigurationParameters(KeyValuePair experimentConfigurationParameters) {
    this.experimentConfigurationParameters = experimentConfigurationParameters;
    return this;
  }

  
  
public String getExecutionName() {
  return executionName;
}

public void setExecutionName(String executionName) {
  this.executionName = executionName;
}

public ExperimentExecutionRequest executionName(String executionName) {
  this.executionName = executionName;
  return this;
}

  
  /**
   * Get experimentConfigurationParameters
   * @return experimentConfigurationParameters
   **/
  @Schema(description = "")
  
    @Valid
    public KeyValuePair getExperimentConfigurationParameters() {
    return experimentConfigurationParameters;
  }

  public void setExperimentConfigurationParameters(KeyValuePair experimentConfigurationParameters) {
    this.experimentConfigurationParameters = experimentConfigurationParameters;
  }

  public ExperimentExecutionRequest duration(Integer duration) {
    this.duration = duration;
    return this;
  }

  /**
   * Duration time in days
   * @return duration
   **/
  @Schema(description = "Duration time in days")
  
    public Integer getDuration() {
    return duration;
  }

  public void setDuration(Integer duration) {
    this.duration = duration;
  }

  public ExperimentExecutionRequest diagnostic(Boolean diagnostic) {
    this.diagnostic = diagnostic;
    return this;
  }

  /**
   * Activation flag for the diagnostic tool
   * @return diagnostic
   **/
  @Schema(description = "Activation flag for the diagnostic tool")
  
    public Boolean isDiagnostic() {
    return diagnostic;
  }

  public void setDiagnostic(Boolean diagnostic) {
    this.diagnostic = diagnostic;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ExperimentExecutionRequest experimentExecutionRequest = (ExperimentExecutionRequest) o;
    return Objects.equals(this.experimentLcmId, experimentExecutionRequest.experimentLcmId) &&
        Objects.equals(this.experimentConfigurationParameters, experimentExecutionRequest.experimentConfigurationParameters) &&
        Objects.equals(this.duration, experimentExecutionRequest.duration) &&
        Objects.equals(this.diagnostic, experimentExecutionRequest.diagnostic);
  }

  @Override
  public int hashCode() {
    return Objects.hash(experimentLcmId, experimentConfigurationParameters, duration, diagnostic);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ExperimentExecutionRequest {\n");
    
    sb.append("    experimentLcmId: ").append(toIndentedString(experimentLcmId)).append("\n");
    sb.append("    experimentConfigurationParameters: ").append(toIndentedString(experimentConfigurationParameters)).append("\n");
    sb.append("    duration: ").append(toIndentedString(duration)).append("\n");
    sb.append("    diagnostic: ").append(toIndentedString(diagnostic)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

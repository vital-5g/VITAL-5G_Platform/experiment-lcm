package it.nextworks.experiment.model;

import java.util.Objects;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.annotation.Validated;

import org.hibernate.annotations.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Experiment
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-03-15T14:52:44.684+01:00[Europe/Rome]")

@Entity
public class ExperimentLcm   {
	
  @Id
  @GeneratedValue
  @JsonIgnore
  private UUID id;
	
  @JsonProperty("status")
  private ExperimentStatus status = null;
  
  @JsonProperty("elcmId")
  private String elcmId = null;
  
  @JsonProperty("vsId")
  private String vsId = null;

  @JsonProperty("testbed")
  private Testbed testbed = null;
  @JsonProperty("networkServiceInstanceId")
  private String networkServiceInstanceId = null;

  @JsonProperty("experimentDescriptorId")
  private String experimentDescriptorId = null;
  
  @JsonProperty("experimentConfigurationParameters")
  private KeyValuePair experimentConfigurationParameters = new KeyValuePair();
  
  
  //this list specifies the test cases for the requested execution//TODO how to run a subset of test cases?
  //in general a subset of the test cases can be executed in each run and the config parameters of the descriptors can be overwritten
  //Important note: this field is optional, i.e. if not provided all the test cases will be executed by default, with the configuration given in the descriptor
  @JsonProperty("executions")
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  @OneToMany(mappedBy = "experiment", cascade= CascadeType.ALL, orphanRemoval = true)
  @OnDelete(action = OnDeleteAction.CASCADE)
  @LazyCollection(LazyCollectionOption.FALSE)
  private List<ExperimentExecution> executions = new ArrayList<>();

  public String getNetworkServiceInstanceId() {
    return networkServiceInstanceId;
  }

  public Testbed getTestbed() {
    return testbed;
  }

  public void setTestbed(Testbed testbed) {
    this.testbed = testbed;
  }

  public void setNetworkServiceInstanceId(String networkServiceInstanceId) {
    this.networkServiceInstanceId = networkServiceInstanceId;
  }

  public String getElcmId() {
	return elcmId;
}


public void setElcmId(String elcmId) {
	this.elcmId = elcmId;
}


public ExperimentLcm status(ExperimentStatus status) {
    this.status = status;
    return this;
  }
 
  
  public ExperimentLcm elcmId(String elcmId) {
	    this.elcmId = elcmId;
	    return this;
	  }

  public ExperimentLcm vsId(String vsId) {
	  this.vsId = vsId;
	  return this;
  }
  
  public String getVsId() {
	  return this.vsId;
  }
  
  public void setVsId(String vsId) {
	  this.vsId = vsId;
  }
  
  
  public ExperimentLcm experimentDescriptorId(String experimentDescriptorId) {
	  this.experimentDescriptorId = experimentDescriptorId;
	  return this;
  }
  
  public String getExperimentDescriptorId() {
	  return this.experimentDescriptorId;
  }
  
  public void setExperimentDescriptorId(String experimentDescriptorId) {
	  this.experimentDescriptorId = experimentDescriptorId;
  }
  
  /**
   * Get status
   * @return status
   **/
  @Schema(required = true, description = "")
      @NotNull

    @Valid
    public ExperimentStatus getStatus() {
    return status;
  }

  public void setStatus(ExperimentStatus status) {
    this.status = status;
  }

  public ExperimentLcm executions(List<ExperimentExecution> executions) {
    this.executions = executions;
    return this;
  }

  public ExperimentLcm addExecutionsItem(ExperimentExecution executionsItem) {
    if (this.executions == null) {
      this.executions = new ArrayList<ExperimentExecution>();
    }
    this.executions.add(executionsItem);
    return this;
  }

  /**
   * Get executions
   * @return executions
   **/
  @Schema(description = "")
      @Valid
    public List<ExperimentExecution> getExecutions() {
    return executions;
  }

  public void setExecutions(List<ExperimentExecution> executions) {
    this.executions = executions;
  }
  
  public ExperimentLcm experimentConfigurationParameters(KeyValuePair experimentConfigurationParameters) {
	  this.experimentConfigurationParameters = experimentConfigurationParameters;
	  return this;
  }


  public KeyValuePair getExperimentConfigurationParameters() {
	return experimentConfigurationParameters;
}


public void setExperimentConfigurationParameters(KeyValuePair experimentConfigurationParameters) {
	this.experimentConfigurationParameters = experimentConfigurationParameters;
}


@Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ExperimentLcm experiment = (ExperimentLcm) o;
    return Objects.equals(this.status, experiment.status) &&
        Objects.equals(this.executions, experiment.executions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, executions);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Experiment {\n");
    sb.append("    elcmId: ").append(toIndentedString(elcmId)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    verticalService: ").append(toIndentedString(vsId)).append("\n");
    sb.append("    executions: ").append(toIndentedString(executions)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

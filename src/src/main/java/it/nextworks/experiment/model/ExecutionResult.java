package it.nextworks.experiment.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class ExecutionResult {

  @Id
  @GeneratedValue
  @JsonIgnore
  private Long id;

  @JsonIgnore
  @OneToOne(fetch= FetchType.EAGER, mappedBy = "result", cascade=CascadeType.ALL, orphanRemoval=true)
  @OnDelete(action = OnDeleteAction.CASCADE)
  private ExperimentExecution execution;
  
  
  /**
   * Gets or Sets state
   */
  public enum StateEnum {
    FAILED("FAILED"),
    SUCCESSFUL("SUCCESSFUL");

    private String value;

    StateEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StateEnum fromValue(String text) {
      for (StateEnum b : StateEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }
  
  
  @JsonProperty("state")
  private StateEnum state = null;

  @JsonProperty("result")
  private String result = null;

  @JsonProperty("resultCode")
  private String resultCode = null;

  public ExecutionResult state(StateEnum state) {
    this.state = state;
    return this;
  }
//
//  public ExperimentExecution getExecution() {
//	    return execution;
//	  }
//
//  public void setExecution(ExperimentExecution execution) {
//	    this.execution = execution;
//  }
	  
  /**
   * Get state
   * @return state
   **/
  @Schema(description = "")
  
    public StateEnum getState() {
    return state;
  }

  public void setState(StateEnum state) {
    this.state = state;
  }

  public ExecutionResult result(String result) {
    this.result = result;
    return this;
  }

  /**
   * Get result
   * @return result
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getResult() {
    return result;
  }

  public void setResult(String result) {
    this.result = result;
  }

  public ExecutionResult resultCode(String resultCode) {
    this.resultCode = resultCode;
    return this;
  }

  /**
   * Get resultCode
   * @return resultCode
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getResultCode() {
    return resultCode;
  }

  public void setResultCode(String resultCode) {
    this.resultCode = resultCode;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ExecutionResult executionResult = (ExecutionResult) o;
    return Objects.equals(this.state, executionResult.state) &&
        Objects.equals(this.result, executionResult.result) &&
        Objects.equals(this.resultCode, executionResult.resultCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(state, result, resultCode);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ExecutionResult {\n");
    
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    result: ").append(toIndentedString(result)).append("\n");
    sb.append("    resultCode: ").append(toIndentedString(resultCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
 
}

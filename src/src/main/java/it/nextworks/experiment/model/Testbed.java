package it.nextworks.experiment.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets Testbed
 */
public enum Testbed {
  ANTWERP("ANTWERP"),
    ATHENS("ATHENS"),
    DANUBE("DANUBE"),
    GALATI("GALATI");

  private String value;

  Testbed(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static Testbed fromValue(String text) {
    for (Testbed b : Testbed.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

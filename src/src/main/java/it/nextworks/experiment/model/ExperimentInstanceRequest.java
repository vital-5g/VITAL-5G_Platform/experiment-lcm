package it.nextworks.experiment.model;

import java.util.HashMap;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;

import io.swagger.v3.oas.annotations.media.Schema;
import it.nextworks.experiment.model.KeyValuePair;

import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ExperimentInstanceRequest
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-03-15T14:52:44.684+01:00[Europe/Rome]")


public class ExperimentInstanceRequest   {
  @JsonProperty("verticalServiceInstanceId")
  private String verticalServiceInstanceId = null;

  @JsonProperty("experimentDescriptorId")
  private String experimentDescriptorId = null;

  @JsonProperty("experimentConfigurationParameters")
  private KeyValuePair experimentConfigurationParameters = new KeyValuePair();

  public ExperimentInstanceRequest verticalServiceInstanceId(String verticalServiceInstanceId) {
    this.verticalServiceInstanceId = verticalServiceInstanceId;
    return this;
  }

  /**
   * Get verticalServiceInstanceId
   * @return verticalServiceInstanceId
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getVerticalServiceInstanceId() {
    return verticalServiceInstanceId;
  }

  public void setVerticalServiceInstanceId(String verticalServiceInstanceId) {
    this.verticalServiceInstanceId = verticalServiceInstanceId;
  }

  public ExperimentInstanceRequest experimentDescriptorId(String experimentDescriptorId) {
    this.experimentDescriptorId = experimentDescriptorId;
    return this;
  }

  /**
   * Get experimentDescriptorId
   * @return experimentDescriptorId
   **/
  @Schema(required = true, description = "")
      @NotNull

    public String getExperimentDescriptorId() {
    return experimentDescriptorId;
  }

  public void setExperimentDescriptorId(String experimentDescriptorId) {
    this.experimentDescriptorId = experimentDescriptorId;
  }

  public ExperimentInstanceRequest experimentConfigurationParameters(KeyValuePair experimentConfigurationParameters) {
    this.experimentConfigurationParameters = experimentConfigurationParameters;
    return this;
  }

  /**
   * Get experimentConfigurationParameters
   * @return experimentConfigurationParameters
   **/
  @Schema(required = true, description = "")
      @NotNull

    @Valid
    public KeyValuePair getExperimentConfigurationParameters() {
    return experimentConfigurationParameters;
  }

  public void setExperimentConfigurationParameters(KeyValuePair experimentConfigurationParameters) {
    this.experimentConfigurationParameters = experimentConfigurationParameters;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ExperimentInstanceRequest experimentInstanceRequest = (ExperimentInstanceRequest) o;
    return Objects.equals(this.verticalServiceInstanceId, experimentInstanceRequest.verticalServiceInstanceId) &&
        Objects.equals(this.experimentDescriptorId, experimentInstanceRequest.experimentDescriptorId) &&
        Objects.equals(this.experimentConfigurationParameters, experimentInstanceRequest.experimentConfigurationParameters);
  }

  @Override
  public int hashCode() {
    return Objects.hash(verticalServiceInstanceId, experimentDescriptorId, experimentConfigurationParameters);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ExperimentInstanceRequest {\n");
    
    sb.append("    verticalServiceInstanceId: ").append(toIndentedString(verticalServiceInstanceId)).append("\n");
    sb.append("    experimentDescriptorId: ").append(toIndentedString(experimentDescriptorId)).append("\n");
    sb.append("    experimentConfigurationParameters: ").append(toIndentedString(experimentConfigurationParameters)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

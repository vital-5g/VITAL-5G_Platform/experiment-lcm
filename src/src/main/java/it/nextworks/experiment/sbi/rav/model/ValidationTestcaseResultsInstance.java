/*
 * RAV API
 * RAV API
 *
 * OpenAPI spec version: 1.0.0-oas3
 * Contact: name@mail.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package it.nextworks.experiment.sbi.rav.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
/**
 * ValidationTestcaseResultsInstance
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2020-04-06T19:10:49.373Z[GMT]")
public class ValidationTestcaseResultsInstance {
  @SerializedName("tcID")
  private String tcID = null;

  @SerializedName("configuration")
  private ModelConfiguration _configuration = null;

  @SerializedName("added")
  private String added = null;

  @SerializedName("finished")
  private String finished = null;

  @SerializedName("status")
  private String status = null;

  @SerializedName("validation")
  private ValidationResults validation = null;

  public ValidationTestcaseResultsInstance tcID(String tcID) {
    this.tcID = tcID;
    return this;
  }

   /**
   * Get tcID
   * @return tcID
  **/
  public String getTcID() {
    return tcID;
  }

  public void setTcID(String tcID) {
    this.tcID = tcID;
  }

  public ValidationTestcaseResultsInstance _configuration(ModelConfiguration _configuration) {
    this._configuration = _configuration;
    return this;
  }

   /**
   * Get _configuration
   * @return _configuration
  **/
  public ModelConfiguration getConfiguration() {
    return _configuration;
  }

  public void setConfiguration(ModelConfiguration _configuration) {
    this._configuration = _configuration;
  }

  public ValidationTestcaseResultsInstance added(String added) {
    this.added = added;
    return this;
  }

   /**
   * Get added
   * @return added
  **/
  public String getAdded() {
    return added;
  }

  public void setAdded(String added) {
    this.added = added;
  }

  public ValidationTestcaseResultsInstance finished(String finished) {
    this.finished = finished;
    return this;
  }

   /**
   * Get finished
   * @return finished
  **/
  public String getFinished() {
    return finished;
  }

  public void setFinished(String finished) {
    this.finished = finished;
  }

  public ValidationTestcaseResultsInstance status(String status) {
    this.status = status;
    return this;
  }

   /**
   * Get status
   * @return status
  **/
  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public ValidationTestcaseResultsInstance validation(ValidationResults validation) {
    this.validation = validation;
    return this;
  }

   /**
   * Get validation
   * @return validation
  **/
  public ValidationResults getValidation() {
    return validation;
  }

  public void setValidation(ValidationResults validation) {
    this.validation = validation;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ValidationTestcaseResultsInstance validationTestcaseResultsInstance = (ValidationTestcaseResultsInstance) o;
    return Objects.equals(this.tcID, validationTestcaseResultsInstance.tcID) &&
        Objects.equals(this._configuration, validationTestcaseResultsInstance._configuration) &&
        Objects.equals(this.added, validationTestcaseResultsInstance.added) &&
        Objects.equals(this.finished, validationTestcaseResultsInstance.finished) &&
        Objects.equals(this.status, validationTestcaseResultsInstance.status) &&
        Objects.equals(this.validation, validationTestcaseResultsInstance.validation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(tcID, _configuration, added, finished, status, validation);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ValidationTestcaseResultsInstance {\n");
    
    sb.append("    tcID: ").append(toIndentedString(tcID)).append("\n");
    sb.append("    _configuration: ").append(toIndentedString(_configuration)).append("\n");
    sb.append("    added: ").append(toIndentedString(added)).append("\n");
    sb.append("    finished: ").append(toIndentedString(finished)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    validation: ").append(toIndentedString(validation)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

package it.nextworks.experiment.sbi.enums;

public enum ConfiguratorType {
    OSM,
    DUMMY,
    OSMPOLLING
}

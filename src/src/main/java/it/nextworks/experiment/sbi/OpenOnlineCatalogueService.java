/*
 * Copyright 2018 Nextworks s.r.l.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.experiment.sbi;

import it.nextworks.catalogue.elements.*;
import it.nextworks.catalogue.elements.exp.ExpBlueprint;
import it.nextworks.catalogue.elements.exp.ExpBlueprintInfo;
import it.nextworks.catalogue.elements.exp.ExpDescriptor;
import it.nextworks.catalogue.elements.exp.ExpDescriptorInfo;
import it.nextworks.catalogue.elements.tc.TestCaseBlueprint;
import it.nextworks.catalogue.elements.tc.TestCaseBlueprintInfo;
import it.nextworks.catalogue.elements.translator.NfvNsInstantiationInfo;
import it.nextworks.catalogue.exceptions.*;
import it.nextworks.catalogue.interfaces.NetAppPackageManagementInterface;
import it.nextworks.catalogue.interfaces.TranslatorInterface;
import it.nextworks.catalogue.interfaces.VsBlueprintCatalogueInterface;
import it.nextworks.catalogue.interfaces.VsDescriptorCatalogueInterface;
import it.nextworks.catalogue.messages.OnboardVSBRequest;
import it.nextworks.experiment.sbi.cataloguev2.Vital5GCatalogueRestClient;
import it.nextworks.experiment.sbi.interfaces.OpenOnlineCatalogueProviderInterface;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.List;
import java.util.UUID;

@Service
public class OpenOnlineCatalogueService
		implements OpenOnlineCatalogueProviderInterface {

	private static final Logger log = LoggerFactory.getLogger(OpenOnlineCatalogueService.class);


	@Autowired
	private KeycloakRestTemplate restTemplate;

	@Value("${portal.catalogue.url}")
	private String catalogueAddress;


	private Vital5GCatalogueRestClient vital5GCatalogueRestClient;

	public OpenOnlineCatalogueService() {}

	@PostConstruct
	private void init(){
		vital5GCatalogueRestClient= new Vital5GCatalogueRestClient(catalogueAddress, restTemplate);
	}

	@Override
	public NfvNsInstantiationInfo translateVsd(UUID vsdId) throws FailedOperationException, NotExistingEntityException {
		log.debug("Received request to translate VSD: "+vsdId);
		return vital5GCatalogueRestClient.translateVsd(vsdId);
	}

	@Override
	public UUID onboardVsBlueprint(OnboardVSBRequest vsb, File nsd) throws AlreadyExistingEntityException, MalformattedElementException, FailedOperationException, UnAuthorizedRequestException, NotExistingEntityException {
		return null;
	}

	@Override
	public VerticalServiceBlueprint getVerticalServiceBlueprint(UUID vsbId) throws NotExistingEntityException, FailedOperationException, MalformattedElementException, UnAuthorizedRequestException {
		log.debug("Received request to retrieve VSB: "+vsbId);
		return vital5GCatalogueRestClient.getVerticalServiceBlueprint(vsbId);
	}

	@Override
	public List<VerticalServiceBlueprintInfo> queryVerticalServiceBlueprint(Testbed testbed, AccessLevel accessLevel) throws MalformattedElementException {
		return null;
	}

	@Override
	public List<VerticalServiceBlueprintInfo> getAllVerticalServiceBlueprints() throws FailedOperationException {
		throw new FailedOperationException("Method not implemented");
	}

	@Override
	public void deleteVsBlueprint(UUID vsbId, boolean force) throws FailedOperationException, UnAuthorizedRequestException, NotExistingEntityException {
		throw new FailedOperationException("Method not implemented");
	}

	@Override
	public UUID onBoardVsDescriptor(VsDescriptor request) throws MalformattedElementException, AlreadyExistingEntityException, FailedOperationException {
		throw new FailedOperationException("Method not implemented");
	}

	@Override
	public List<VsDescriptor> queryVsDescriptor() throws MalformattedElementException, FailedOperationException {
		throw new FailedOperationException("Method not implemented");
	}

	@Override
	public VsDescriptor getVsd(UUID vsdId) throws NotExistingEntityException, FailedOperationException, MalformattedElementException {
		log.debug("Received request to retrieve VSD: "+vsdId);
		return vital5GCatalogueRestClient.getVsd(vsdId);
	}

	@Override
	public void deleteVsDescriptor(UUID vsDescriptorId, boolean force) throws MalformattedElementException, NotExistingEntityException, FailedOperationException {
		throw new FailedOperationException("Method not implemented");
	}

	@Override
	public void useVsDescriptor(UUID vsdId, UUID vsiId) throws FailedOperationException, NotExistingEntityException, MalformattedElementException {
		log.debug("Received request to use VSD: "+vsdId+" "+vsiId);
		vital5GCatalogueRestClient.useVsDescriptor(vsdId, vsiId);
	}

	@Override
	public void releaseVsDescriptor(UUID vsdId, UUID vsiId) throws FailedOperationException, NotExistingEntityException {
		log.debug("Received request to release VSD: "+vsdId+" "+vsiId);
		vital5GCatalogueRestClient.releaseVsDescriptor(vsdId, vsiId);
	}

	@Override
	public UUID onboardNetAppPackage(File file) throws AlreadyExistingEntityException, MalformattedElementException, UnAuthorizedRequestException, FailedOperationException {
		return null;
	}

	@Override
	public UUID onboardNetAppPackage(File file, File file1) throws AlreadyExistingEntityException, MalformattedElementException, UnAuthorizedRequestException, FailedOperationException {
		return null;
	}

	@Override
	public NetAppBlueprint getNetAppBlueprint(UUID uuid) throws UnAuthorizedRequestException, NotExistingEntityException, FailedOperationException {
		log.debug("Received request to retrieve NetApp Blueprint "+uuid);
		return vital5GCatalogueRestClient.getNetAppBlueprint(uuid);
	}

	@Override
	public File getNetAppPackage(UUID uuid) throws UnAuthorizedRequestException, NotExistingEntityException {
		return null;
	}

	@Override
	public List<NetAppPackageInfo> queryNetAppPackages(Testbed testbed, AccessLevel accessLevel, NetAppSpecLevel netAppSpecLevel) {
		return null;
	}

	@Override
	public List<NetAppPackageInfo> getAllNetAppPackages() {
		return null;
	}

	@Override
	public void deleteNetAppPackage(UUID uuid, boolean b) throws UnAuthorizedRequestException, NotExistingEntityException {

	}

	@Override
	public File getNetAppPackageSoftwareDoc(UUID uuid, String s) throws UnAuthorizedRequestException, NotExistingEntityException, FailedOperationException {
		return null;
	}

	@Override
	public UUID onboardExperimentBlueprint(ExpBlueprint vsb) throws AlreadyExistingEntityException, MalformattedElementException, FailedOperationException, UnAuthorizedRequestException, NotExistingEntityException {
		return null;
	}

	@Override
	public ExpBlueprint getExperimentBlueprint(UUID expbId) throws NotExistingEntityException, MalformattedElementException {
		log.debug("Received request to retrieve Experiment Blueprint "+expbId);
		return vital5GCatalogueRestClient.getExperimentBlueprint(expbId);
	}

	@Override
	public List<ExpBlueprintInfo> queryExperimentBlueprint(Testbed testbed) throws MalformattedElementException, FailedOperationException {
		return null;
	}

	@Override
	public List<ExpBlueprintInfo> getAllExperimentBlueprints() {
		return null;
	}

	@Override
	public void deleteExperimentBlueprint(UUID expbId, boolean force) throws FailedOperationException, UnAuthorizedRequestException, NotExistingEntityException, MalformattedElementException {

	}

	@Override
	public List<ExpBlueprintInfo> getExperimentBlueprintsByTestCaseBlueprint(UUID testCaseBlueprintId) {
		return null;
	}

	@Override
	public UUID onboardExpDescriptor(ExpDescriptor request) throws MalformattedElementException, AlreadyExistingEntityException, FailedOperationException, NotExistingEntityException {
		return null;
	}

	@Override
	public List<ExpDescriptorInfo> queryExpDescriptor() throws MalformattedElementException, NotExistingEntityException, FailedOperationException {
		return null;
	}

	@Override
	public ExpDescriptor getExpDescriptor(UUID expdId) throws MalformattedElementException, NotExistingEntityException, FailedOperationException {
		log.debug("Received request to retrieve Experiment Descriptor Blueprint "+expdId);
		return vital5GCatalogueRestClient.getExpDescriptor(expdId);
	}

	@Override
	public void deleteExpDescriptor(UUID expDescriptorId, boolean force) throws MalformattedElementException, NotExistingEntityException, FailedOperationException {

	}

	@Override
	public void useExpDescriptor(UUID expDescriptorId, UUID experimentId) throws MalformattedElementException, NotExistingEntityException, FailedOperationException {

	}

	@Override
	public void releaseExpDescriptor(UUID expDescriptorId, UUID experimentId) throws MalformattedElementException, NotExistingEntityException, FailedOperationException {

	}

	@Override
	public TestCaseBlueprint getTestCaseBlueprint(UUID tcbId) throws MalformattedElementException, NotExistingEntityException, UnAuthorizedRequestException {
		log.debug("Received request to retrieve TCB: "+tcbId);
		return vital5GCatalogueRestClient.getTestCaseBlueprint(tcbId);
	}

	@Override
	public void deleteTcBlueprint(UUID testCaseBlueprintId, boolean force)
			throws MalformattedElementException, NotExistingEntityException, FailedOperationException, UnAuthorizedRequestException {
	}

	@Override
	public List<TestCaseBlueprintInfo> queryTcBlueprint()
			throws MalformattedElementException, NotExistingEntityException, FailedOperationException {
		return null;
	}

	@Override
	public UUID onboardTcBlueprint(TestCaseBlueprint request)
			throws MalformattedElementException, AlreadyExistingEntityException, FailedOperationException, NotExistingEntityException {
		return null;
	}
}

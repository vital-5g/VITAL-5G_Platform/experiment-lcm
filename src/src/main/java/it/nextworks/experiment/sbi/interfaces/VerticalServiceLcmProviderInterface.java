package it.nextworks.experiment.sbi.interfaces;

import it.nextworks.lcm.vs.elements.VerticalServiceInstance;

public interface VerticalServiceLcmProviderInterface {

	VerticalServiceInstance getVerticalServiceInstance(String vsiId);
	void useVerticalServiceInstance(String vsiId, String experimentLcmId);
	void releaseVerticalServiceInstance(String vsiId, String experimentLcmId);
	
}

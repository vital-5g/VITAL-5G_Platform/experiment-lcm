package it.nextworks.experiment.sbi.mano.interfaces;

import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.NfvoLcmOperationType;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.OperationStatus;

public interface PollingManagerInterface {
    void addOperation(String operationId, OperationStatus expectedStatus, String nfvNsiId, NfvoLcmOperationType operationType, Testbed testbed);

    void removeOperation(String operationId);
}

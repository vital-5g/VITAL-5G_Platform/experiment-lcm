package it.nextworks.experiment.sbi.interfaces;

import it.nextworks.catalogue.elements.*;
import it.nextworks.catalogue.elements.exp.ExpBlueprint;
import it.nextworks.catalogue.elements.exp.ExpDescriptor;
import it.nextworks.catalogue.interfaces.*;

public interface OpenOnlineCatalogueProviderInterface
        extends TranslatorInterface, ExpBlueprintCatalogueInterface, ExpDescriptorCatalogueInterface, VsBlueprintCatalogueInterface, VsDescriptorCatalogueInterface, NetAppPackageManagementInterface, TcBlueprintCatalogueInterface {

}

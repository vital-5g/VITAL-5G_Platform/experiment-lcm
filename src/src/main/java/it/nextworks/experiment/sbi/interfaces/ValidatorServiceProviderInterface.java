package it.nextworks.experiment.sbi.interfaces;

import it.nextworks.lcm.vs.elements.VerticalServiceInstance;

public interface ValidatorServiceProviderInterface {

    void configureExperiment(String experimentId, String executionId, boolean perfDiag, String nsInstanceId, VerticalServiceInstance verticalServiceInstance, String exdId);
    void startValidation(String experimentId, String executionId);
    void stopValidation(String experimentId, String executionId);
    void queryValidationResult(String experimentId, String executionId);
    void terminateExperiment(String experimentId, String executionId);
}

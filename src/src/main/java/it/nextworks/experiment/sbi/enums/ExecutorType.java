package it.nextworks.experiment.sbi.enums;

public enum ExecutorType {
    JENKINS,
    RC,
    DUMMY
}

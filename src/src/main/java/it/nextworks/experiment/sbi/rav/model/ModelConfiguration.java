/*
 * RAV API
 * RAV API
 *
 * OpenAPI spec version: 1.0.0-oas3
 * Contact: name@mail.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package it.nextworks.experiment.sbi.rav.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;
/**
 * ModelConfiguration
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2020-04-06T19:10:49.373Z[GMT]")
public class ModelConfiguration {
  @SerializedName("topics")
  private List<Topic> topics = new ArrayList<Topic>();

  @SerializedName("publish")
  private List<Publishtopic> publish = new ArrayList<Publishtopic>();

  public ModelConfiguration topics(List<Topic> topics) {
    this.topics = topics;
    return this;
  }

  public ModelConfiguration addTopicsItem(Topic topicsItem) {
    this.topics.add(topicsItem);
    return this;
  }

   /**
   * Get topics
   * @return topics
  **/
  public List<Topic> getTopics() {
    return topics;
  }

  public void setTopics(List<Topic> topics) {
    this.topics = topics;
  }

  public ModelConfiguration publish(List<Publishtopic> publish) {
    this.publish = publish;
    return this;
  }

  public ModelConfiguration addPublishItem(Publishtopic publishItem) {
    this.publish.add(publishItem);
    return this;
  }

   /**
   * Get publish
   * @return publish
  **/
  public List<Publishtopic> getPublish() {
    return publish;
  }

  public void setPublish(List<Publishtopic> publish) {
    this.publish = publish;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ModelConfiguration _configuration = (ModelConfiguration) o;
    return Objects.equals(this.topics, _configuration.topics) &&
        Objects.equals(this.publish, _configuration.publish);
  }

  @Override
  public int hashCode() {
    return Objects.hash(topics, publish);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ModelConfiguration {\n");
    
    sb.append("    topics: ").append(toIndentedString(topics)).append("\n");
    sb.append("    publish: ").append(toIndentedString(publish)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

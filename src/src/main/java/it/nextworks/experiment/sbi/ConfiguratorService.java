package it.nextworks.experiment.sbi;

import it.nextworks.experiment.sbi.interfaces.ExecutorServiceProviderInterface;
import it.nextworks.experiment.sbi.interfaces.model.NfvoLcmActionExecution;
import it.nextworks.experiment.sbi.mano.NfvoLcmService;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.NotExistingEntityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.nextworks.experiment.configuration.ConfigurationParameters;
import it.nextworks.experiment.sbi.dummyDrivers.DummyConfiguratorDriver;
import it.nextworks.experiment.sbi.enums.ConfiguratorType;
import it.nextworks.experiment.sbi.interfaces.ConfiguratorServiceProviderInterface;
import it.nextworks.experiment.sbi.mano.OsmDriver;

import javax.annotation.PostConstruct;
import java.net.URISyntaxException;
import java.util.List;

@Service
public class ConfiguratorService implements ConfiguratorServiceProviderInterface, ExecutorServiceProviderInterface {

    private static final Logger log = LoggerFactory.getLogger(ConfiguratorService.class);

    private ConfiguratorServiceProviderInterface driver;
    private ExecutorServiceProviderInterface executordriver;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private NfvoLcmService nfvoLcmService;

    @Autowired
    @Qualifier(ConfigurationParameters.elcmQueueExchange)
    private TopicExchange messageExchange;

    @Autowired
    private DummyConfiguratorDriver dummyConfiguratorDriver;

    @Value("${configurator.type}")
    private ConfiguratorType configuratorType;

    @Value("${runtime.configurator.uri}")
    private String runTimeConfiguratorURI;

    @PostConstruct
    public void init() throws URISyntaxException {
        log.debug("Initializing Configurator driver");
        if (configuratorType.equals(ConfiguratorType.OSM)) {
            this.driver = OsmDriver.getInstance(runTimeConfiguratorURI, rabbitTemplate, messageExchange);
            this.executordriver = (ExecutorServiceProviderInterface) this.driver;
        }
        else if (configuratorType.equals((ConfiguratorType.DUMMY))) {
            this.driver = dummyConfiguratorDriver;
            this.executordriver = (ExecutorServiceProviderInterface) this.driver;
        }
        else if (configuratorType.equals(ConfiguratorType.OSMPOLLING)) {
            this.driver = nfvoLcmService;
            this.executordriver = (ExecutorServiceProviderInterface) this.driver;
        }
        else
            log.error("Wrong configuration for Configurator service.");
    }

    @Override
    public void applyConfiguration(String executionId, NfvoLcmActionExecution nfvoLcmActionExecution) {
        log.debug("Configuration service driver is {}", driver);
        driver.applyConfiguration(executionId, nfvoLcmActionExecution);
    }

//    @Override
//    public void abortConfiguration(String executionId, String tcDescriptorId, String configId){
//        driver.abortConfiguration(executionId, tcDescriptorId, configId);
//    }

//    @Override
//    public void configureInfrastructureMetricCollection(String executionId, String tcDescriptorId, List<MetricInfo> metrics){
//        driver.configureInfrastructureMetricCollection(executionId, tcDescriptorId, metrics);
//    }

    @Override
    public void resetConfiguration(String executionId, NfvoLcmActionExecution actionExecution){
        driver.resetConfiguration(executionId, actionExecution);
    }

    @Override
    public void runTestCase(String executionId, NfvoLcmActionExecution actionExecution) {
        executordriver.runTestCase(executionId, actionExecution);
    }

    @Override
    public void abortTestCase(String executionId, String tcDescriptorId) {

    }

//    @Override
//    public void removeInfrastructureMetricCollection(String executionId, String tcDescriptorId, String metricConfigId){
//        driver.removeInfrastructureMetricCollection(executionId, tcDescriptorId, metricConfigId);
//    }
}

package it.nextworks.experiment.sbi.mano;

import it.nextworks.experiment.model.ExperimentExecution;
import it.nextworks.experiment.model.ExperimentLcm;
import it.nextworks.experiment.repository.ExperimentExecutionRepository;
import it.nextworks.experiment.repository.ExperimentLcmRepository;
import it.nextworks.experiment.sbi.enums.PollingManagerType;
import it.nextworks.experiment.sbi.interfaces.ConfiguratorServiceProviderInterface;
import it.nextworks.experiment.sbi.interfaces.ExecutorServiceProviderInterface;
import it.nextworks.experiment.sbi.interfaces.VerticalServiceLcmProviderInterface;
import it.nextworks.experiment.sbi.interfaces.model.NfvoLcmActionExecution;
import it.nextworks.experiment.sbi.dummyDrivers.DummyPollingManager;
import it.nextworks.experiment.sbi.mano.interfaces.PollingManagerInterface;
import it.nextworks.inventory.elements.NfvoDriverType;
import it.nextworks.inventory.elements.TestbedNfvoInformation;
import it.nextworks.lcm.nfvo.osm.Osm10Client;
import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.exceptions.FailedOperationException;
import it.nextworks.lcm.vs.interfaces.nfvo.NsLcmProviderInterface;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.*;
import it.nextworks.experiment.sbi.inventory.MultiSiteInventoryService;

import it.nextworks.experiment.sbi.dummyDrivers.DummyNfvoLcmDriver;

import it.nextworks.experiment.sbi.mano.polling.NfvoLcmOperationPollingManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class NfvoLcmService implements NsLcmProviderInterface, ConfiguratorServiceProviderInterface, ExecutorServiceProviderInterface {

    private static final Logger log = LoggerFactory.getLogger(NfvoLcmService.class);

    @Autowired
    private MultiSiteInventoryService multiSiteInventoryService;


    @Autowired
    private NfvoLcmOperationPollingManager pollingManager;

    //@Autowired
    //private DummyPollingManager dummyPollingManager;

    private PollingManagerInterface pollingManagerDriver;

    @Autowired
    private ExperimentLcmRepository elcmRepository;

    @Autowired
    private ExperimentExecutionRepository experimentExecutionRepository;

    private VerticalServiceLcmProviderInterface vslcmService;

    @Value("${environment:DEVELOPMENT}")
    private String environment;

    @Value("${pollingmanager.type}")
    private PollingManagerType pollingManagerType;

    private Map<Testbed, NsLcmProviderInterface> drivers = new HashMap<>();


    @PostConstruct
    public void init() {
        log.debug("Initializing NfvoLcmService");
        if (pollingManagerType == PollingManagerType.DUMMY)
            pollingManagerDriver = new DummyPollingManager(null, null, null);
        else if (pollingManagerType == PollingManagerType.POLLING_MANAGER)
            pollingManagerDriver = pollingManager;
    }


    @Override
    public String createNetworkServiceIdentifier(CreateNsIdentifierRequestInternal request) throws FailedOperationException {
        // Leave Empty
        return null;
    }

    @Override
    public String instantiateNetworkService(InstantiateNsRequestInternal request) throws FailedOperationException {
        // Leave Empty
        return null;
    }

    //@Override
    public OperationStatus getOperationStatus(String operationId, Testbed testbed) throws FailedOperationException {
        log.debug("Received get operation status request: "+ operationId+" "+testbed);
        NsLcmProviderInterface nsLcmProviderInterface = getTestbedDriver(testbed);
        log.info("nsLcmProviderInterface {}", nsLcmProviderInterface.toString());
        OperationStatus operationStatus = nsLcmProviderInterface.getOperationStatus(operationId, testbed);
        log.info("operationStatus {}", operationStatus.toString());
        return operationStatus;
    }

    @Override
    public Map<String, String> getNetworkServiceInstanceEndpoints(String nsInstanceId, Testbed tesbed) throws FailedOperationException {
        // Leave Empty
        return null;
    }

    @Override
    public String terminateNetworkService(String networkServiceInstanceId, Testbed testbed) throws FailedOperationException {
        // Leave Empty
        return null;
    }

    @Override
    public String executeDay2Action(String nsInstanceId, String actionId, String vnfProfileId, Map<String, String> inputParams, Testbed testbed) throws FailedOperationException {
        // Leave Empty
        return null;
    }

    private NsLcmProviderInterface getTestbedDriver(Testbed testbed) throws FailedOperationException {
        TestbedNfvoInformation nfvoInformation = multiSiteInventoryService.getTestbedNfvoInformation(testbed,
                environment);
        NsLcmProviderInterface testbedDriver = null;
        if(nfvoInformation.getNfvoDriverType()== NfvoDriverType.OSM10){
            log.debug("Creating OSM10 driver with base URL: " +nfvoInformation.getBaseUrl());
            testbedDriver = new Osm10Client(nfvoInformation);

        }else if(nfvoInformation.getNfvoDriverType()== NfvoDriverType.DUMMY){
            log.debug("Creating DUMMY driver with base URL: " +nfvoInformation.getBaseUrl());
            testbedDriver = new DummyNfvoLcmDriver();
        }
        return testbedDriver;
    }

    @Override
    public void runTestCase(String executionId, NfvoLcmActionExecution actionExecution) {
        log.debug("OSMPOLLING: Executing day2 RUN action for executionID {}", executionId);
        pollingExecuteDay2Action(executionId, actionExecution, NfvoLcmOperationType.DAY2_RUN);
    }

    @Override
    public void applyConfiguration(String executionId, NfvoLcmActionExecution nfvoLcmActionExecution) {
        log.debug("OSMPOLLING: Executing day2 RUN action for executionID {}", executionId);
        if(nfvoLcmActionExecution.getActionName()!=null)
            pollingExecuteDay2Action(executionId, nfvoLcmActionExecution, NfvoLcmOperationType.DAY2_CONFIG);
        else log.debug("Skipping due to null action");
    }

    @Override
    public void resetConfiguration(String executionId, NfvoLcmActionExecution actionExecution) {
        log.debug("OSMPOLLING: Executing day2 CLEAN action for executionID {}", executionId);
        if(actionExecution.getActionName()!=null)
            pollingExecuteDay2Action(executionId, actionExecution, NfvoLcmOperationType.DAY2_CLEAN);
        else log.debug("Skipping due to null action");
    }

    private void pollingExecuteDay2Action(String executionId, NfvoLcmActionExecution actionName, NfvoLcmOperationType nfvoLcmOperationType) {
        Optional<ExperimentExecution> experimentExecutionOptional = experimentExecutionRepository.findByExecutionId(executionId);
        if (!experimentExecutionOptional.isPresent()) {
            log.error("Unable to find Experiment execution from executionId {}", executionId);
        }
        ExperimentExecution experimentExecution = experimentExecutionOptional.get();
        String experimentLcmId = experimentExecution.getExperimentLcmId();
        ExperimentLcm experimentLcm = elcmRepository.findByElcmId(experimentLcmId).get();
        try {
            String operationId = getTestbedDriver(Testbed.valueOf(experimentLcm.getTestbed().toString()))
                    .executeDay2Action( experimentLcm.getNetworkServiceInstanceId(),
                            actionName.getActionName(),
                            actionName.getNfvoReferenceId(),
                            actionName.getInputValues(),
                            Testbed.valueOf(experimentLcm.getTestbed().toString()));

            experimentExecution.setCurrentOperationId(operationId);
            experimentExecutionRepository.saveAndFlush(experimentExecution);


            pollingManagerDriver.addOperation(operationId, OperationStatus.SUCCESSFULLY_DONE, experimentLcm.getNetworkServiceInstanceId(),
                    nfvoLcmOperationType, Testbed.valueOf(experimentLcm.getTestbed().toString()));


        } catch (FailedOperationException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void abortTestCase(String executionId, String tcDescriptorId) {

    }

    @Override 
    public String getNfvoVmVimReference(String nsInstanceId, String vnfReference, Testbed testbed) throws FailedOperationException {
        return new String();
    }

}


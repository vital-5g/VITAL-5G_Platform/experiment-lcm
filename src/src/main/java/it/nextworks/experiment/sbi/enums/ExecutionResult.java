package it.nextworks.experiment.sbi.enums;

public enum ExecutionResult {
    COMPLETED,
    FAILED
}

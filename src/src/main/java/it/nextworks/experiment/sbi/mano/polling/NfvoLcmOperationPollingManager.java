/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.experiment.sbi.mano.polling;



import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import it.nextworks.experiment.configuration.ConfigurationParameters;
import it.nextworks.experiment.model.ExperimentExecution;
import it.nextworks.experiment.rabbit.CleaningResultInternalMessage;
import it.nextworks.experiment.rabbit.ConfigurationResultInternalMessage;
import it.nextworks.experiment.rabbit.ExecutionResultInternalMessage;
import it.nextworks.experiment.rabbit.InternalMessage;
import it.nextworks.experiment.repository.ExperimentExecutionRepository;
import it.nextworks.experiment.sbi.enums.ConfigurationStatus;
import it.nextworks.experiment.sbi.mano.interfaces.NfvoLcmNotificationConsumerInterface;
import it.nextworks.experiment.sbi.mano.interfaces.PollingManagerInterface;
import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.interfaces.nfvo.NsLcmProviderInterface;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.NetworkServiceStatusChange;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.NfvoLcmOperationType;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.OperationStatus;
import it.nextworks.experiment.sbi.mano.interfaces.NfvoLcmNotificationInterface;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


/**
 * Class that manages the periodical polling of TIMEO NFVO operations
 * 
 * @author nextworks
 *
 */
@EnableScheduling
@Service
@Primary
public class NfvoLcmOperationPollingManager implements SchedulingConfigurer, PollingManagerInterface, NfvoLcmNotificationConsumerInterface {

	private static final Logger log = LoggerFactory.getLogger(NfvoLcmOperationPollingManager.class);
	
	@Value("${nfvo.lcm.polling}")
	private int timeoPollingPeriod;
	
	//map of active polled operations on TIMEO NFVO. The key is the operation ID. The value provides the info to poll the operation.
	private Map<String, PolledNfvoLcmOperation> polledOperations = new HashMap<>();
	
	@Autowired
	private NsLcmProviderInterface nfvoLcmService;

	@Autowired
	private ExperimentExecutionRepository experimentExecutionRepository;
	
	@Autowired
	private NfvoLcmNotificationInterface nfvoNotificationManager;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	@Qualifier(ConfigurationParameters.elcmQueueExchange)
	private TopicExchange messageExchange;
	
	public NfvoLcmOperationPollingManager() {
		log.debug("Initializing NFVO operations polling manager");
	}
	
	/**
	 * 
	 * @return the executor of the NFVO polling thread task
	 */
	@Bean(destroyMethod = "shutdown")
	public Executor taskExecutor() {
		return Executors.newSingleThreadScheduledExecutor();
	}

	/**
	 * Method to trigger the periodical polling task. 
	 * The period is configured in the application property file.
	 * 
	 */
	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
		taskRegistrar.setScheduler(taskExecutor());
		taskRegistrar.addTriggerTask(
				new NfvoLcmOperationPollingTask(this, nfvoLcmService, nfvoNotificationManager),
				new Trigger() {
					
					@Override
					public Date nextExecutionTime(TriggerContext triggerContext) {
						Calendar nextExecutionTime = new GregorianCalendar();
						Date lastActualExecutionTime = triggerContext.lastActualExecutionTime();
						nextExecutionTime.setTime(lastActualExecutionTime != null ? lastActualExecutionTime : new Date());
						nextExecutionTime.add(Calendar.SECOND, timeoPollingPeriod);
						return nextExecutionTime.getTime();
					}
				});
	}

	
	/**
	 * Adds a new TIMEO NFVO operation in the list of the operations to be polled
	 * 
	 * @param operationId ID of the NFVO operation to be polled
	 * @param expectedStatus expected status of the operation - when the operation reaches this status the listener is notified
	 * @param nfvNsiId ID of the network service instance the operation refers to
	 * @param operationType type of operation
	 */
	public synchronized void addOperation(String operationId, OperationStatus expectedStatus, String nfvNsiId, NfvoLcmOperationType operationType, Testbed testbed) {
		PolledNfvoLcmOperation operation = new PolledNfvoLcmOperation(operationId, expectedStatus, nfvNsiId, operationType, testbed);
		this.polledOperations.put(operationId, operation);
		log.debug("Added operation " + operationId + " to the list of NFVO operations in polling. Expected status: " + expectedStatus.toString());
	}
	
	/**
	 * Removes an operation with the given ID from the list of operations to be polled
	 * 
	 * @param operationId ID the VNFM operation to be removed from the list
	 */
	public synchronized void removeOperation(String operationId) {
		this.polledOperations.remove(operationId);
		log.debug("Operation " + operationId + " removed from the list of VNFM operations to be polled");
	}


	/**
	 * @return a copy of the polledOperations
	 */
	public synchronized Map<String, PolledNfvoLcmOperation> getPolledOperationsCopy() {
		Map<String, PolledNfvoLcmOperation> copy = new HashMap<>();
		for (Map.Entry<String, PolledNfvoLcmOperation> e : polledOperations.entrySet()) {
			copy.put(e.getKey(), e.getValue());
		}
		return copy;
	}

	private void sendMessageToQueue(InternalMessage msg, String topic) throws JsonProcessingException {
		ObjectMapper mapper = buildObjectMapper();
		String json = mapper.writeValueAsString(msg);
		rabbitTemplate.convertAndSend(messageExchange.getName(), topic, json);
	}


	private ObjectMapper buildObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
		mapper.registerModule(new JavaTimeModule());
		return mapper;
	}

	private void manageConfigurationError(String errorMessage, String executionId){
		log.error("Configuration of Experiment Execution with Id {} failed : {}", executionId, errorMessage);
		errorMessage = String.format("Configuration of Experiment Execution with Id %s failed : %s", executionId, errorMessage);
		String topic = "lifecycle.configurationResult." + executionId;
		InternalMessage internalMessage = new ConfigurationResultInternalMessage(ConfigurationStatus.FAILED, errorMessage, null, true);
		try {
			sendMessageToQueue(internalMessage, topic);
		} catch (JsonProcessingException e) {
			log.error("Error while translating internal scheduling message in Json format");
			log.debug(null, e);
		}
	}

	@Override
	public void notifyNfvNsStatusChange(String nfvNsId, String OperationId, NetworkServiceStatusChange changeType, boolean b, Testbed testbed) {
		Optional<ExperimentExecution> experimentExecutionOptional = experimentExecutionRepository.findByCurrentOperationId(OperationId);
		ExperimentExecution experimentExecution = experimentExecutionOptional.get();
		String executionId = experimentExecution.getExecutionId();

		InternalMessage internalMessage = null;
		String topic = null;
		switch (changeType) {
			case DAY2_CONFIGURATION_EXECUTED:
				topic = "lifecycle.configurationResult." + executionId;
				internalMessage = new ConfigurationResultInternalMessage(ConfigurationStatus.CONFIGURED, "OK", "configId",false);
				break;
			case DAY2_RUN_EXECUTED:
				topic = "lifecycle.executionresult." + executionId;
				internalMessage = new ExecutionResultInternalMessage();
				break;
			case DAY2_CLEAN_EXECUTED:
				topic = "lifecycle.cleanresult." + executionId;
				internalMessage = new CleaningResultInternalMessage();
				break;
			default:
				log.info("Invalid changetype");
				break;
		}

		try {
			sendMessageToQueue(internalMessage, topic);
		} catch (JsonProcessingException e) {
			log.error("Error while translating internal scheduling message in Json format");
			manageConfigurationError("Error while translating internal scheduling message in Json format", executionId);
		}

		log.debug("Sending Notification about status change of nfvNsId {} and OperationId {}", nfvNsId, OperationId);
	}

	
}

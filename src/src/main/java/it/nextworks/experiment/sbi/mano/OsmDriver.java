package it.nextworks.experiment.sbi.mano;

import it.nextworks.experiment.sbi.interfaces.ExecutorServiceProviderInterface;
import it.nextworks.experiment.sbi.interfaces.model.NfvoLcmActionExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import it.nextworks.experiment.configuration.ConfigurationParameters;
import it.nextworks.experiment.rabbit.ConfigurationResultInternalMessage;
import it.nextworks.experiment.rabbit.InternalMessage;
import it.nextworks.experiment.rabbit.ValidationResultInternalMessage;
import it.nextworks.experiment.sbi.enums.ConfigurationStatus;
import it.nextworks.experiment.sbi.interfaces.ConfiguratorServiceProviderInterface;
import it.nextworks.experiment.sbi.interfaces.SiteOrchestratorProviderInterface;
import it.nextworks.experiment.sbi.rav.ResultAnalyticsDriver;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.lcm.nfvo.osm.Osm10Client;

public class OsmDriver implements SiteOrchestratorProviderInterface, ConfiguratorServiceProviderInterface, ExecutorServiceProviderInterface {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    @Qualifier(ConfigurationParameters.elcmQueueExchange)
    private TopicExchange messageExchange;
    
	
	private static final Logger log = LoggerFactory.getLogger(ResultAnalyticsDriver.class);
	
    private OsmDriver(String osmHost, RabbitTemplate rabbitTemplate, TopicExchange messageExchange){
        log.debug("Initializing Msno Driver: uri {}", osmHost);
//        ApiClient ac = new ApiClient();
        String url = "http://" + osmHost + "/nslcm/v1";
//        restClient = new DefaultApi(ac.setBasePath(url));
        this.messageExchange = messageExchange;
        this.rabbitTemplate = rabbitTemplate;
    }
	
	public static OsmDriver getInstance(String osmHost, RabbitTemplate rabbitTemplate, TopicExchange messageExchange){
		
        return new OsmDriver(osmHost, rabbitTemplate, messageExchange);
    }
    
	
	public void runConfiguration(String executionId) {
				
	}
	
	public void runStuff(String executionId) {
		log.debug("Starting configuration of experiment execution with id {}", executionId);
		try{
			Thread.sleep(5000);
		} catch (Exception e1) {
			log.error("Something went wrong on runConfiguration sleep thread");
		}
		String result = "OK";
        String topic = "lifecycle.configurationResult." + executionId;
        InternalMessage internalMessage = new ConfigurationResultInternalMessage(ConfigurationStatus.CONFIGURED, result, null, false);
        try {
            sendMessageToQueue(internalMessage, topic);
        } catch (JsonProcessingException e) {
            log.error("Error while translating internal scheduling message in Json format");
            manageConfigurationError("Error while translating internal scheduling message in Json format", executionId);
        }
	}
	
    private void sendMessageToQueue(InternalMessage msg, String topic) throws JsonProcessingException {
        ObjectMapper mapper = buildObjectMapper();
        String json = mapper.writeValueAsString(msg);
        rabbitTemplate.convertAndSend(messageExchange.getName(), topic, json);
    }

    private ObjectMapper buildObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }
    
    private void manageConfigurationError(String errorMessage, String executionId){
        log.error("Configuration of Experiment Execution with Id {} failed : {}", executionId, errorMessage);
        errorMessage = String.format("Configuration of Experiment Execution with Id %s failed : %s", executionId, errorMessage);
        String topic = "lifecycle.configurationResult." + executionId;
        InternalMessage internalMessage = new ConfigurationResultInternalMessage(ConfigurationStatus.FAILED,errorMessage, null, true);
        try {
            sendMessageToQueue(internalMessage, topic);
        } catch (JsonProcessingException e) {
            log.error("Error while translating internal scheduling message in Json format");
            log.debug(null, e);
        }
    }

	@Override
	public void queryNs() throws FailedOperationException, MalformattedElementException {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void resetConfiguration(String executionId, NfvoLcmActionExecution actionExecution) {
		// TODO Auto-generated method stub
		
	}

    private void configureNetAppUsingDay2(String executionId) {

    }

	@Override
	public void applyConfiguration(String executionId, NfvoLcmActionExecution nfvoLcmActionExecution) {
		log.info("OSM DRIVER: Applying configuration with executionId {}", executionId);
        runStuff(executionId);
		
	}

    @Override
    public void runTestCase(String executionId, NfvoLcmActionExecution actionExecution) {

    }

    @Override
    public void abortTestCase(String executionId, String tcDescriptorId) {

    }
}

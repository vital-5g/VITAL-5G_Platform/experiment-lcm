package it.nextworks.experiment.sbi.enums;

public enum ConfigurationStatus {
    CONFIGURED,
    CONF_RESET,
    FAILED
}

package it.nextworks.experiment.sbi.interfaces;

import it.nextworks.experiment.sbi.interfaces.model.NfvoLcmActionExecution;

public interface ExecutorServiceProviderInterface {
    void runTestCase(String executionId, NfvoLcmActionExecution actionExecution);
    void abortTestCase(String executionId, String tcDescriptorId);
}

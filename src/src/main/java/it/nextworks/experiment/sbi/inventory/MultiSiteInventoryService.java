package it.nextworks.experiment.sbi.inventory;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.nextworks.inventory.elements.Environment;
import it.nextworks.inventory.elements.TestbedNfvoInformation;
import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.exceptions.FailedOperationException;

import it.nextworks.experiment.sbi.inventory.rest.MultiSiteInventoryRestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

@Service
public class MultiSiteInventoryService {

    private static final Logger log = LoggerFactory.getLogger(MultiSiteInventoryService.class);

    @Value("${site-inventory.url:http://localhost:8084/multisite-inventory}")
    private String multisiteInventoryUrl;

    private MultiSiteInventoryRestClient restClient;

    @PostConstruct
    void setupMultiSiteInventoryClient(){
        log.debug("Configuring Multisite Inventory client with URL:"+ multisiteInventoryUrl);
        restClient = new MultiSiteInventoryRestClient(multisiteInventoryUrl);
    }


    public TestbedNfvoInformation getTestbedNfvoInformation(Testbed testbed, String environment) throws FailedOperationException {
        log.debug("Received request to retrieve Testbed NFVO information: "+testbed + " "+environment);
        //return new TestbedNfvoInformation(nfvoBaseUrl, vimAccountId, username, password, nfvoDriverType, project, vimExternalNetwork);
        List<TestbedNfvoInformation> nfvos = restClient.getNfvoInformation(it.nextworks.inventory.elements.Testbed.valueOf(testbed.toString()), Environment.valueOf(environment));

        if(!nfvos.isEmpty()){
            return nfvos.get(0);

        }else{
            log.error("Could not find NFVO information for specified testbed/environment");
            return null;
        }

    }

}

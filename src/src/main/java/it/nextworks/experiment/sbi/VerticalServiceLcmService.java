package it.nextworks.experiment.sbi;

import java.net.URISyntaxException;

import javax.annotation.PostConstruct;

import it.nextworks.experiment.sbi.vslcmv2.VsLcmAuthRestClient;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.nextworks.experiment.sbi.dummyDrivers.DummyValidatorDriver;
import it.nextworks.experiment.sbi.dummyDrivers.DummyVerticalServiceLcmDriver;
import it.nextworks.experiment.sbi.enums.ValidatorType;
import it.nextworks.experiment.sbi.interfaces.VerticalServiceLcmProviderInterface;
import it.nextworks.experiment.sbi.rav.ResultAnalyticsDriver;
import it.nextworks.experiment.sbi.vslcm.VerticalServiceLcmDriver;
import it.nextworks.lcm.vs.elements.VerticalServiceInstance;

@Service
public class VerticalServiceLcmService implements VerticalServiceLcmProviderInterface {
	private static final Logger log = LoggerFactory.getLogger(VerticalServiceLcmService.class);
	
    @Value("${vertical.service.host}")
    private String verticalServiceHost;
    
    @Value("${vertical.service.type}")
    private String verticalServiceType;

    @Autowired
    private KeycloakRestTemplate restTemplate;
    
    private VerticalServiceLcmProviderInterface driver;
    
    
    @PostConstruct
    public void init() throws URISyntaxException {
        log.debug("Initializing VerticalServiceLCM driver");
        if (verticalServiceType.equals("VSI"))
            this.driver = VerticalServiceLcmDriver.getInstance(verticalServiceHost);
        else if (verticalServiceType.equals("DUMMY"))
            this.driver = new DummyVerticalServiceLcmDriver();
        else if (verticalServiceType.equals("VSI2"))
            this.driver = new VsLcmAuthRestClient(verticalServiceHost, restTemplate);
        else
            log.error("Wrong configuration for Executor service.");
    }


	@Override
	public VerticalServiceInstance getVerticalServiceInstance(String vsiId) {
		return driver.getVerticalServiceInstance(vsiId);
	}


	@Override
	public void useVerticalServiceInstance(String vsiId, String experimentLcmId) {
		driver.useVerticalServiceInstance(vsiId, experimentLcmId);
		
	}


	@Override
	public void releaseVerticalServiceInstance(String vsiId, String experimentLcmId) {
		driver.releaseVerticalServiceInstance(vsiId, experimentLcmId);
		
	}
    
    


}

package it.nextworks.experiment.sbi;

import it.nextworks.experiment.configuration.ConfigurationParameters;
import it.nextworks.experiment.sbi.dummyDrivers.DummyOrchestratorDriver;
import it.nextworks.experiment.sbi.enums.OrchestratorType;
import it.nextworks.experiment.sbi.interfaces.SiteOrchestratorProviderInterface;
import it.nextworks.experiment.sbi.mano.OsmDriver;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.common.messages.GeneralizedQueryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class SiteOrchestratorService implements SiteOrchestratorProviderInterface {

    private static final Logger log = LoggerFactory.getLogger(SiteOrchestratorService.class);

    private SiteOrchestratorProviderInterface driver;

    @Value("${site.orchestrator.type}")
    private OrchestratorType orchestratorType;
    
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    @Qualifier(ConfigurationParameters.elcmQueueExchange)
    private TopicExchange messageExchange;

    @Value("${msno.host}")
    private String msnoHost;

    @PostConstruct
    public void init(){
        log.debug("Initializing Multi-site Orchestrator driver");
        if (orchestratorType.equals(OrchestratorType.OSM))
            this.driver = OsmDriver.getInstance(msnoHost, rabbitTemplate, messageExchange);
        else if (orchestratorType.equals(OrchestratorType.DUMMY))
            this.driver = new DummyOrchestratorDriver();
        else
            log.error("Wrong configuration for Orchestrator service.");
    }

//    @Override
//    public NsInstance queryNs(GeneralizedQueryRequest request) throws FailedOperationException, MalformattedElementException{
//        return driver.queryNs(request);
//    }
//    
    @Override
    public void queryNs() throws FailedOperationException, MalformattedElementException{
        driver.queryNs();
    }
}

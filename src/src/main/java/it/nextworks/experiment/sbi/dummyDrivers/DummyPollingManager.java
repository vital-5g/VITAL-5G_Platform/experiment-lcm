package it.nextworks.experiment.sbi.dummyDrivers;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import it.nextworks.experiment.configuration.ConfigurationParameters;
import it.nextworks.experiment.model.ExperimentExecution;
import it.nextworks.experiment.rabbit.CleaningResultInternalMessage;
import it.nextworks.experiment.rabbit.ConfigurationResultInternalMessage;
import it.nextworks.experiment.rabbit.ExecutionResultInternalMessage;
import it.nextworks.experiment.rabbit.InternalMessage;
import it.nextworks.experiment.repository.ExperimentExecutionRepository;
import it.nextworks.experiment.sbi.enums.ConfigurationStatus;
import it.nextworks.experiment.sbi.mano.interfaces.NfvoLcmNotificationConsumerInterface;
import it.nextworks.experiment.sbi.mano.interfaces.PollingManagerInterface;
import it.nextworks.experiment.sbi.mano.polling.NfvoLcmOperationPollingManager;
import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.NetworkServiceStatusChange;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.NfvoLcmOperationType;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.OperationStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

public class DummyPollingManager implements PollingManagerInterface, NfvoLcmNotificationConsumerInterface {

    private static final Logger log = LoggerFactory.getLogger(DummyPollingManager.class);

    private RabbitTemplate rabbitTemplate;

    private TopicExchange messageExchange;

    private ExperimentExecutionRepository experimentExecutionRepository;

    public DummyPollingManager(RabbitTemplate rabbitTemplate, TopicExchange messageExchange, ExperimentExecutionRepository experimentExecutionRepository) {
        log.info("Initializing DUMMY Polling Manager");
        this.rabbitTemplate = rabbitTemplate;
        this.messageExchange = messageExchange;
        this.experimentExecutionRepository = experimentExecutionRepository;
    }

    public void addOperation(String operationId, OperationStatus expectedStatus, String nfvNsiId, NfvoLcmOperationType operationType, Testbed testbed) {
        log.info("DUMMY: Added operation {} to the Polling Operation List", operationId);

        log.debug("Waiting 5 seconds to emulate expeiment configuration ");
        try{
            Thread.sleep(5000);
        } catch (Exception e1) {
            log.error("Something went wrong on runConfiguration sleep thread");
        }

        removeOperation(operationId);
        switch (operationType) {
            case DAY2_CONFIG:
                notifyNfvNsStatusChange(nfvNsiId, operationId, NetworkServiceStatusChange.DAY2_CONFIGURATION_EXECUTED, false, testbed);
                break;
            case DAY2_RUN:
                notifyNfvNsStatusChange(nfvNsiId, operationId, NetworkServiceStatusChange.DAY2_RUN_EXECUTED, false, testbed);
                break;
            case DAY2_CLEAN:
                notifyNfvNsStatusChange(nfvNsiId, operationId, NetworkServiceStatusChange.DAY2_CLEAN_EXECUTED, false, testbed);
                break;
        }

    }

    public void removeOperation(String operationId) {
        log.info("DUMMY: Removed operation {} to the Polling Operation List", operationId);
    }


    private void sendMessageToQueue(InternalMessage msg, String topic) throws JsonProcessingException {
        ObjectMapper mapper = buildObjectMapper();
        String json = mapper.writeValueAsString(msg);
        rabbitTemplate.convertAndSend(messageExchange.getName(), topic, json);
    }


    private ObjectMapper buildObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }

    private void manageConfigurationError(String errorMessage, String executionId){
        log.error("Configuration of Experiment Execution with Id {} failed : {}", executionId, errorMessage);
        errorMessage = String.format("Configuration of Experiment Execution with Id %s failed : %s", executionId, errorMessage);
        String topic = "lifecycle.configurationResult." + executionId;
        InternalMessage internalMessage = new ConfigurationResultInternalMessage(ConfigurationStatus.FAILED, errorMessage, null, true);
        try {
            sendMessageToQueue(internalMessage, topic);
        } catch (JsonProcessingException e) {
            log.error("Error while translating internal scheduling message in Json format");
            log.debug(null, e);
        }
    }


    @Override
    public void notifyNfvNsStatusChange(String nfvNsId, String OperationId, NetworkServiceStatusChange changeType, boolean b, Testbed testbed) {
        Optional<ExperimentExecution> experimentExecutionOptional = experimentExecutionRepository.findByCurrentOperationId(OperationId);
        ExperimentExecution experimentExecution = experimentExecutionOptional.get();
        String executionId = experimentExecution.getExecutionId();

        log.info("Dummy: Notifying status change, changetype {}", changeType);

        InternalMessage internalMessage = null;
        String topic = null;
        switch (changeType) {
            case DAY2_CONFIGURATION_EXECUTED:
                topic = "lifecycle.configurationResult." + executionId;
                internalMessage = new ConfigurationResultInternalMessage(ConfigurationStatus.CONFIGURED, "OK", "configId",false);
                break;
            case DAY2_RUN_EXECUTED:
                topic = "lifecycle.executionresult." + executionId;
                internalMessage = new ExecutionResultInternalMessage();
                break;
            case DAY2_CLEAN_EXECUTED:
                topic = "lifecycle.cleanresult." + executionId;
                internalMessage = new CleaningResultInternalMessage();
                break;
            default:
                log.info("Invalid changetype");
                break;
        }

        try {
            sendMessageToQueue(internalMessage, topic);
        } catch (JsonProcessingException e) {
            log.error("Error while translating internal scheduling message in Json format");
            manageConfigurationError("Error while translating internal scheduling message in Json format", executionId);
        }

        log.debug("Sending Notification about status change of nfvNsId {} and OperationId {}", nfvNsId, OperationId);
    }
}

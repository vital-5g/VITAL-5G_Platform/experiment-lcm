package it.nextworks.experiment.sbi.vslcm;


import it.nextworks.experiment.sbi.interfaces.VerticalServiceLcmProviderInterface;
import it.nextworks.experiment.sbi.vslcm.api.VerticalServiceLcmApi;
import it.nextworks.experiment.sbi.vslcm.model.ExperimentExecutionSpec;
import it.nextworks.lcm.vs.elements.VerticalServiceInstance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VerticalServiceLcmDriver implements VerticalServiceLcmProviderInterface {

    // static variable single_instance of type RAVDriver
    private static VerticalServiceLcmDriver single_instance = null;
    
    private VerticalServiceLcmApi verticalServiceLcmApi;

    private static final Logger log = LoggerFactory.getLogger(VerticalServiceLcmDriver.class);

    private VerticalServiceLcmDriver(String verticalServiceHost) {
    	log.debug("Initializing VSI Driver : uri {}", verticalServiceHost);
        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath(verticalServiceHost);
        apiClient.setConnectTimeout(60000);
        apiClient.setReadTimeout(60000);
        
        verticalServiceLcmApi = new VerticalServiceLcmApi(apiClient);
    }


    public static VerticalServiceLcmDriver getInstance(String vsiHost) {
        if (single_instance == null)

			//TODO add new client
            single_instance = new VerticalServiceLcmDriver(vsiHost);
        else
            log.debug("VSI Driver already instantiated: uri {}", vsiHost);
        return single_instance;
    }


	@Override
	public VerticalServiceInstance getVerticalServiceInstance(String vsiId) {
		try {
			log.debug("Querying VSI Driver to retrieve verticalServiceInstance with id {}", vsiId);
			return verticalServiceLcmApi.getVsInstance(vsiId);
		} catch (ApiException e) {
			log.error("Failed querying VSI Driver to retrieve verticalServiceInstance with id {}. errorMessage: {}", vsiId, e.getMessage());
			return null;
		}
	}

	


	@Override
	public void useVerticalServiceInstance(String vsiId, String experimentLcmId) {
		log.debug("Setting as IN_USE for verticalServiceInstance with id {}", vsiId);
		ExperimentExecutionSpec body = new ExperimentExecutionSpec();
		body.experimentExecutionId(experimentLcmId);
		try {
			verticalServiceLcmApi.useVsInstance(body, vsiId, null, null, null, null, null);
			log.debug("Executed set IN_USE for verticalServiceInstance with id {}", vsiId);
		} catch (ApiException e) {
			log.error("Failed set IN_USE for verticalServiceInstance with id {}. errorMessage: {}", vsiId, e.getMessage());
		}
	}



	@Override
	public void releaseVerticalServiceInstance(String vsiId, String experimentLcmId) {
		log.debug("Releasing lock for verticalServiceInstance with id {}", vsiId);
		ExperimentExecutionSpec body = new ExperimentExecutionSpec();
		body.experimentExecutionId(experimentLcmId);
		try {
			verticalServiceLcmApi.releaseVsInstance(body, vsiId, null, null, null, null, null);
			log.debug("Executed release for verticalServiceInstance with id {}", vsiId);
		} catch (ApiException e) {
			log.error("Failed release for verticalServiceInstance with id {}. errorMessage: {}", vsiId, e.getMessage());
		}
		
	}

}

package it.nextworks.experiment.sbi.interfaces.model;

import java.util.HashMap;
import java.util.Map;

public class NfvoLcmActionExecution {
    private String NfvoReferenceId = null;
    private Map<String, String> inputValues = null;

    private String actionName = null;

    public NfvoLcmActionExecution(String nfvoReferenceId, Map<String, String> inputValues, String actionName) {
        NfvoReferenceId = nfvoReferenceId;
        this.inputValues = inputValues;
        this.actionName = actionName;
    }

    public String getNfvoReferenceId() {
        return NfvoReferenceId;
    }

    public Map<String, String> getInputValues() {
        return inputValues;
    }

    public String getActionName() {
        return actionName;
    }
}

package it.nextworks.experiment.sbi.dummyDrivers;

import it.nextworks.experiment.sbi.interfaces.VerticalServiceLcmProviderInterface;
import it.nextworks.lcm.vs.elements.VerticalServiceInstance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DummyVerticalServiceLcmDriver implements VerticalServiceLcmProviderInterface {

    private static final Logger log = LoggerFactory.getLogger(DummyVerticalServiceLcmDriver.class);

    public DummyVerticalServiceLcmDriver() {
        log.debug("Initializing Dummy Vertical Service Driver");
    }

	@Override
	public VerticalServiceInstance getVerticalServiceInstance(String vsiId) {
		log.info("DUMMY VSI DRIVER: Retrieving VS Instance with ID: {}", vsiId);
		VerticalServiceInstance vsi = new VerticalServiceInstance("VSD_ID", null, null, null, null, null, null, null, null);
		return vsi;
	}

	@Override
	public void useVerticalServiceInstance(String vsiId, String experimentLcmId) {
		log.info("DUMMY VS DRIVER: Start using VS Instance with ID {} for experiment {}", vsiId, experimentLcmId);
		
	}

	@Override
	public void releaseVerticalServiceInstance(String vsiId, String experimentLcmId) {
		log.info("DUMMY VS DRIVER: Releasing VS Instance with ID {} for experiment {}", vsiId, experimentLcmId);
	}

}

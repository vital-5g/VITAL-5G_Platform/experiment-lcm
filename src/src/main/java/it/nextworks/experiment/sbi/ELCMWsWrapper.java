package it.nextworks.experiment.sbi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.ProcessBuilder;
import java.io.*;
import java.lang.*;
import java.util.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

public class ELCMWsWrapper {
    private static final Logger log = LoggerFactory.getLogger(ELCMWsWrapper.class);

    private final String OsmAddress;
    private final String JujuPassword;

    public ELCMWsWrapper(String OsmAddress, String JujuPassword) {
        this.OsmAddress = OsmAddress;
        this.JujuPassword = JujuPassword;
    }

    public void health() {
        log.info("Hi from ELCMWsWrapper");
    }

    public void Bootstrap() {
        log.debug("Hi From Bootstrap");

        StringBuilder sb = new StringBuilder();
        sb.append("sudo apt update\n");
        sb.append("sudo apt install -y python3 python3-pip jq\n");
        sb.append("pip install websocket-client\n");

        try {
            ProcessBuilder pb = new ProcessBuilder("/bin/bash");
            Process bash = pb.start();
            PrintStream ps = new PrintStream(bash.getOutputStream());
            ps.println(sb);
            ps.close();
            BufferedReader br = new BufferedReader(new InputStreamReader(bash.getInputStream()));
            try {
                bash.waitFor();
            } catch (java.lang.InterruptedException ex) {
                log.error("InterruptedException in Bootstrap function");
                ex.printStackTrace(System.out);
            }
            //String line = br.readLine();
            br.close();
        } catch (java.io.IOException ex) {
            log.error("IOException in Bootstrap function");
            ex.printStackTrace(System.out);
        }
    }

    public JSONArray GetModels() {
        log.debug("Hi From Self Contained");

        StringBuilder sb = new StringBuilder();
        sb.append("python3 <<EOF\n");
        sb.append("import websocket\n");
        sb.append("import ssl\n");
        sb.append("import json\n");
        sb.append("def ws_send(conn, data):\n");
        sb.append("	req = json.dumps(data)\n");
        sb.append("	try:\n");
        sb.append("		conn.send(req)\n");
        sb.append("	except Exception as err:\n");
        sb.append("		return\n");
        sb.append("	try:\n");
        sb.append("		resp = conn.recv()\n");
        sb.append("	except Exception as err:\n");
        sb.append("		return\n");
        sb.append("	return json.loads(resp)\n");
        sb.append("conn = websocket.create_connection(\"wss://").append(this.OsmAddress).append(":17070/api\", sslopt={\'cert_reqs\': ssl.CERT_NONE})\n");
        sb.append("ws_send(conn, {\"type\":\"Admin\",\"request\":\"Login\",\"version\":3,\"params\":{\"credentials\": \"").append(this.JujuPassword).append("\",\"auth-tag\":\"user-admin\"},\"request-id\":1})\n");
        sb.append("models = ws_send(conn, {\"type\":\"ModelManager\",\"request\":\"ListModels\",\"version\":5,\"params\":{\"tag\":\"user-admin\"},\"request-id\":2})\n");
        sb.append("conn.close()\n");
        sb.append("print(json.dumps(models))\n");
        sb.append("EOF\n");

        try {
            ProcessBuilder pb = new ProcessBuilder("/bin/bash");
            Process bash = pb.start();
            PrintStream ps = new PrintStream(bash.getOutputStream());
            ps.println(sb);
            ps.close();
            BufferedReader br = new BufferedReader(new InputStreamReader(bash.getInputStream()));
            try {
                bash.waitFor();
            } catch (java.lang.InterruptedException ex) {
                log.error("InterruptedException in GetModels function");
                ex.printStackTrace(System.out);
            }

            StringBuilder res = new StringBuilder();
            String line = "";
            while ((line = br.readLine()) != null) {
                res.append(line);
            }

            br.close();
            try {
                final JSONObject JsonResp = new JSONObject(res.toString());
                final JSONObject Response = JsonResp.getJSONObject("response");
                return Response.getJSONArray("user-models");
            } catch (JSONException ex) {
                log.error("JSONException in GetModels function");
                ex.printStackTrace(System.out);
            }
        } catch (java.io.IOException ex) {
            log.error("IOException in GetModels function");
            ex.printStackTrace(System.out);
        }
        return null;
    }

    public JSONObject GetUnitByApplication(String ModelUUID, String AppName) {
        log.debug("Hi From GetUnitByApplication {} {}", ModelUUID, AppName);
        return this.GetModelFullStatus(ModelUUID).getJSONObject("applications").getJSONObject(AppName).getJSONObject("units");
    }

    public JSONObject FindUnitId(String AppName, String ActionName) {
		/*
		{
			"ModelUUID": <String>,
			"UnitID": <String>,
			"ActionName": <String>,
			"ActionParams": <json>
		}

		*/

        log.debug("Hi From FindUnitId {} {}", AppName, ActionName);
        JSONArray Models = this.GetModels();
        int length = Models.length();

        List<String> ModelUUIDs = new ArrayList<String>();
        for (int i=0; i<length; i++)
            ModelUUIDs.add(Models.getJSONObject(i).getJSONObject("model").getString("uuid").toString());

        for(String model:ModelUUIDs) {
            log.info(model);
            JSONObject Actions = GetActionsByApplication(model, AppName);
            JSONArray Results = Actions.getJSONArray("results");
            JSONObject Res = Results.getJSONObject(0);
            if (Res.has("actions")) {
                log.debug("Found actions " + Res.toString());
                Iterator<String> keys = Res.getJSONObject("actions").keys();
                while(keys.hasNext()) {
                    String key = keys.next();
                    if (key.equals(ActionName)) {
                        log.info("FOUND ACTION " + ActionName);
                        JSONObject Units = this.GetUnitByApplication(model, AppName);
                        Iterator<String> UnitKeys = Units.keys();
                        String FirstUnit = UnitKeys.next().replace("/", "-");
                        log.info("First Unit " + FirstUnit);
                        JSONObject Ret = new JSONObject();
                        Ret.put("ModelUUID", model);
                        Ret.put("UnitID", FirstUnit);
                        Ret.put("ActionName", ActionName);
                        Ret.put("ActionParams", Res.getJSONObject("actions").getJSONObject(ActionName).getJSONObject("params"));
                        return Ret;
                    }
                }
            }
        }
        return null;
    }

    public JSONObject GetModelFullStatus(String ModelUUID) {
        log.debug("Hi From GetModelFullStatus {}", ModelUUID);

        StringBuilder sb = new StringBuilder();
        sb.append("python3 <<EOF\n");
        sb.append("import websocket\n");
        sb.append("import ssl\n");
        sb.append("import json\n");
        sb.append("def ws_send(conn, data):\n");
        sb.append("	req = json.dumps(data)\n");
        sb.append("	try:\n");
        sb.append("		conn.send(req)\n");
        sb.append("	except Exception as err:\n");
        sb.append("		return\n");
        sb.append("	try:\n");
        sb.append("		resp = conn.recv()\n");
        sb.append("	except Exception as err:\n");
        sb.append("		return\n");
        sb.append("	return json.loads(resp)\n");
        sb.append("conn = websocket.create_connection(\"wss://").append(this.OsmAddress).append(":17070/model/").append(ModelUUID).append("/api\", sslopt={\'cert_reqs\': ssl.CERT_NONE})\n");
        sb.append("ws_send(conn, {\"type\":\"Admin\",\"request\":\"Login\",\"version\":3,\"params\":{\"credentials\": \"").append(this.JujuPassword).append("\",\"auth-tag\":\"user-admin\"},\"request-id\":1})\n");
        sb.append("full_status = ws_send(conn, {\"type\":\"Client\",\"request\":\"FullStatus\",\"version\":2,\"request-id\":2})\n");
        sb.append("conn.close()\n");
        sb.append("print(json.dumps(full_status))\n");
        sb.append("EOF\n");

        try {
            ProcessBuilder pb = new ProcessBuilder("/bin/bash");
            Process bash = pb.start();
            PrintStream ps = new PrintStream(bash.getOutputStream());
            ps.println(sb);
            ps.close();
            BufferedReader br = new BufferedReader(new InputStreamReader(bash.getInputStream()));
            try {
                bash.waitFor();
            } catch (java.lang.InterruptedException ex) {
                log.error("InterruptedException in GetModelFullStatus function");
                ex.printStackTrace(System.out);
            }

            StringBuilder res = new StringBuilder();
            String line = "";
            while ((line = br.readLine()) != null) {
                res.append(line);
            }

            br.close();

            try {
                final JSONObject JsonResp = new JSONObject(res.toString());
                return JsonResp.getJSONObject("response");
            } catch (JSONException ex) {
                log.error("JSONException in GetModelFullStatus function");
                ex.printStackTrace(System.out);
            }
        } catch (java.io.IOException ex) {
            log.error("IOException in GetModelFullStatus function");
            ex.printStackTrace(System.out);
        }
        return null;
    }

    public JSONObject GetActionsByApplication(String ModelUUID, String AppName) {
        String AppTag = "application-" + AppName;
        log.debug("Hi From model GetActionsByApplication {} {} {}", ModelUUID, AppName, AppTag);

        StringBuilder sb = new StringBuilder();
        sb.append("python3 <<EOF\n");
        sb.append("import websocket\n");
        sb.append("import ssl\n");
        sb.append("import json\n");
        sb.append("def ws_send(conn, data):\n");
        sb.append("	req = json.dumps(data)\n");
        sb.append("	try:\n");
        sb.append("		conn.send(req)\n");
        sb.append("	except Exception as err:\n");
        sb.append("		return\n");
        sb.append("	try:\n");
        sb.append("		resp = conn.recv()\n");
        sb.append("	except Exception as err:\n");
        sb.append("		return\n");
        sb.append("	return json.loads(resp)\n");
        sb.append("conn = websocket.create_connection(\"wss://").append(this.OsmAddress).append(":17070/model/").append(ModelUUID).append("/api\", sslopt={\'cert_reqs\': ssl.CERT_NONE})\n");
        sb.append("ws_send(conn, {\"type\":\"Admin\",\"request\":\"Login\",\"version\":3,\"params\":{\"credentials\": \"").append(this.JujuPassword).append("\",\"auth-tag\":\"user-admin\"},\"request-id\":1})\n");
        sb.append("ret = ws_send(conn,{\"type\": \"Action\",\"request\":\"ApplicationsCharmsActions\",\"version\":6,\"params\":{\"entities\": [{\"tag\": \"").append(AppTag).append("\"}]},\"request-id\":2})\n");
        sb.append("conn.close()\n");
        sb.append("print(json.dumps(ret))\n");
        sb.append("EOF\n");

        try {
            ProcessBuilder pb = new ProcessBuilder("/bin/bash");
            Process bash = pb.start();
            PrintStream ps = new PrintStream(bash.getOutputStream());
            ps.println(sb);
            ps.close();
            BufferedReader br = new BufferedReader(new InputStreamReader(bash.getInputStream()));
            try {
                bash.waitFor();
            } catch (java.lang.InterruptedException ex) {
                log.error("InterruptedException in GetActionsByApplication function");
                ex.printStackTrace(System.out);
            }

            StringBuilder res = new StringBuilder();
            String line = "";
            while ((line = br.readLine()) != null) {
                res.append(line);
            }

            br.close();

            try {
                final JSONObject JsonResp = new JSONObject(res.toString());
                return JsonResp.getJSONObject("response");
            } catch (JSONException ex) {
                log.error("JSONException in GetActionsByApplication function");
                ex.printStackTrace(System.out);
            }
        } catch (java.io.IOException ex) {
            log.error("IOException in GetActionsByApplication function");
            ex.printStackTrace(System.out);
        }
        return null;
    }

    public JSONObject ExecuteAction(String ModelUUID, String ActionName, String UnitName, String Parameters) {
        log.debug("Hi From model ExecuteAction {} {} {} {}", ModelUUID, ActionName, UnitName, Parameters);
        String UnitTag = "unit-" + UnitName;
        log.debug("Parameters " + Parameters);

        StringBuilder sb = new StringBuilder();
        sb.append("python3 <<EOF\n");
        sb.append("import websocket\n");
        sb.append("import ssl\n");
        sb.append("import json\n");
        sb.append("def ws_send(conn, data):\n");
        sb.append("	req = json.dumps(data)\n");
        sb.append("	try:\n");
        sb.append("		conn.send(req)\n");
        sb.append("	except Exception as err:\n");
        sb.append("		return\n");
        sb.append("	try:\n");
        sb.append("		resp = conn.recv()\n");
        sb.append("	except Exception as err:\n");
        sb.append("		return\n");
        sb.append("	return json.loads(resp)\n");
        sb.append("conn = websocket.create_connection(\"wss://").append(this.OsmAddress).append(":17070/model/").append(ModelUUID).append("/api\", sslopt={\'cert_reqs\': ssl.CERT_NONE})\n");
        sb.append("ws_send(conn, {\"type\":\"Admin\",\"request\":\"Login\",\"version\":3,\"params\":{\"credentials\": \"").append(this.JujuPassword).append("\",\"auth-tag\":\"user-admin\"},\"request-id\":1})\n");
        sb.append("ret = ws_send(conn,{\"type\": \"Action\", \"request\": \"EnqueueOperation\", \"version\": 6, \"params\": {\"actions\": [{\"name\": \"").append(ActionName).append("\", \"receiver\": \"").append(UnitTag).append("\", \"parameters\": {\"filename\": \"/home/ubuntu/fromws\"}}]}, \"request-id\": 2})\n");
        sb.append("conn.close()\n");
        sb.append("print(json.dumps(ret))\n");
        sb.append("EOF\n");

        try {
            ProcessBuilder pb = new ProcessBuilder("/bin/bash");
            Process bash = pb.start();
            PrintStream ps = new PrintStream(bash.getOutputStream());
            ps.println(sb);
            ps.close();
            BufferedReader br = new BufferedReader(new InputStreamReader(bash.getInputStream()));
            try {
                bash.waitFor();
            } catch (java.lang.InterruptedException ex) {
                log.error("InterruptedException in ExecuteAction function");
                ex.printStackTrace(System.out);
            }

            StringBuilder res = new StringBuilder();
            String line = "";
            while ((line = br.readLine()) != null) {
                res.append(line);
            }

            log.info("RES " + res);

            br.close();

            try {
                final JSONObject JsonResp = new JSONObject(res.toString());
                return JsonResp.getJSONObject("response");
            } catch (JSONException ex) {
                log.error("JSONException in ExecuteAction function");
                ex.printStackTrace(System.out);
            }
        } catch (java.io.IOException ex) {
            log.error("IOException in ExecuteAction function");
            ex.printStackTrace(System.out);
        }
        return null;
    }
}
/*
 * RAV API
 * RAV API
 *
 * OpenAPI spec version: 1.0.0-oas3
 * Contact: name@mail.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package it.nextworks.experiment.sbi.rav.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;
/**
 * ConfigurationDict
 */

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2020-04-06T19:10:49.373Z[GMT]")
public class ConfigurationDict {
  @SerializedName("vertical")
  private String vertical = null;

  @SerializedName("expID")
  private String expID = null;

  @SerializedName("perfdiag")
  private boolean perfDiag = false;

  @SerializedName("identifier")
  private String nsInstanceId = null;



  //  @SerializedName("executeID")
//  private String executionID = null;


  @SerializedName("testcases")
  private List<ConfigurationDictTestcases> testcases = new ArrayList<ConfigurationDictTestcases>();

  public ConfigurationDict vertical(String vertical) {
    this.vertical = vertical;
    return this;
  }

   /**
   * Get vertical
   * @return vertical
  **/
  public String getVertical() {
    return vertical;
  }

  public void setVertical(String vertical) {
    this.vertical = vertical;
  }

  public ConfigurationDict expID(String expID) {
    this.expID = expID;
    return this;
  }

   /**
   * Get expID
   * @return expID
  **/
  public String getExpID() {
    return expID;
  }

  public void setExpID(String expID) {
    this.expID = expID;
  }

  public ConfigurationDict testcases(List<ConfigurationDictTestcases> testcases) {
    this.testcases = testcases;
    return this;
  }

  public ConfigurationDict addTestcasesItem(ConfigurationDictTestcases testcasesItem) {
    this.testcases.add(testcasesItem);
    return this;
  }

//  public String getExecutionID() {
//    return executionID;
//  }
//
//  public void setExecutionID(String executionID) {
//    this.executionID = executionID;
//  }
//
//
//  public ConfigurationDict executionId(String executionID){
//    this.executionID = executionID;
//    return this;
//  }
public boolean getPerfDiag() {
  return perfDiag;
}

  public void setPerfDiag(boolean perfDiag) {
    this.perfDiag = perfDiag;
  }

  public String getNsInstanceId() {
    return nsInstanceId;
  }

  public void setNsInstanceId(String nsInstanceId) {
    this.nsInstanceId = nsInstanceId;
  }

   /**
   * Get testcases
   * @return testcases
  **/
  public List<ConfigurationDictTestcases> getTestcases() {
    return testcases;
  }

  public void setTestcases(List<ConfigurationDictTestcases> testcases) {
    this.testcases = testcases;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConfigurationDict configurationDict = (ConfigurationDict) o;
    return Objects.equals(this.vertical, configurationDict.vertical) &&
        Objects.equals(this.expID, configurationDict.expID) &&
        Objects.equals(this.testcases, configurationDict.testcases);
//    &&
//        Objects.equals(this.executionID, configurationDict.executionID);
  }

  @Override
  public int hashCode() {
    return Objects.hash(vertical, expID, testcases);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConfigurationDict {\n");
    
    sb.append("    vertical: ").append(toIndentedString(vertical)).append("\n");
    sb.append("    expID: ").append(toIndentedString(expID)).append("\n");
//    sb.append("    executeID: ").append(toIndentedString(executionID)).append("\n");
    sb.append("    testcases: ").append(toIndentedString(testcases)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

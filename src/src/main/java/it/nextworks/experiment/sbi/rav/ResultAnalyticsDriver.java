package it.nextworks.experiment.sbi.rav;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Response;
import it.nextworks.catalogue.elements.exp.ExpDescriptor;
import it.nextworks.experiment.model.ExperimentExecution;
import it.nextworks.experiment.rabbit.InternalMessage;
import it.nextworks.experiment.rabbit.RunExperimentInternalMessage;
import it.nextworks.experiment.rabbit.ValidationResultInternalMessage;
import it.nextworks.experiment.repository.ExperimentExecutionRepository;
import it.nextworks.experiment.sbi.OpenOnlineCatalogueService;
import it.nextworks.experiment.sbi.enums.ValidationStatus;
import it.nextworks.experiment.sbi.interfaces.ValidatorServiceProviderInterface;
import it.nextworks.experiment.sbi.rav.api.ApiClient;
import it.nextworks.experiment.sbi.rav.api.ValidationApi;
import it.nextworks.experiment.sbi.rav.model.*;
import it.nextworks.lcm.vs.elements.ApplicationMetricRecord;
import it.nextworks.lcm.vs.elements.InfrastructureMetricRecord;
import it.nextworks.lcm.vs.elements.VerticalServiceInstance;
import it.nextworks.nfvmano.libs.ifa.common.elements.Filter;
import it.nextworks.nfvmano.libs.ifa.common.messages.GeneralizedQueryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.math.BigDecimal;
import java.util.*;

public class ResultAnalyticsDriver implements ValidatorServiceProviderInterface {

    // static variable single_instance of type RAVDriver
    private static ResultAnalyticsDriver single_instance = null;

    private static final Logger log = LoggerFactory.getLogger(ResultAnalyticsDriver.class);

    private RabbitTemplate rabbitTemplate;
    private TopicExchange messageExchange;

    private ExperimentExecutionRepository experimentExecutionRepository;
    private OpenOnlineCatalogueService catalogueService;

    private ValidationApi ravApi;
    private String monitoringAddress;
    private String monitoringPort;

    // private constructor restricted to this class itself
    private ResultAnalyticsDriver(String ravURI, String monitoringAddress, String monitoringPort, OpenOnlineCatalogueService catalogueService, ExperimentExecutionRepository experimentExecutionRepository, RabbitTemplate rabbitTemplate, TopicExchange messageExchange) {
        log.debug("Initializing RAV Driver : uri {}", ravURI);

        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath(ravURI);
        apiClient.setConnectTimeout(60000);
        apiClient.setReadTimeout(60000);
        apiClient.setDebugging(true);
        ravApi = new ValidationApi(apiClient);

        this.monitoringAddress = monitoringAddress;
        this.monitoringPort = monitoringPort;
        this.catalogueService = catalogueService;
        this.experimentExecutionRepository = experimentExecutionRepository;
        this.rabbitTemplate = rabbitTemplate;
        this.messageExchange = messageExchange;
    }

    // static method to create instance of RAVDriver class
    public static ResultAnalyticsDriver getInstance(String ravURI, String monitoringAddress, String monitoringPort, OpenOnlineCatalogueService catalogueService, ExperimentExecutionRepository experimentExecutionRepository, RabbitTemplate rabbitTemplate, TopicExchange messageExchange) {
        if (single_instance == null)
            single_instance = new ResultAnalyticsDriver(ravURI, monitoringAddress, monitoringPort, catalogueService, experimentExecutionRepository, rabbitTemplate, messageExchange);
        else
            log.debug("RAV Driver already instantiated: uri {}", ravURI);
        return single_instance;
    }

    @Override
    public void configureExperiment(String experimentId, String executionId, boolean perfDiag, String nsInstanceId, VerticalServiceInstance verticalServiceInstance, String exdId){
        new Thread(() -> {
            configurationImplementation(experimentId, executionId, perfDiag, nsInstanceId, verticalServiceInstance, exdId);
        }).start();
    }

    @Override
    public void startValidation(String experimentId, String executionId){
        new Thread(() -> {
            startValidationImplementation(experimentId, executionId);
        }).start();
    }

    @Override
    public void stopValidation(String experimentId, String executionId){
        new Thread(() -> {
            stopValidationImplementation(experimentId, executionId);
        }).start();
    }

    @Override
    public void queryValidationResult(String experimentId, String executionId){
        new Thread(() -> {
            queryValidationResultImplementation(experimentId, executionId);
        }).start();
    }

    @Override
    public void terminateExperiment(String experimentId, String executionId){
        new Thread(() -> {
            terminationImplementation(experimentId, executionId);
        }).start();
    }

    private void configurationImplementation(String experimentId, String executionId, boolean perfDiag, String nsInstanceId, VerticalServiceInstance verticalServiceInstance, String exdId){
        //TODO configure experiment validation
        // Get experimentDescriptorId from experiment execution id


        // TODO: Topic cannot be related to TCs. Will be sent all of them for each TC (should be changed interface on RAV)
        List<Topic> topics = new ArrayList<>();
        List<Publishtopic> publishTopics = new ArrayList<>();

        log.debug("Started generating application metrics");
        for (ApplicationMetricRecord entry: verticalServiceInstance.getApplicationMetrics()){
            Topic topic = new Topic();
            topic.brokerAddr(monitoringAddress+":"+monitoringPort);
            topic.setMetric(entry.getMetricId());
            topic.setTopic(entry.getTopic());
            log.debug("topic name created for RAV: {}", entry.getTopic());
            topics.add(topic);
        }
        log.debug("Stopped generating application metrics");

        log.debug("Started generating infrastructure metrics");
        for (InfrastructureMetricRecord entry: verticalServiceInstance.getInfrastructureMetrics()){
            Topic topic = new Topic();
            topic.brokerAddr(monitoringAddress+":"+monitoringPort);
            topic.setMetric(entry.getMetricId());
            topic.setTopic(entry.getTopic());
            topic.setCategory("infrastructure");
            log.debug("topic name created for RAV: {}", entry.getTopic());
            topics.add(topic);
        }
        log.debug("Stopped generating infrastructure metrics");

        //log.debug("Start generating application metrics");
        //for (Map.Entry<String, String> entry: expExecution.getKpiMetrics().entrySet()){
        //    for (KeyPerformanceIndicator kpi : expBlueprintResponse.getExpBlueprintInfo().get(0).getExpBlueprint().getKpis()){
        //        if (entry.getKey().equalsIgnoreCase(kpi.getKpiId())){
        //            Publishtopic pt = new Publishtopic();
        //            pt.setBrokerAddr(monitoringAddress+":"+monitoringPort);
        //            pt.setKpi(kpi.getKpiId());
        //            pt.setFormula(kpi.getFormula());
        //            pt.setInterval(new BigDecimal(kpi.getInterval().replaceAll("[^0-9.]+", ""))); //TODO check if converts it in a proper way
        //            List<String> metricsIds = new ArrayList<>();
        //            pt.setUnit(kpi.getUnit());
        //            for (String metric : kpi.getMetricIds()){
        //                metricsIds.add(metric);
        //            }
        //            KpiThreshold kpiThreshold = expDescriptor.getKpiThresholds().get(kpi.getKpiId());
        //            pt.setUpperBound(String.valueOf(kpiThreshold.getUpperBound()));
        //            pt.setLowerBound(String.valueOf(kpiThreshold.getLowerBound()));
        //            pt.setInput(metricsIds);
        //            pt.setTopic(entry.getValue());
        //            log.debug("KPI topics created for RAV: {}", entry.getValue());
        //            publishTopics.add(pt);
        //        }
        //    }
        //}
        //log.debug("Stopped generating kpi metrics");

        ConfigurationDict confDict = new ConfigurationDict();
        //TODO putted fixed value, pls double check
        confDict.setVertical("Smart Transportation");
        confDict.setExpID(experimentId);
        if (perfDiag){
            confDict.setNsInstanceId(nsInstanceId);
            confDict.setPerfDiag(perfDiag);
        }
        //confDict.setExpID(experimentId);
        //confDict.setExecutionID(executionId);  //TODO Confirm with RAV developer
        List<ConfigurationDictTestcases> testCasesListConfig = new ArrayList<ConfigurationDictTestcases>();
        //for (String tcdId : expDescriptor.getTestCaseDescriptorIds()){
        //    ConfigurationDictTestcases confDictTC = new ConfigurationDictTestcases();
        //    confDictTC.setTcID(tcdId);
        //    ModelConfiguration model = new ModelConfiguration();
        //    model.setPublish(publishTopics);
        //    model.setTopics(topics);
        //    confDictTC.setConfiguration(model);
        //    testCasesListConfig.add(confDictTC);
        //}

        ConfigurationDictTestcases confDictTC = new ConfigurationDictTestcases();
        confDictTC.setExdId(exdId);
        ModelConfiguration model = new ModelConfiguration();
        model.setPublish(publishTopics);
        model.setTopics(topics);
        confDictTC.setTcID("tcId"); //Todo this is fixed but we should doublecheck
        confDictTC.setConfiguration(model);
        testCasesListConfig.add(confDictTC);

        confDict.setTestcases(testCasesListConfig);

        try {
            log.info("Calling Configure Experiment to RAV");
            Call call = ravApi.setConfigurationExpCall(confDict, experimentId, null, null);
            Response response = call.execute();
            if (response.code() == 200) {
                log.info("Calling Start Testcase Validation Experiment to RAV");
                Call callValidation = ravApi.startTestcaseValidationCall(experimentId, "tcId", null, null);
                Response responseValidation = callValidation.execute();
                if (responseValidation.code() == 200) {
                    String topic = "lifecycle.run." + executionId;
                    InternalMessage internalMessage = new RunExperimentInternalMessage();
                    try {
                        sendMessageToQueue(internalMessage, topic);
                    } catch (JsonProcessingException e) {
                        log.error("Error while translating internal scheduling message in Json format");
                        manageValidationError("Error while translating internal scheduling message in Json format", executionId);
                    }
                    //String configuration = "OK";
                    //String topic = "lifecycle.validation." + executionId;
                    //InternalMessage internalMessage = new ValidationResultInternalMessage(ValidationStatus.CONFIGURED, "Validation ok", false);
                    //try {
                    //    sendMessageToQueue(internalMessage, topic);
                    //} catch (JsonProcessingException e) {
                    //    log.error("Error while translating internal scheduling message in Json format", e);
                    //    manageValidationError("EEM: Error while translating internal scheduling message in Json format", executionId);
                    //}
                }

            } else {
                log.error("Status code {} on configure validation of execution {}", response.code(), executionId);
                manageValidationError("RAV: Status code "+ response.code() +" on configure validation of execution " + executionId, executionId);
            }
        } catch (Exception e5){
            log.error("Configuration action of execution {} failed", executionId, e5);
            manageValidationError("EEM: Configuration action of execution " + executionId + " failed", executionId);
        }

    }

    private void startValidationImplementation(String experimentId, String executionId){
        log.info("Starting new validation task for execution {} ", executionId);

//        try {
//            Call call = ravApi.startTestcaseValidationCall(executionId, tcDescriptorId, null, null);
//            Response response = call.execute();
//            if (response.code() != 200){
//                log.error("Status code {} on start validation of execution {} and tcDescriptor {}", response.code(), executionId, tcDescriptorId);
//                manageValidationError("RAV: Status code "+ response.code() +" on start validation of execution " + executionId + " and tcDescriptor " + tcDescriptorId, executionId);
//                return;
//            }
//        } catch (Exception e) {
//            log.error("Exception on start validation of execution {} and tcDescriptor {}", executionId, tcDescriptorId, e);
//            manageValidationError("EEM: Exception on start validation of execution " + executionId + " and tcDescriptor " + tcDescriptorId, executionId);
//            return;
//        }
//
//        String validationStarted = "OK";
//        String topic = "lifecycle.validation." + executionId;
//        InternalMessage internalMessage = new ValidationResultInternalMessage(ValidationStatus.ACQUIRING, validationStarted, false);
//        try {
//            sendMessageToQueue(internalMessage, topic);
//        } catch (JsonProcessingException e) {
//            log.error("Error while translating internal scheduling message in Json format", e);
//            manageValidationError("EEM: Error while translating internal scheduling message in Json format", executionId);
//        }
    }

    private void stopValidationImplementation(String experimentId, String executionId){
        log.info("Stopping validation task for execution {} ", executionId);
        //TODO stop TC validation
//        try {//TODO remove
//            Call call = ravApi.terminateCurrentTestcaseCall(executionId, tcDescriptorId, null, null);
//            Response response = call.execute();
//            if (response.code() != 200){
//                log.error("Status code {} on stop validation of execution {} and tcDescriptor {}", response.code(), executionId, tcDescriptorId);
//                manageValidationError("RAV: Status code "+ response.code() +" on stop validation of execution " + executionId + " and tcDescriptor " + tcDescriptorId, executionId);
//                return;
//            }
//        } catch (Exception e) {
//            log.error("Exception on stop validation of execution {} and tcDescriptor {}", executionId, tcDescriptorId, e);
//            manageValidationError("EEM: Exception on stop validation of execution " + executionId + " and tcDescriptor " + tcDescriptorId, executionId);
//            return;
//        }
//
//        String validationStarted = "OK";
//        String topic = "lifecycle.validation." + executionId;
//        InternalMessage internalMessage = new ValidationResultInternalMessage(ValidationStatus.VALIDATING, validationStarted, false);
//        try {
//            sendMessageToQueue(internalMessage, topic);
//        } catch (JsonProcessingException e) {
//            log.error("Error while translating internal scheduling message in Json format", e);
//            manageValidationError("EEM: Error while translating internal scheduling message in Json format", executionId);
//        }
//        //validation not stopped
//        //manageValidationError();
    }

    private void queryValidationResultImplementation(String experimentId, String executionId){
        log.info("Querying validation task for execution {} ", executionId);
//          StatusResponse statusResponse = null;
////        try {
////            Call call = ravApi.startTestcaseValidationCall(executionId, tcDescriptorId, null, null);
////            Response response = call.execute();
////            if (response.code() != 200){
////                log.error("Failed to start validation of execution {} and tcDiD {}: Response code  received {}", executionId, tcDescriptorId, response.code());
////                manageValidationError("Failed to start validation of execution " + executionId + " and tcDiD " + tcDescriptorId + ": Response code received " + response.code() , executionId);
////            }
////        } catch(Exception e1){
////            log.error("Failed to start validation of execution {} and tcDiD {}", executionId, tcDescriptorId);
////            e1.getMessage();
////            manageValidationError("Error while translating internal scheduling message in Json format", executionId);
////        }
//
//            try {
//                Thread.sleep(5000);
//                statusResponse = ravApi.showTestcaseValidationStatus(executionId, tcDescriptorId);//TODO add check on status code
//            } catch (Exception e){
//                log.error("Error trying to validate execution {} and tcDescriptor {}", executionId, tcDescriptorId, e);
//                manageValidationError("EEM: Error trying to validate execution " + executionId + " and tcDescriptor " + tcDescriptorId, executionId);
//                return;
//            }
//
//        //TODO insert some delay in performing queries..Every time EEM receives a VALIDATING message, it sends immediately a new queryValidationResult
//
//        //TODO remove, handled via Notification Endpoint
//        ValidationStatus validationStatus;
//        String reportUrl = null;
//        String topic = "lifecycle.validation." + executionId;
//        //if VALIDATING
//        if(! statusResponse.getStatus().equalsIgnoreCase("VALIDATED"))
//            validationStatus = ValidationStatus.VALIDATING;
//        else{
//            validationStatus = ValidationStatus.VALIDATED;
//            reportUrl = statusResponse.getReport();
//        }
//        InternalMessage internalMessage = new ValidationResultInternalMessage(validationStatus, reportUrl, false);
//        try {
//            sendMessageToQueue(internalMessage, topic);
//        } catch (JsonProcessingException e) {
//            log.error("Error while translating internal scheduling message in Json format", e);
//            manageValidationError("EEM: Error while translating internal scheduling message in Json format", executionId);
//        }

    }

    private void terminationImplementation(String experimentId, String executionId){
        Call calltcb = null;
        try{
            calltcb = ravApi.terminateCurrentTestcaseCall(experimentId, "tcId", null, null);
            Response responsetcb = calltcb.execute();
            if (responsetcb.code() == 200) {
                log.debug("Validation of execution {} terminated correctly", executionId);
            } else {
                log.error("RAV server replied with an error code {} when terminating validation of execution {}", responsetcb.code(), executionId);
            }
        } catch(Exception e){
            log.error("Something went wrong terminating experiment validation identified by {}", executionId, e);
        }



        Call call = null;
        try{
            call = ravApi.terminateExperimentCall(experimentId, null, null);
            Response response = call.execute();
            if (response.code() == 200) {
                log.debug("Validation of execution {} terminated correctly", executionId);
            } else {
                log.error("RAV server replied with an error code {} when terminating validation of execution {}", response.code(), executionId);
            }
        } catch(Exception e){
            log.error("Something went wrong terminating experiment validation identified by {}", executionId, e);
        }
        String topic = "lifecycle.validation." + executionId;
        InternalMessage internalMessage = new ValidationResultInternalMessage(ValidationStatus.VALIDATED, "Validated", false);
        try {
            sendMessageToQueue(internalMessage, topic);
        } catch (JsonProcessingException e) {
            log.error("Error while translating internal scheduling message in Json format");
            log.debug(null, e);
        }
    }

    private void manageValidationError(String errorMessage, String executionId){
        log.error("Validation of Experiment Execution with Id {} failed : {}", executionId, errorMessage);
        errorMessage = String.format("Validation of Experiment Execution with Id %s failed : %s", executionId, errorMessage);
        String topic = "lifecycle.validation." + executionId;
        InternalMessage internalMessage = new ValidationResultInternalMessage(ValidationStatus.FAILED, errorMessage, true);
        try {
            sendMessageToQueue(internalMessage, topic);
        } catch (JsonProcessingException e) {
            log.error("Error while translating internal scheduling message in Json format", e);
        }
    }

    private void sendMessageToQueue(InternalMessage msg, String topic) throws JsonProcessingException {
        ObjectMapper mapper = buildObjectMapper();
        String json = mapper.writeValueAsString(msg);
        rabbitTemplate.convertAndSend(messageExchange.getName(), topic, json);
    }

    private ObjectMapper buildObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }
}

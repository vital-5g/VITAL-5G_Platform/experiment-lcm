package it.nextworks.experiment.sbi;

import it.nextworks.lcm.vs.elements.VerticalServiceInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.nextworks.experiment.configuration.ConfigurationParameters;
import it.nextworks.experiment.repository.ExperimentExecutionRepository;
import it.nextworks.experiment.sbi.dummyDrivers.DummyValidatorDriver;
import it.nextworks.experiment.sbi.enums.ValidatorType;
import it.nextworks.experiment.sbi.interfaces.ValidatorServiceProviderInterface;
import it.nextworks.experiment.sbi.rav.ResultAnalyticsDriver;

import javax.annotation.PostConstruct;
import java.net.URISyntaxException;

@Service
public class ValidatorService implements ValidatorServiceProviderInterface {

    private static final Logger log = LoggerFactory.getLogger(ValidatorService.class);

    private ValidatorServiceProviderInterface driver;

    @Value("${validator.type}")
    private ValidatorType validatorType;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    @Qualifier(ConfigurationParameters.elcmQueueExchange)
    private TopicExchange messageExchange;

    @Autowired
    private ExperimentExecutionRepository experimentExecutionRepository;

    @Autowired
    private OpenOnlineCatalogueService catalogueService;

    @Value("${monitoring.address}")
    private String monitoringAddress;

    @Value("${monitoring.port}")
    private String monitoringPort;

    @Value("${rav.uri}")
    private String ravURI;

    @PostConstruct
    public void init() throws URISyntaxException {
        log.debug("Initializing Validator driver");
        if (validatorType.equals(ValidatorType.RAV))
            this.driver = ResultAnalyticsDriver.getInstance(ravURI, monitoringAddress, monitoringPort, catalogueService, experimentExecutionRepository, rabbitTemplate, messageExchange);
        else if (validatorType.equals(ValidatorType.DUMMY))
            this.driver = new DummyValidatorDriver(rabbitTemplate, messageExchange, experimentExecutionRepository);
        else
            log.error("Wrong configuration for Executor service.");
    }

    @Override
    public void configureExperiment(String experimentId, String executionId, boolean perfDiag, String nsInstanceId, VerticalServiceInstance verticalServiceInstance, String exdId){
        driver.configureExperiment(experimentId, executionId, perfDiag, nsInstanceId, verticalServiceInstance, exdId);
    }


    @Override
	public void startValidation(String experimentId, String executionId) {
		driver.startValidation(experimentId, executionId);
		
	}

	@Override
	public void stopValidation(String experimentId, String executionId) {
		driver.stopValidation(experimentId, executionId);
		
	}

	@Override
	public void queryValidationResult(String experimentId, String executionId) {
		driver.queryValidationResult(experimentId, executionId);
		
	}

	@Override
	public void terminateExperiment(String experimentId, String executionId) {
		driver.terminateExperiment(experimentId, executionId);
		
	}


}

package it.nextworks.experiment.sbi.dummyDrivers;


import it.nextworks.catalogue.elements.*;
import it.nextworks.catalogue.elements.exp.*;
import it.nextworks.catalogue.elements.translator.NfvNsInstantiationInfo;
import it.nextworks.catalogue.exceptions.*;
import it.nextworks.catalogue.messages.OnboardVSBRequest;
import it.nextworks.experiment.sbi.interfaces.OpenOnlineCatalogueProviderInterface;
import it.nextworks.catalogue.elements.tc.TestCaseBlueprint;
import it.nextworks.catalogue.elements.tc.TestCaseBlueprintInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DummyOpenOnlineCatalogueDriver implements OpenOnlineCatalogueProviderInterface {

    private static final Logger log = LoggerFactory.getLogger(DummyOpenOnlineCatalogueDriver.class);

    public DummyOpenOnlineCatalogueDriver() {
        log.debug("Initializing Dummy Validator Driver");

    }

	public ExpDescriptor getExperimentDescriptor(String expDiD) {
		log.info("Experiment Descriptor with id {} is found", expDiD);
		ExpDescriptor expDescr = new ExpDescriptor(null, "ExpDescriptor name", "1", UUID.randomUUID(), null, null, null, null, null, null);
		log.debug("ExpDescripor Content: {}", expDescr.toString());
		return expDescr;
	}

	//public ExpBlueprint getExperimentBlueprint(String expBId) {
	//	log.info("Experiment Blueprint with id {} is found", expBId);
	//	List<InfrastructureMetric> iml = new ArrayList<InfrastructureMetric>();
	//	KeyPerformanceIndicator kpi = new KeyPerformanceIndicator(new ExpBlueprint(), "KPI_1", "KPI_1", null, "ms", null, null, MetricGraphType.COUNTER);
	//	List<KeyPerformanceIndicator> kpis = new ArrayList<KeyPerformanceIndicator>();
	//	kpis.add(kpi);
	//	InfrastructureMetric im = new InfrastructureMetric(null,null, "METRIC_ID", "METRIC_ID", MetricCollectionType.GAUGE, expBId, null, null, null, null );
	//	iml.add(im);
	//	ExpBlueprint expBlueprint = new ExpBlueprint(null, "1", "expDummy", "Exp dummy", null, null, null, iml, null, null);
	//	log.debug("{}", expBlueprint.toString());
	//	return expBlueprint;
	//}

	public VsDescriptor getVsd(String vsdId) {
		log.info("Vertical Service descriptor with id {} is found", vsdId);
		VsDescriptor vsd = new VsDescriptor(
			null, //vsdinfo
			"VSD_TEST", //name
			"1.0", //version 
			null, //vsBlueprintId
			null, //qosParameters 
			null, //accessLevel 
			null //tenantID
			);
		log.debug("{}", vsd.toString());
		return vsd;
	}

	public VerticalServiceBlueprint getVsb(String vsbId) {
		log.info("Vertical Service Blueprint with id {} is found", vsbId);
		VerticalServiceBlueprint vsb = new VerticalServiceBlueprint(
				null, "VSB_TEST", null, "DESCRIPTION VSB", null, null, null, null, AccessLevel.RESTRICTED, null, null, null, null, null, null, null,null);
		log.debug("{}", vsb.toString());
		return vsb;
	}

	@Override
	public UUID onboardExperimentBlueprint(ExpBlueprint vsb) throws AlreadyExistingEntityException, MalformattedElementException, FailedOperationException, UnAuthorizedRequestException, NotExistingEntityException {
		return null;
	}

	@Override
	public ExpBlueprint getExperimentBlueprint(UUID expbId) throws NotExistingEntityException, MalformattedElementException {
		return null;
	}

	@Override
	public List<ExpBlueprintInfo> queryExperimentBlueprint(Testbed testbed) throws MalformattedElementException, FailedOperationException {
		return null;
	}

	@Override
	public List<ExpBlueprintInfo> getAllExperimentBlueprints() {
		return null;
	}

	@Override
	public void deleteExperimentBlueprint(UUID expbId, boolean force) throws FailedOperationException, UnAuthorizedRequestException, NotExistingEntityException, MalformattedElementException {

	}

	@Override
	public List<ExpBlueprintInfo> getExperimentBlueprintsByTestCaseBlueprint(UUID testCaseBlueprintId) {
		return null;
	}

	@Override
	public UUID onboardExpDescriptor(ExpDescriptor request) throws MalformattedElementException, AlreadyExistingEntityException, FailedOperationException, NotExistingEntityException {
		return null;
	}

	@Override
	public List<ExpDescriptorInfo> queryExpDescriptor() throws MalformattedElementException, NotExistingEntityException, FailedOperationException {
		return null;
	}

	@Override
	public ExpDescriptor getExpDescriptor(UUID expdId) throws MalformattedElementException, NotExistingEntityException {
		return null;
	}

	@Override
	public void deleteExpDescriptor(UUID expDescriptorId, boolean force) throws MalformattedElementException, NotExistingEntityException, FailedOperationException {

	}

	@Override
	public void useExpDescriptor(UUID expDescriptorId, UUID experimentId) throws MalformattedElementException, NotExistingEntityException, FailedOperationException {

	}

	@Override
	public void releaseExpDescriptor(UUID expDescriptorId, UUID experimentId) throws MalformattedElementException, NotExistingEntityException, FailedOperationException {

	}

	@Override
	public UUID onboardNetAppPackage(File file) throws AlreadyExistingEntityException, MalformattedElementException, UnAuthorizedRequestException, FailedOperationException {
		return null;
	}

	@Override
	public UUID onboardNetAppPackage(File file, File vnfPackage) throws AlreadyExistingEntityException, MalformattedElementException, UnAuthorizedRequestException, FailedOperationException {
		return null;
	}

	@Override
	public NetAppBlueprint getNetAppBlueprint(UUID netAppPackageId) throws UnAuthorizedRequestException, NotExistingEntityException, FailedOperationException {
		return null;
	}

	@Override
	public File getNetAppPackage(UUID netAppPackageId) throws UnAuthorizedRequestException, NotExistingEntityException, FailedOperationException {
		return null;
	}

	@Override
	public List<NetAppPackageInfo> queryNetAppPackages(Testbed testbed, AccessLevel accessLevel, NetAppSpecLevel specLevel) {
		return null;
	}

	@Override
	public List<NetAppPackageInfo> getAllNetAppPackages() {
		return null;
	}

	@Override
	public void deleteNetAppPackage(UUID netAppPackageId, boolean forced) throws UnAuthorizedRequestException, NotExistingEntityException, FailedOperationException {

	}

	@Override
	public File getNetAppPackageSoftwareDoc(UUID netAppPackageId, String softwareDocId) throws UnAuthorizedRequestException, NotExistingEntityException, FailedOperationException {
		return null;
	}

	@Override
	public NfvNsInstantiationInfo translateVsd(UUID vsdId) throws NotExistingEntityException, FailedOperationException {
		return null;
	}

	@Override
	public UUID onboardVsBlueprint(OnboardVSBRequest vsb, File nsd) throws AlreadyExistingEntityException, MalformattedElementException, FailedOperationException, UnAuthorizedRequestException, NotExistingEntityException {
		return null;
	}

	@Override
	public VerticalServiceBlueprint getVerticalServiceBlueprint(UUID vsbId) throws NotExistingEntityException, MalformattedElementException, FailedOperationException, UnAuthorizedRequestException {
		return null;
	}

	@Override
	public List<VerticalServiceBlueprintInfo> queryVerticalServiceBlueprint(Testbed testbed, AccessLevel accessLevel) throws MalformattedElementException {
		return null;
	}

	@Override
	public List<VerticalServiceBlueprintInfo> getAllVerticalServiceBlueprints() throws FailedOperationException {
		return null;
	}

	@Override
	public void deleteVsBlueprint(UUID vsbId, boolean force) throws FailedOperationException, UnAuthorizedRequestException, NotExistingEntityException {

	}

	@Override
	public UUID onBoardVsDescriptor(VsDescriptor request) throws MalformattedElementException, AlreadyExistingEntityException, FailedOperationException, UnAuthorizedRequestException {
		return null;
	}

	@Override
	public List<VsDescriptor> queryVsDescriptor() throws MalformattedElementException, FailedOperationException {
		return null;
	}

	@Override
	public VsDescriptor getVsd(UUID vsdId) throws NotExistingEntityException, FailedOperationException, MalformattedElementException, UnAuthorizedRequestException {
		return null;
	}

	@Override
	public void deleteVsDescriptor(UUID vsDescriptorId, boolean force) throws MalformattedElementException, NotExistingEntityException, FailedOperationException, UnAuthorizedRequestException {

	}

	@Override
	public void useVsDescriptor(UUID vsdId, UUID vsiId) throws FailedOperationException, NotExistingEntityException, MalformattedElementException {

	}

	@Override
	public void releaseVsDescriptor(UUID vsdId, UUID vsiId) throws FailedOperationException, NotExistingEntityException, MalformattedElementException {

	}

	@Override
	public TestCaseBlueprint getTestCaseBlueprint(UUID tcbId) throws MalformattedElementException, NotExistingEntityException, UnAuthorizedRequestException {
		return null;
	}

	@Override
	public void deleteTcBlueprint(UUID testCaseBlueprintId, boolean force)
			throws MalformattedElementException, NotExistingEntityException, FailedOperationException, UnAuthorizedRequestException {
	}

	@Override
	public List<TestCaseBlueprintInfo> queryTcBlueprint()
			throws MalformattedElementException, NotExistingEntityException, FailedOperationException {
		return null;
	}

	@Override
	public UUID onboardTcBlueprint(TestCaseBlueprint request)
			throws MalformattedElementException, AlreadyExistingEntityException, FailedOperationException, NotExistingEntityException {
		return null;
	}
}

package it.nextworks.experiment.sbi.enums;

public enum ValidationStatus {
    CONFIGURED,
    ACQUIRING,
    VALIDATING,
    VALIDATED,
    FAILED
}

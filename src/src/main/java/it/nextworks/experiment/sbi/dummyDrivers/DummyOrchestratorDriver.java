package it.nextworks.experiment.sbi.dummyDrivers;

import it.nextworks.experiment.sbi.interfaces.SiteOrchestratorProviderInterface;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.common.messages.GeneralizedQueryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DummyOrchestratorDriver implements SiteOrchestratorProviderInterface {

    private static final Logger log = LoggerFactory.getLogger(DummyOrchestratorDriver.class);

    public DummyOrchestratorDriver() {
        log.debug("Initializing Dummy Orchestrator Driver");
    }

//    @Override
//    public NsInstance queryNs(GeneralizedQueryRequest request) throws FailedOperationException, MalformattedElementException{
//        return new NsInstance();
//    }
    
	  @Override
	  public void queryNs() throws FailedOperationException, MalformattedElementException{
		  log.debug("Querying network service from dummy Orchestrator service done");
	  }
}

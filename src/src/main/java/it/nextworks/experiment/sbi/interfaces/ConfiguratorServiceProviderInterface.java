package it.nextworks.experiment.sbi.interfaces;


import it.nextworks.experiment.sbi.interfaces.model.NfvoLcmActionExecution;

public interface ConfiguratorServiceProviderInterface {

    void applyConfiguration(String executionId, NfvoLcmActionExecution actionExecution);

    void resetConfiguration(String executionId, NfvoLcmActionExecution actionExecution);
}

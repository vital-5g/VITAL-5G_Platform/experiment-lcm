package it.nextworks.experiment.sbi.vslcmv2;

import it.nextworks.catalogue.elements.VerticalServiceBlueprint;
import it.nextworks.experiment.sbi.cataloguev2.Vital5GCatalogueRestClient;
import it.nextworks.experiment.sbi.interfaces.VerticalServiceLcmProviderInterface;
import it.nextworks.lcm.vs.elements.VerticalServiceInstance;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;


public class VsLcmAuthRestClient implements VerticalServiceLcmProviderInterface {


    private String slcmUrl;
    private static final Logger log = LoggerFactory.getLogger(VsLcmAuthRestClient.class);


    private KeycloakRestTemplate restTemplate;

    public VsLcmAuthRestClient(String slcmUrl, KeycloakRestTemplate krt) {
        this.restTemplate = krt;
        this.slcmUrl = slcmUrl;
    }

    @Override
    public VerticalServiceInstance getVerticalServiceInstance(String vsiId) {
        log.debug("Building HTTP request for querying VS Instance with ID " + vsiId);
        HttpHeaders header = new HttpHeaders();
        header.add("Content-Type", "application/json");

        HttpEntity<?> getEntity = new HttpEntity<>(header);

        String url = slcmUrl + "/vslcm/" + vsiId;

        log.debug("Sending HTTP request to retrieve VS blueprint. URL:" + url);

        ResponseEntity<VerticalServiceInstance> httpResponse =
                restTemplate.exchange(url, HttpMethod.GET, getEntity, VerticalServiceInstance.class);

        log.debug("Response code: " + httpResponse.getStatusCode().toString());
        HttpStatus code = httpResponse.getStatusCode();

        if (code.equals(HttpStatus.OK)) {
            log.debug("VS Instance correctly retrieved");
            return httpResponse.getBody();
        }
        return null;
    }

    @Override
    public void useVerticalServiceInstance(String vsiId, String experimentLcmId) {

    }

    @Override
    public void releaseVerticalServiceInstance(String vsiId, String experimentLcmId) {

    }
}

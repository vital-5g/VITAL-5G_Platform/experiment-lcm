package it.nextworks.experiment.sbi.dummyDrivers;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import it.nextworks.experiment.configuration.ConfigurationParameters;
import it.nextworks.experiment.model.ExperimentExecution;
import it.nextworks.experiment.model.ExperimentLcm;
import it.nextworks.experiment.rabbit.CleaningResultInternalMessage;
import it.nextworks.experiment.rabbit.ExecutionResultInternalMessage;
import it.nextworks.experiment.rabbit.InternalMessage;
import it.nextworks.experiment.rabbit.ConfigurationResultInternalMessage;
import it.nextworks.experiment.repository.ExperimentExecutionRepository;
import it.nextworks.experiment.repository.ExperimentLcmRepository;
import it.nextworks.experiment.sbi.enums.ConfigurationStatus;
import it.nextworks.experiment.sbi.interfaces.ConfiguratorServiceProviderInterface;

import it.nextworks.experiment.sbi.interfaces.ExecutorServiceProviderInterface;
import it.nextworks.experiment.sbi.interfaces.model.NfvoLcmActionExecution;
import it.nextworks.experiment.sbi.mano.interfaces.PollingManagerInterface;
import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.NfvoLcmOperationType;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.OperationStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Optional;
import java.util.UUID;

@Service
public class DummyConfiguratorDriver implements ConfiguratorServiceProviderInterface, ExecutorServiceProviderInterface {

    private static final Logger log = LoggerFactory.getLogger(DummyConfiguratorDriver.class);
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    @Qualifier(ConfigurationParameters.elcmQueueExchange)
    private TopicExchange messageExchange;

    @Autowired
    private ExperimentExecutionRepository experimentExecutionRepository;

    @Autowired
    private ExperimentLcmRepository elcmRepository;

    @Autowired
    private PollingManagerInterface pollingManager;

    //public DummyConfiguratorDriver(RabbitTemplate rabbitTemplate, TopicExchange messageExchange) {
    //    log.debug("Initializing Dummy Configurator Driver");
    //    this.rabbitTemplate = rabbitTemplate;
    //    this.messageExchange = messageExchange;
    //}


    @PostConstruct
    void init() {
        log.info("Init Dummy Configurator Driver");
        pollingManager = new DummyPollingManager(rabbitTemplate, messageExchange, experimentExecutionRepository);
    }

    @Override
    public void applyConfiguration(String executionId, NfvoLcmActionExecution nfvoLcmActionExecution){
        Optional<ExperimentExecution> experimentExecutionOptional = experimentExecutionRepository.findByExecutionId(executionId);
        if (!experimentExecutionOptional.isPresent()) {
            log.error("Unable to find Experiment execution from executionId {}", executionId);
        }
        ExperimentExecution experimentExecution = experimentExecutionOptional.get();
        String experimentLcmId = experimentExecution.getExperimentLcmId();
        ExperimentLcm experimentLcm = elcmRepository.findByElcmId(experimentLcmId).get();

        log.info("Dummy Configuration Driver: adding dummy config operation");
        String operationId = UUID.randomUUID().toString();
        experimentExecution.setCurrentOperationId(operationId);
        experimentExecutionRepository.saveAndFlush(experimentExecution);

        pollingManager.addOperation(operationId, OperationStatus.SUCCESSFULLY_DONE, experimentLcm.getNetworkServiceInstanceId(),
                NfvoLcmOperationType.DAY2_CONFIG, Testbed.valueOf(experimentLcm.getTestbed().toString()));
    }


    @Override
    public void resetConfiguration(String executionId, NfvoLcmActionExecution actionExecution){
        Optional<ExperimentExecution> experimentExecutionOptional = experimentExecutionRepository.findByExecutionId(executionId);
        if (!experimentExecutionOptional.isPresent()) {
            log.error("Unable to find Experiment execution from executionId {}", executionId);
        }
        ExperimentExecution experimentExecution = experimentExecutionOptional.get();
        String experimentLcmId = experimentExecution.getExperimentLcmId();
        ExperimentLcm experimentLcm = elcmRepository.findByElcmId(experimentLcmId).get();

        log.info("Dummy Configuration Driver: adding dummy clean operation");
        String operationId = UUID.randomUUID().toString();
        experimentExecution.setCurrentOperationId(operationId);
        experimentExecutionRepository.saveAndFlush(experimentExecution);

        pollingManager.addOperation(operationId, OperationStatus.SUCCESSFULLY_DONE, experimentLcm.getNetworkServiceInstanceId(),
                NfvoLcmOperationType.DAY2_CLEAN, Testbed.valueOf(experimentLcm.getTestbed().toString()));
    }

    private void sendMessageToQueue(InternalMessage msg, String topic) throws JsonProcessingException {
        ObjectMapper mapper = buildObjectMapper();
        String json = mapper.writeValueAsString(msg);
        rabbitTemplate.convertAndSend(messageExchange.getName(), topic, json);
    }

    private ObjectMapper buildObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }

    private void manageConfigurationError(String errorMessage, String executionId){
        log.error("Configuration of Experiment Execution with Id {} failed : {}", executionId, errorMessage);
        errorMessage = String.format("Configuration of Experiment Execution with Id %s failed : %s", executionId, errorMessage);
        String topic = "lifecycle.configurationResult." + executionId;
        InternalMessage internalMessage = new ConfigurationResultInternalMessage(ConfigurationStatus.FAILED, errorMessage, null, true);
        try {
            sendMessageToQueue(internalMessage, topic);
        } catch (JsonProcessingException e) {
            log.error("Error while translating internal scheduling message in Json format");
            log.debug(null, e);
        }
    }

    private void manageExecutionError(String errorMessage, String executionId) {

    }

    @Override
    public void runTestCase(String executionId, NfvoLcmActionExecution actionExecution) {
        Optional<ExperimentExecution> experimentExecutionOptional = experimentExecutionRepository.findByExecutionId(executionId);
        if (!experimentExecutionOptional.isPresent()) {
            log.error("Unable to find Experiment execution from executionId {}", executionId);
        }
        ExperimentExecution experimentExecution = experimentExecutionOptional.get();
        String experimentLcmId = experimentExecution.getExperimentLcmId();
        ExperimentLcm experimentLcm = elcmRepository.findByElcmId(experimentLcmId).get();

        log.info("Dummy Configuration Driver: adding dummy run operation");
        String operationId = UUID.randomUUID().toString();
        experimentExecution.setCurrentOperationId(operationId);
        experimentExecutionRepository.saveAndFlush(experimentExecution);

        pollingManager.addOperation(operationId, OperationStatus.SUCCESSFULLY_DONE, experimentLcm.getNetworkServiceInstanceId(),
                NfvoLcmOperationType.DAY2_RUN, Testbed.valueOf(experimentLcm.getTestbed().toString()));
    }

    @Override
    public void abortTestCase(String executionId, String tcDescriptorId) {

    }
}

/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.experiment.sbi.mano.polling;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import it.nextworks.experiment.configuration.ConfigurationParameters;
import it.nextworks.experiment.manager.ExperimentExecutionInstanceManager;
import it.nextworks.experiment.model.ExperimentExecution;
import it.nextworks.experiment.rabbit.ConfigurationResultInternalMessage;
import it.nextworks.experiment.rabbit.InternalMessage;
import it.nextworks.experiment.repository.ExperimentExecutionRepository;
import it.nextworks.experiment.sbi.enums.ConfigurationStatus;
import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.exceptions.FailedOperationException;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.*;
import it.nextworks.experiment.sbi.mano.NfvoLcmService;
import it.nextworks.experiment.sbi.mano.interfaces.NfvoLcmNotificationConsumerInterface;
import it.nextworks.experiment.sbi.mano.interfaces.NfvoLcmNotificationInterface;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;


/**
 * This class handles all the LCM notifications received from the NFVO.
 * 
 * @author nextworks
 *
 */
@Service
public class NfvoLcmNotificationsManager implements NfvoLcmNotificationInterface {

	private static final Logger log = LoggerFactory.getLogger(NfvoLcmNotificationsManager.class);
	
	@Autowired
	private NfvoLcmNotificationConsumerInterface engine;
	
	@Autowired
	private NfvoLcmService nfvoLcmService;


	@Override
	public void notifyNetworkServiceStatusChange(InternalNsLifecycleChangeNotification notification) {
		log.debug("Received notification from NFVO driver about a change in the NFV NS lifecycle");
		String nfvNsId = notification.getNsInstanceId();
		String operationId = notification.getOperationId();
		NfvoLcmOperationType operation = notification.getOperationType();
		log.debug("Notification about operation  with ID " + operationId + " related to NFV NS instance " + nfvNsId);
		NfvoLcmNotificationType notificationType = notification.getLcmNotificationType();
		if (notificationType == NfvoLcmNotificationType.LIFECYCLE_OPERATION_START) {
			log.debug("Notification about operation starting. This message is not processed");
			return;
		} else if (notificationType == NfvoLcmNotificationType.LIFECYCLE_OPERATION_RESULT) {
			log.debug("Notification about operation result. Going to retrieve the status of the operation to verify the status.");
			try {
				OperationStatus operationStatus = nfvoLcmService.getOperationStatus(operationId, notification.getTestbed());
				boolean successful = true;
				if (operationStatus == OperationStatus.FAILED) {
					log.error("The operation has failed on the NFVO.");
					successful = false;
				}

				NetworkServiceStatusChange changeType = readChangeType(operation);
				log.debug("Forwarding the notification to the engine.");
				engine.notifyNfvNsStatusChange(nfvNsId, operationId, changeType, successful, notification.getTestbed());
			} catch (  FailedOperationException e) {
				log.error("Error while trying to get operation status: " + e.getMessage());
				log.error("Sending a message about a failure at the NFVO.");
				NetworkServiceStatusChange changeType = readChangeType(operation);
				engine.notifyNfvNsStatusChange(nfvNsId, operationId, changeType, false, notification.getTestbed());
			}
		} else {
			log.error("Notification type not supported.");
		}
	}

	private NetworkServiceStatusChange readChangeType(NfvoLcmOperationType operation) {
		log.debug("Change type for operation:"+operation);
		NetworkServiceStatusChange changeType = NetworkServiceStatusChange.NOT_SPECIFIED;
		if (operation.equals(NfvoLcmOperationType.NS_INSTANTIATION)) changeType=NetworkServiceStatusChange.NS_CREATED;
		else if (operation.equals(NfvoLcmOperationType.NS_TERMINATION)) changeType=NetworkServiceStatusChange.NS_TERMINATED;
		else if (operation.equals(NfvoLcmOperationType.NS_SCALING)) changeType=NetworkServiceStatusChange.NS_MODIFIED;
		else if (operation.equals(NfvoLcmOperationType.DAY2_CONFIG)) changeType=NetworkServiceStatusChange.DAY2_CONFIGURATION_EXECUTED;
		else if (operation.equals(NfvoLcmOperationType.DAY2_RUN)) changeType=NetworkServiceStatusChange.DAY2_RUN_EXECUTED;
		else if (operation.equals(NfvoLcmOperationType.DAY2_CLEAN)) changeType=NetworkServiceStatusChange.DAY2_CLEAN_EXECUTED;
		return changeType;
	}
}

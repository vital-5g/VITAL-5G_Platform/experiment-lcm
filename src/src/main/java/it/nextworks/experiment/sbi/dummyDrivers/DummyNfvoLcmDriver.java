package it.nextworks.experiment.sbi.dummyDrivers;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import it.nextworks.experiment.configuration.ConfigurationParameters;
import it.nextworks.experiment.rabbit.CleaningResultInternalMessage;
import it.nextworks.experiment.rabbit.ConfigurationResultInternalMessage;
import it.nextworks.experiment.rabbit.InternalMessage;
import it.nextworks.experiment.sbi.enums.ConfigurationStatus;
import it.nextworks.experiment.sbi.interfaces.ConfiguratorServiceProviderInterface;
import it.nextworks.experiment.sbi.interfaces.ExecutorServiceProviderInterface;
import it.nextworks.experiment.sbi.interfaces.model.NfvoLcmActionExecution;
import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.exceptions.FailedOperationException;
import it.nextworks.lcm.vs.interfaces.nfvo.NsLcmProviderInterface;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.CreateNsIdentifierRequestInternal;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.InstantiateNsRequestInternal;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.OperationStatus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class DummyNfvoLcmDriver implements NsLcmProviderInterface, ConfiguratorServiceProviderInterface, ExecutorServiceProviderInterface {
    private static final Logger log = LoggerFactory.getLogger(DummyNfvoLcmDriver.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    @Qualifier(ConfigurationParameters.elcmQueueExchange)
    private TopicExchange messageExchange;

    @Override 
    public String getNfvoVmVimReference(String nsInstanceId, String vnfReference, Testbed testbed) throws FailedOperationException {
        return new String();
    }

    @Override
    public String createNetworkServiceIdentifier(CreateNsIdentifierRequestInternal request) throws FailedOperationException {
        log.debug("Received create ns identifier request");
        return UUID.randomUUID().toString();
    }

    @Override
    public String instantiateNetworkService(InstantiateNsRequestInternal request) throws FailedOperationException {
        log.debug("Received  request");
        return UUID.randomUUID().toString();
    }

    @Override
    public OperationStatus getOperationStatus(String operationId, Testbed testbed) throws FailedOperationException {
        return OperationStatus.SUCCESSFULLY_DONE;
    }

    @Override
    public Map<String, String> getNetworkServiceInstanceEndpoints(String nsInstanceId, Testbed tesbed) throws FailedOperationException {
        return new HashMap<>();
    }

    @Override
    public String terminateNetworkService(String networkServiceInstanceId, Testbed testbed) throws FailedOperationException {
        return UUID.randomUUID().toString();
    }

    @Override
    public String executeDay2Action(String nsInstanceId, String actionId, String vnfProfileId, Map<String, String> inputParams, Testbed testbed) throws FailedOperationException {
        return UUID.randomUUID().toString();
    }

    private ObjectMapper buildObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }

    private void sendMessageToQueue(InternalMessage msg, String topic) throws JsonProcessingException {
        ObjectMapper mapper = buildObjectMapper();
        String json = mapper.writeValueAsString(msg);
        rabbitTemplate.convertAndSend(messageExchange.getName(), topic, json);
    }


    private void manageConfigurationError(String errorMessage, String executionId){
        log.error("Configuration of Experiment Execution with Id {} failed : {}", executionId, errorMessage);
        errorMessage = String.format("Configuration of Experiment Execution with Id %s failed : %s", executionId, errorMessage);
        String topic = "lifecycle.configurationResult." + executionId;
        InternalMessage internalMessage = new ConfigurationResultInternalMessage(ConfigurationStatus.FAILED, errorMessage, null, true);
        try {
            sendMessageToQueue(internalMessage, topic);
        } catch (JsonProcessingException e) {
            log.error("Error while translating internal scheduling message in Json format");
            log.debug(null, e);
        }
    }
    @Override
    public void applyConfiguration(String executionId, NfvoLcmActionExecution actionExecution) {
        String result = "OK";
        String topic = "lifecycle.configurationResult." + executionId;
        InternalMessage internalMessage = new ConfigurationResultInternalMessage(ConfigurationStatus.CONFIGURED, result, "configId",false);
        try {
            sendMessageToQueue(internalMessage, topic);
        } catch (JsonProcessingException e) {
            log.error("Error while translating internal scheduling message in Json format");
            manageConfigurationError("Error while translating internal scheduling message in Json format", executionId);
        }
    }

    @Override
    public void resetConfiguration(String executionId, NfvoLcmActionExecution actionExecution) {
        String result = "OK";
        String topic = "lifecycle.cleaningresult." + executionId;
        InternalMessage internalMessage = new CleaningResultInternalMessage();
        try {
            sendMessageToQueue(internalMessage, topic);
        } catch (JsonProcessingException e) {
            log.error("Error while translating internal scheduling message in Json format");
            manageConfigurationError("Error while translating internal scheduling message in Json format", executionId);
        }

    }

    @Override
    public void runTestCase(String executionId, NfvoLcmActionExecution actionExecution) {

    }

    @Override
    public void abortTestCase(String executionId, String tcDescriptorId) {

    }
}

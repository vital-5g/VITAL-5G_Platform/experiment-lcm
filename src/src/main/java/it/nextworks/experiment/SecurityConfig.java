package it.nextworks.experiment;

import it.nextworks.experiment.keycloak.CustomKeycloakSpringBootConfigResolver;
import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.springboot.KeycloakSpringBootProperties;
import org.keycloak.adapters.springsecurity.KeycloakConfiguration;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

@KeycloakConfiguration
class SecurityConfig extends KeycloakWebSecurityConfigurerAdapter {



    @Value("${auth.admin-role:Platform_admin}")
    private String adminRole;

    @Value("${auth.technician-role:Platform_technician}")
    private String technicianRole;

    @Value("${auth.netapp-developer-role:NetApp_Developer}")
    private String netAppDeveloperRole;

    @Value("${auth.vertical-service-provider-role:Vertical_Service_Provider}")
    private String vspRole;

    @Value("${auth.experimenter-role:Experimenter}")
    private String experimenterRole;

    @Value("${auth.testbed-admin-role:Testbed_Admin}")
    private String testbedAdminRole;

    @Value("${auth.platform-module-role:Platform_module}")
    private String platformModuleRole;

    @Value("${auth.site-infrastructure-manager-role:T&L_Site Infrastructure_Manager}")
    private String simRole;


    /**
     * Registers the KeycloakAuthenticationProvider with the authentication manager.
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
        auth.authenticationProvider(keycloakAuthenticationProvider);
    }

    /**
     * Provide a session authentication strategy bean which should be of type
     * RegisterSessionAuthenticationStrategy for public or confidential applications
     * and NullAuthenticatedSessionStrategy for bearer-only applications.
     */
    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
    }

    /**
     * Use properties in application.properties instead of keycloak.json
     */
    @Bean
    @Primary
    public KeycloakConfigResolver keycloakConfigResolver(KeycloakSpringBootProperties properties) {
        return new CustomKeycloakSpringBootConfigResolver(properties);
    }

    /**
     * Secure appropriate endpoints
     */

    /**
     * /portal/elcm/experiment: GET POST
     * /portal/elcm/experiment/{id}: GET, DELETE 
     * /portal/elcm/experiment/{id}/execution: GET, POST
     * /portal/elcm/experiment/{id}/execution/{executionId}: GET 
     * /portal/elcm/experiment/{id}/execution/{executionId}/run: POST
     * /portal/elcm/experiment/{id}/execution/{executionId}/stop: POST
     * 
     */

    /**
     * adminRole, technicianRole, netAppDeveloperRole, vspRole, experimenterRole, testbedAdminRole, platformModuleRole, simRole
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http.csrf()
                .disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/portal/elcm/experiment/").hasAnyAuthority(adminRole, technicianRole, experimenterRole)
                .antMatchers(HttpMethod.POST, "/portal/elcm/experiment/").hasAnyAuthority(adminRole, technicianRole, experimenterRole)

                .antMatchers(HttpMethod.GET, "/portal/elcm/experiment/{id}/").hasAnyAuthority(adminRole, technicianRole, experimenterRole)
                .antMatchers(HttpMethod.DELETE, "/portal/elcm/experiment/{id}/").hasAnyAuthority(adminRole, technicianRole, experimenterRole)

                .antMatchers(HttpMethod.GET, "/portal/elcm/experiment/{id}/execution/").hasAnyAuthority(adminRole, technicianRole, experimenterRole)
                .antMatchers(HttpMethod.POST, "/portal/elcm/experiment/{id}/execution/").hasAnyAuthority(adminRole, technicianRole, experimenterRole)

                .antMatchers(HttpMethod.GET, "/portal/elcm/experiment/{id}/execution/{executionId}/").hasAnyAuthority(adminRole, technicianRole, experimenterRole)

                .antMatchers(HttpMethod.POST, "/portal/elcm/experiment/{id}/execution/{executionId}/run").hasAnyAuthority(adminRole, technicianRole, experimenterRole)

                .antMatchers(HttpMethod.POST, "/portal/elcm/experiment/{id}/execution/{executionId}/stop").hasAnyAuthority(adminRole, technicianRole, experimenterRole)

                .anyRequest().authenticated()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
}
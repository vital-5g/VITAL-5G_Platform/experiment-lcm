package it.nextworks.experiment.rabbit;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.squareup.okhttp.internal.Internal;

public class ConfigureExperimentInternalMessage extends InternalMessage {
    @JsonCreator
    public ConfigureExperimentInternalMessage() { this.type = InternalMessageType.CONFIGURE; }
}

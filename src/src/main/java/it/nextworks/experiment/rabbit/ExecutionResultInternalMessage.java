package it.nextworks.experiment.rabbit;

import com.fasterxml.jackson.annotation.JsonCreator;
import it.nextworks.experiment.sbi.enums.ExecutionResult;

public class ExecutionResultInternalMessage extends InternalMessage{

    @JsonCreator
    public ExecutionResultInternalMessage() { this.type = InternalMessageType.EXECUTION_RESULT; }
}

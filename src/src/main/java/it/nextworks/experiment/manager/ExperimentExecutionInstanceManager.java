package it.nextworks.experiment.manager;

import java.io.IOException;
import java.util.*;

import it.nextworks.catalogue.elements.tc.NfvoLcmAction;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.exceptions.UnAuthorizedRequestException;
import it.nextworks.experiment.rabbit.*;
import it.nextworks.experiment.sbi.ConfiguratorService;
import it.nextworks.experiment.sbi.interfaces.model.NfvoLcmActionExecution;
import it.nextworks.experiment.sbi.mano.NfvoLcmService;
import it.nextworks.lcm.vs.elements.Testbed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import it.nextworks.catalogue.elements.VsDescriptor;
import it.nextworks.catalogue.elements.exp.ExpBlueprint;
import it.nextworks.catalogue.elements.exp.ExpDescriptor;
import it.nextworks.catalogue.elements.tc.TestCaseBlueprint;
import it.nextworks.experiment.model.ExecutionStatus;
import it.nextworks.experiment.model.ExperimentExecution;
import it.nextworks.experiment.model.ExperimentLcm;
import it.nextworks.experiment.model.ExperimentStatus;
import it.nextworks.experiment.repository.ExperimentExecutionRepository;
import it.nextworks.experiment.repository.ExperimentLcmRepository;
//import it.nextworks.experiment.sbi.catalogue.api.ExpdCatalogueApiApi;
import it.nextworks.experiment.sbi.enums.ConfigurationStatus;
import it.nextworks.experiment.sbi.interfaces.ConfiguratorServiceProviderInterface;
import it.nextworks.experiment.sbi.interfaces.OpenOnlineCatalogueProviderInterface;
import it.nextworks.experiment.sbi.interfaces.SiteOrchestratorProviderInterface;
import it.nextworks.experiment.sbi.interfaces.ValidatorServiceProviderInterface;
import it.nextworks.experiment.sbi.interfaces.VerticalServiceLcmProviderInterface;
import it.nextworks.experiment.sbi.monitoring.MonitoringService;
import it.nextworks.lcm.vs.elements.VerticalServiceInstance;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.NotExistingEntityException;

public class ExperimentExecutionInstanceManager {

    private static final Logger log = LoggerFactory.getLogger(ExperimentExecutionInstanceManager.class);

    private String executionId;
    private String experimentId;
    private ExecutionStatus currentState;

    private VerticalServiceLcmProviderInterface vslcmService;
    private OpenOnlineCatalogueProviderInterface catalogueService;
    private SiteOrchestratorProviderInterface siteOrchestratorService;
    private MonitoringService monService;
    private NfvoLcmService nfvoLcmService;
    private ValidatorServiceProviderInterface validatorService;
    private ConfiguratorService configurationService;

    private ExperimentLcmRepository elcmRepository;
    private ExperimentExecutionRepository experimentExecutionRepository;

    private VerticalServiceInstance verticalServiceInstance;

    private VsDescriptor verticalServiceDescriptor;

    private ExpDescriptor experimentDescriptor;

    private ExpBlueprint experimentBlueprint;

    private TestCaseBlueprint testCaseBlueprint;

    public ExperimentExecutionInstanceManager(String executionId,
    										  String experimentId,
    										  VerticalServiceLcmProviderInterface vsService,
    										  OpenOnlineCatalogueProviderInterface catalogueService,
    										  SiteOrchestratorProviderInterface siteOrchestratorService,
    										  MonitoringService monService,
    										  ValidatorServiceProviderInterface validatorService,
    										  ConfiguratorService configurationService,
    										  ExperimentExecutionRepository experimentExecutionRepository,
                                              NfvoLcmService nfvoLcmService,
    										  ExperimentLcmRepository elcmRepository,
                                              VerticalServiceInstance vsi,
                                              VsDescriptor vsd,
                                              ExpDescriptor expd,
                                              ExpBlueprint expb,
                                              TestCaseBlueprint tcb
    										  ) throws NotExistingEntityException, FailedOperationException {
        Optional<ExperimentExecution> experimentExecutionOptional = experimentExecutionRepository.findByExecutionId(executionId);
        if(!experimentExecutionOptional.isPresent())
            throw new NotExistingEntityException(String.format("Experiment Execution with Id %s not found", executionId));

        this.executionId = executionId;
        this.experimentId = experimentId;
        this.catalogueService = catalogueService;
        this.vslcmService = vsService;
        this.siteOrchestratorService = siteOrchestratorService;
        this.monService = monService;
        this.validatorService = validatorService;
        this.configurationService = configurationService;
        this.experimentExecutionRepository = experimentExecutionRepository;
        this.elcmRepository = elcmRepository;
        this.verticalServiceInstance = vsi;
        this.verticalServiceDescriptor = vsd;
        this.experimentDescriptor = expd;
        this.experimentBlueprint = expb;
        this.testCaseBlueprint = tcb;
        this.nfvoLcmService = nfvoLcmService;

        this.currentState = experimentExecutionOptional.get().getState();
        //Retrieve again all information for stored experiment executions
        //if(!this.currentState.equals(ExecutionStatus.CREATED)) {
        //    try {
        //        retrieveAllInformation();
        //    }catch (FailedOperationException e){
        //        log.error(e.getMessage());
        //        manageExperimentExecutionError(e.getMessage());
        //    }
        //}
        //Restart experiment executions based on the current state
        // TODO: INvestigate restart experiment
        //switch(currentState){
        //    case CONFIGURING:
        //        configurationService.applyConfiguration(executionId, );
        //        break;
        //    case RUNNING:
        //        runExperimentExecution(executionId);
        //        break;
        //    case VALIDATING:
        //        validatorService.startValidation(experimentId, executionId);
        //        break;
        //    case CLEANING:
        //    	performCleaning(executionId);
        //    	break;
        //    case ROLLING_BACK:
        //    	// doRollingBack()
        //    	break;
        //    case TERMINATING:
        //    	runTermination(executionId);
        //    default:
        //        log.debug("There aren't pending operations for Experiment Execution with Id {}", executionId);
        //}
    }

    private void runExperimentExecution(String executionId) {
    	log.debug("Execution for experiment id {} is still running", executionId);
    }

    private void runTermination(String executionId) {
    	//TODO
    }

    /**
     * Method used to receive messages about experiment execution LCM from the Rabbit MQ
     *
     * @param message received message
     */
    public void receiveMessage(String message) {
        log.info("Received message for Experiment Execution {}", executionId);
        log.debug(message);
        try {
            ObjectMapper mapper = Jackson2ObjectMapperBuilder.json()
                    .modules(new JavaTimeModule())
                    .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE)
                    .build();
            InternalMessage im = mapper.readValue(message, InternalMessage.class);
            InternalMessageType imt = im.getType();

            switch (imt) {
                //Messages from NBI
                case CONFIGURE:
                    log.debug("Processing request to configure Experiment Execution with id {}", executionId);
                    processConfigureRequest();
                    break;
                case RUN: {
                    log.debug("Processing request to run Experiment Execution with Id {}", executionId);
                    processRunRequest();
                    break;
                }
                case EXECUTION_RESULT:
                    log.debug("Processing execution Result of Experiment Execution with Id {}", executionId);
                    processExecutionResultRequest();
                    break;
                case STOP: {
                    log.debug("Processing request to stop Experiment Execution with Id {}", executionId);
                    processStopRequest();
                    break;
                }
                case VALIDATION_RESULT: {
                    ValidationResultInternalMessage msg = (ValidationResultInternalMessage) im;
                    log.debug("Processing validation result of Experiment Execution with Id {}", executionId);
                    processValidationResult(msg);
                    break;
                }
                case CONFIGURATION_RESULT: {
                    ConfigurationResultInternalMessage msg = (ConfigurationResultInternalMessage) im;
                    log.debug("Processing configuration result of Experiment Execution with Id {}", executionId);
                    processConfigurationResult(msg);
                    break;
                }
                case CLEANING_RESULT: {
                    //CleaningResultInternalMessage msg = (CleaningResultInternalMessage) imt;
                    log.debug("Processing cleaning result of Experiment Execution with Id {}", executionId);
                    processCleaningResult();
                    break;
                }
                default:
                    log.error("Received message with not supported type. Skipping");
                    break;
            }
        } catch (JsonParseException e) {
            log.debug("Error while parsing message", e);
            manageExperimentExecutionError("Error while parsing message: " + e.getMessage());
        } catch (JsonMappingException e) {
            log.debug("Error in Json mapping", e);
            manageExperimentExecutionError("Error in Json mapping: " + e.getMessage());
        } catch (IOException e) {
            log.debug("IO error when receiving json message", e);
            manageExperimentExecutionError("IO error when receiving json message: " + e.getMessage());
        } catch (Exception e){
            log.debug("Unhandled Exception", e);
            manageExperimentExecutionError("Generic internal error: " + e.getMessage());
        }
    }

    private void processExecutionResultRequest() {
        log.debug("Experiment running executed");
        if(updateAndNotifyExperimentExecutionState(ExecutionStatus.VALIDATING)) {
            validatorService.terminateExperiment(experimentId, executionId);
        }
    }

    private void processCleaningResult() {
        if(updateAndNotifyExperimentExecutionState(ExecutionStatus.TERMINATED)) {
            log.debug("Experiment execution with id {} has been cleaned, switching to TERMINATED state", executionId);
            Optional<ExperimentLcm> expLcmOpt = elcmRepository.findByElcmId(experimentId);
            ExperimentLcm expLcm = expLcmOpt.get();
            expLcm.setStatus(ExperimentStatus.TERMINATED);
            elcmRepository.saveAndFlush(expLcm);
        }
    }

    private void performCleaning(String executionId) {
        log.info("Cleaning experiment execution for experiment execution id {}", executionId);
        NfvoLcmAction resetAction = testCaseBlueprint.getResetAction();
        if (resetAction == null|| resetAction.getNfvoActionId()==null) {
            log.info("TCB without reset action, skipping this step");
            processCleaningResult();
        }
        log.info("Attempting to execute executionAction {} with id {}", resetAction.getNfvoReferenceId(), resetAction.getNfvoActionId());
        Map<String, String> inputParameters = experimentDescriptor.getResetActionParameters();
        NfvoLcmActionExecution nfvoLcmActionExecution = new NfvoLcmActionExecution(resetAction.getNfvoReferenceId(), inputParameters, resetAction.getNfvoActionId());
        configurationService.resetConfiguration(executionId, nfvoLcmActionExecution);
    }

    private void processValidationResult(ValidationResultInternalMessage msg){
        if(msg.isFailed()) {
            manageExperimentExecutionError(msg.getResult());
            return;
        }
        switch (msg.getValidationStatus()){
            case CONFIGURED:
                log.info("Validator configured for Experiment Execution with Id {}", executionId);
                processConfigurationResult(new ConfigurationResultInternalMessage(ConfigurationStatus.CONFIGURED, "Validation configured", null,false));
                break;
            case ACQUIRING: case VALIDATING:
                validatorService.queryValidationResult(experimentId, executionId);
                break;
            case VALIDATED:
                Optional<ExperimentExecution> experimentExecutionOptional = experimentExecutionRepository.findByExecutionId(executionId);
                if(!experimentExecutionOptional.isPresent()){
                    log.error("ELCM: Experiment Execution with Id {} not found", executionId);
                    return;
                }
                ExperimentExecution experimentExecution = experimentExecutionOptional.get();
                experimentExecution.setReportUrl(msg.getResult());
                experimentExecutionRepository.saveAndFlush(experimentExecution);
                if(this.currentState.equals(ExecutionStatus.FAILED)){
                    log.error("VALIDATION FAILED");
                    //TODO what should I have to do?
                    //validatorService.terminateExperiment(experimentId, executionId);
                }else{
                    //Run another test case if run type is RUN_ALL
                    if(updateAndNotifyExperimentExecutionState(ExecutionStatus.CLEANING))
                    	performCleaning(executionId);
                }
        }
    }

    private void processRunRequest() throws NotExistingEntityException, FailedOperationException {
        log.info("Running Experiment Execution with Id {}", executionId);
        NfvoLcmAction executionAction = testCaseBlueprint.getExecutionAction();
        if (executionAction == null) {
            log.info("TCB Without executeaction, waiting for REST Notification for execution completition");
            updateAndNotifyExperimentExecutionState(ExecutionStatus.RUNNING_MANUAL);
        } else {
            if(updateAndNotifyExperimentExecutionState(ExecutionStatus.RUNNING)) {
                log.info("Attempting to execute executionAction {} with id {}", executionAction.getNfvoReferenceId(), executionAction.getNfvoActionId());
                log.debug("My Configuration Service is {}", configurationService.toString());
                Map<String, String> inputParameters = experimentDescriptor.getExecutionActionParameters();
                NfvoLcmActionExecution nfvoLcmActionExecution = new NfvoLcmActionExecution(executionAction.getNfvoReferenceId(), inputParameters, executionAction.getNfvoActionId());
                configurationService.runTestCase(executionId, nfvoLcmActionExecution);
            }
        }
    }

    private void processConfigureRequest() throws NotExistingEntityException, FailedOperationException {
    	if(updateAndNotifyExperimentExecutionState(ExecutionStatus.CONFIGURING)) {
            log.info("Configuring Experiment Execution with Id {}", executionId);
            Optional<ExperimentLcm> expLcmOpt = elcmRepository.findByElcmId(experimentId);
    		ExperimentLcm expLcm = expLcmOpt.get();
            NfvoLcmAction configurationAction = testCaseBlueprint.getConfigurationAction();
            if (configurationAction == null || configurationAction.getNfvoActionId()==null) {
                log.info("TCB without configuration action, skipping this step");
                processConfigurationResult(new ConfigurationResultInternalMessage(ConfigurationStatus.CONFIGURED, "Skipped", "configId",false));
            }
            log.info("Attempting to execute configurationAction {} with id {}", configurationAction.getNfvoReferenceId(), configurationAction.getNfvoActionId());
    		expLcm.setStatus(ExperimentStatus.LOCKED);
    		elcmRepository.saveAndFlush(expLcm);
            Map<String, String> inputParameters = experimentDescriptor.getConfigActionParameters();
            NfvoLcmActionExecution nfvoLcmActionExecution = new NfvoLcmActionExecution(configurationAction.getNfvoReferenceId(), inputParameters, configurationAction.getNfvoActionId());

            configurationService.applyConfiguration(executionId, nfvoLcmActionExecution);
        }
    }

    private void processStopRequest(){
    	if(currentState.equals(ExecutionStatus.RUNNING)) {
			log.debug("Experiment execution with di {} has stopped it's execution and it's starting the validation process", executionId);
    		updateAndNotifyExperimentExecutionState(ExecutionStatus.VALIDATING);
    		validatorService.startValidation(experimentId, executionId);
    	}
    }
    private void processConfigurationResult(ConfigurationResultInternalMessage msg){
        if(msg.isFailed()) {
            manageExperimentExecutionError(msg.getResult());
            return;
        }
        validatorService.configureExperiment(experimentId, executionId, true, verticalServiceInstance.getNetworkServiceInstanceId(), verticalServiceInstance, experimentDescriptor.getExpDescriptorId().toString());
    }

    private void retrieveAllInformation() throws FailedOperationException, it.nextworks.catalogue.exceptions.FailedOperationException, it.nextworks.catalogue.exceptions.NotExistingEntityException, MalformattedElementException, UnAuthorizedRequestException {
        log.info("Retrieving all the information for Experiment Execution with Id {}", executionId);
        Optional<ExperimentExecution> experimentExecutionOptional = experimentExecutionRepository.findByExecutionId(executionId);
        if(!experimentExecutionOptional.isPresent())
            throw new FailedOperationException(String.format("Experiment Execution with Id %s not found", executionId));
        ExperimentExecution experimentExecution = experimentExecutionOptional.get();
        ExperimentLcm elcmExp = elcmRepository.findByElcmId(experimentExecution.getExperimentLcmId()).get();
        log.debug("Trying to get VSI {}", elcmExp.getVsId());
        // GET VSI
        VerticalServiceInstance vsi = vslcmService.getVerticalServiceInstance(elcmExp.getVsId());
        if (vsi == null) {
        	throw new FailedOperationException(String.format("Vertical Service Instance with Id %s not found", elcmExp.getVsId()));
        }
        log.debug("Trying to get VSD_ID {}", vsi.getVsdId());
        // Get VSD_ID
        VsDescriptor vsd = catalogueService.getVsd(vsi.getVsdId());
        if (vsd == null) {
        	throw new FailedOperationException(String.format("Vertical Service Instance with Id %s not found", elcmExp.getVsId()));
        }
        // GET EXPD
        ExpDescriptor expd = catalogueService.getExpDescriptor(UUID.fromString(elcmExp.getExperimentDescriptorId()));
        if (expd == null ){
        	throw new FailedOperationException(String.format("Experiment Descriptor with Id %s not found", elcmExp.getExperimentDescriptorId()));
        }
        
        // GET EXPB
        ExpBlueprint expb = catalogueService.getExperimentBlueprint(UUID.fromString(expd.getExpBlueprintId().toString()));
        if (expb == null ){
        	throw new FailedOperationException(String.format("Experiment Blueprint with Id %s not found", expd.getExpBlueprintId().toString()));
        }
        
    }

    private void manageExperimentExecutionError(String errorMessage){
        log.error("Experiment Execution with Id {} failed : {}", executionId, errorMessage);
        Optional<ExperimentExecution> experimentExecutionOptional = experimentExecutionRepository.findByExecutionId(executionId);
        experimentExecutionOptional.ifPresent(experimentExecution -> experimentExecutionRepository.saveAndFlush(experimentExecution.errorMessage(errorMessage)));
    }

    private boolean updateAndNotifyExperimentExecutionState(ExecutionStatus newState){
    	ExecutionStatus previousState = this.currentState;
        this.currentState = newState;
        log.info("Attempting to change the state from {} to {}", previousState, newState);
        if(!isStateChangeAllowed(previousState, newState)) {
            log.info("State change from {} to {} not allowed. Skipping", previousState, newState);
            return false;
        }
        Optional<ExperimentExecution> experimentExecutionOptional = experimentExecutionRepository.findByExecutionId(executionId);
        experimentExecutionOptional.ifPresent(experimentExecution -> experimentExecutionRepository.saveAndFlush(experimentExecution.state(currentState)));
        return true;
    }

    private boolean isStateChangeAllowed(ExecutionStatus currentState, ExecutionStatus newState){
        //Map allowed state change
        switch (newState){
        	case CREATED:
        		return currentState.equals(ExecutionStatus.TERMINATED);
        	case CONFIGURING:
        		return currentState.equals(ExecutionStatus.CREATED) ||
                        currentState.equals(ExecutionStatus.TERMINATED);
        	case RUNNING:
            case RUNNING_MANUAL:
                return currentState.equals(ExecutionStatus.CONFIGURING);
        	case VALIDATING:
                return currentState.equals(ExecutionStatus.RUNNING) ||
                        currentState.equals(ExecutionStatus.RUNNING_MANUAL);
        	case CLEANING:
        		return currentState.equals(ExecutionStatus.VALIDATING);
	        case TERMINATED:
	        	return currentState.equals(ExecutionStatus.CLEANING);
	        case FAILED:
	        	return currentState.equals(ExecutionStatus.ROLLING_BACK) ||
	        			currentState.equals(ExecutionStatus.CLEANING);
            default:
                log.debug("New state not recognized");
                return false;
        }
    }
}

package it.nextworks.experiment.nbi;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import it.nextworks.experiment.coordinator.ElcmService;
import it.nextworks.experiment.model.*;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.FailedOperationException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.MalformattedElementException;
import it.nextworks.nfvmano.libs.ifa.common.exceptions.NotExistingEntityException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-03-27T16:25:04.500Z[GMT]")
@RestController
@CrossOrigin
public class ElcmApiController implements ElcmApi {

    private static final Logger log = LoggerFactory.getLogger(ElcmApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;
    
    @Autowired
    private ElcmService elcmService;

    @Autowired
    public ElcmApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    // Create a new experiment LCM
    public ResponseEntity<String> createExperiment(@Parameter(in = ParameterIn.DEFAULT, description = "Experiment Request parameters", required=true, schema=@Schema()) @Valid @RequestBody ExperimentInstanceRequest body) {
    	log.info("Requested creation of new ExperimentLCM {}", body.toString());
    	try {
			String response = elcmService.createExperimentLcmInstance(body);
        	log.debug("New Experiment LCM created with id {}", response);
            return new ResponseEntity<String>(response, HttpStatus.CREATED);
        } catch (FailedOperationException e) {
        	log.error("Failed to create new experimentLCM: {}", e.getMessage());
        	return new ResponseEntity<String>(HttpStatus.CONFLICT);
		}
    }
    
    // Create new experiment execution 
    public ResponseEntity<String> createExperimentExecution(@Parameter(in = ParameterIn.PATH, description = "Experiment identifier", required=true, schema=@Schema()) @PathVariable("id") String id,@Parameter(in = ParameterIn.DEFAULT, description = "Experiment Request parameters", required=true, schema=@Schema()) @Valid @RequestBody ExperimentExecutionRequest body) {
    	log.info("Requested creation of new ExperimentLCM {}", body.toString());
    	try {
			String response = elcmService.createExperimentExecutionInstance(id, body);
			return new ResponseEntity<String>(response, HttpStatus.CREATED);
		} catch (FailedOperationException e) {
			log.error("Failed to create new experiment execution: {}", e.getMessage());
        	return new ResponseEntity<String>(HttpStatus.CONFLICT);
		} catch (NotExistingEntityException e) {
			log.error("Failed to create new experiment execution. ExperimentLCM with id {} not found: {}", id, e.getMessage());
        	return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
    }

    // Terminate an experiment LCM 
    public ResponseEntity<Void> terminateExperiment(@Parameter(in = ParameterIn.PATH, description = "Experiment Instance identifier", required=true, schema=@Schema()) @PathVariable("id") String id, @RequestParam(required = false) Boolean Force) {
    	log.info("Requested termination of ExperimentLCM {}", id);
        try {
			elcmService.terminateExperimentLcmInstance(id, Force);
			log.debug("ExperimentLCM with id {} terminated", id);
	        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} catch (NotExistingEntityException e) {
			log.error("ExperimentLCM with id {} not found: ErrorMessage:", id, e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} catch (FailedOperationException e) {
			log.error("ExperimentLCM with id {} cannot be terminated. Active executions are still running: ErrorMessage:", id, e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
    }

	// Delete an experiment LCM
	public ResponseEntity<Void> deleteExperiment(@Parameter(in = ParameterIn.PATH, description = "Experiment Instance identifier", required=true, schema=@Schema()) @PathVariable("id") String id) {
    	log.info("Delete an ExperimentLCM {}", id);
        try {
			elcmService.deleteExperimentLcmInstance(id);
			log.debug("ExperimentLCM with id {} terminated", id);
	        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} catch (NotExistingEntityException e) {
			log.error("ExperimentLCM with id {} not found: ErrorMessage:", id, e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} catch (FailedOperationException e) {
			log.error("ExperimentLCM with id {} cannot be deleted. Active executions are still running: ErrorMessage:", id, e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
    }

    // GET EXPERIMENT EXECUTION 
    public ResponseEntity<ExperimentExecution> getExperimentExecution(@Parameter(in = ParameterIn.PATH, description = "id", required=true, schema=@Schema()) @PathVariable("id") String id,@Parameter(in = ParameterIn.PATH, description = "Execution id", required=true, schema=@Schema()) @PathVariable("executionId") String executionId) {
    	log.info("Requested experiment execution with id {}", executionId);
    	try{
    		ExperimentExecution response = elcmService.getExperimentExecutionInstance(id, executionId);
    		log.debug("Found execution with id {} - {}", executionId, response.toString());
    		return new ResponseEntity<ExperimentExecution>(response, HttpStatus.OK);
    	} catch (NotExistingEntityException e) {
    		log.error("Cannot Found execution id {} - Error message {}", executionId, e.getMessage());
    		return new ResponseEntity<ExperimentExecution>(HttpStatus.NOT_FOUND);
    	}
    }

    // STOP EXPERIMENT EXECUTION
    public ResponseEntity<Void> stopExperimentExecution(@Parameter(in = ParameterIn.PATH, description = "id", required=true, schema=@Schema()) @PathVariable("id") String id,@Parameter(in = ParameterIn.PATH, description = "Execution id", required=true, schema=@Schema()) @PathVariable("executionId") String executionId) {
        log.info("Requested to STOP execution for expExecution id {}", executionId);
        try {
			elcmService.stopExperimentExecution(executionId);
			log.debug("Execution stopped for experiment execution id {}", executionId);
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} catch (FailedOperationException e) {
			log.debug("Stopping execution for experiment execution id {} failed. Error message: ", executionId, e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} catch (NotExistingEntityException e) {
			log.debug("Execution experiment with id {} not found. Error message: ", executionId, e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
    }

    // RUN EXECUTION
    public ResponseEntity<Void> runExperimentExecution(@Parameter(in = ParameterIn.PATH, description = "id", required=true, schema=@Schema()) @PathVariable("id") String id,@Parameter(in = ParameterIn.PATH, description = "Execution id", required=true, schema=@Schema()) @PathVariable("executionId") String executionId) {
    	log.info("Requested to run expExecution with id {} with parameters: {}", executionId, request.toString());
    	try {
			elcmService.runExperimentExecution(executionId);
			log.debug("Execution started for experiment execution id {}", executionId);
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} catch (FailedOperationException e) {
			log.error("Run action for experiment execution id {} failed. Error message: ", executionId, e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		} catch (NotExistingEntityException e) {
			log.error("Experiment execution with id {} does not exist. Error message: ", executionId, e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} catch (MalformattedElementException e) {
			log.error("Error while translating internal scheduling message in Json format");
			return new ResponseEntity<Void>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
		}
    }

	@Override
	public ResponseEntity<Void> ExperimentExecutionCompleted(String id, String executionId) {
		log.info("Requested to notify expExecution completition with id {} with parameters: {}", executionId, request.toString());
		try {
			elcmService.ExperimentExecutionCompleted(executionId);
			log.debug("Execution completition notified for experiment execution id {}", executionId);
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		} catch (FailedOperationException e) {
			log.error("Run action for experiment execution id {} failed. Error message: ", executionId, e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		} catch (NotExistingEntityException e) {
			log.error("Experiment execution with id {} does not exist. Error message: ", executionId, e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		} catch (MalformattedElementException e) {
			log.error("Error while translating internal scheduling message in Json format");
			return new ResponseEntity<Void>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
		}
	}

	// Return list of Experiment Executions for a give ExperimentLCM
    public ResponseEntity<List<ExperimentExecution>> getExperimentExecutions(@Parameter(in = ParameterIn.PATH, description = "Experiment identifier", required=true, schema=@Schema()) @PathVariable("id") String id) {
        log.info("Requested list of experiment executions for experimentLcm identified with id {}", id);
    	
        try {
			List<ExperimentExecution> response = elcmService.getExperimentExecutions(id);
			log.debug("List of Experiment executions for experimentLcm {} - {}", id, response.toString());
			return new ResponseEntity<List<ExperimentExecution>>(response, HttpStatus.OK);
		} catch (NotExistingEntityException e) {
			log.error("Experiment LCM {} not found", id);
			return new ResponseEntity<List<ExperimentExecution>>(HttpStatus.NOT_FOUND);
		}
    }
    
    // Return list of experiments 
    public ResponseEntity<List<ExperimentLcm>> getExperiments(@Parameter(in = ParameterIn.QUERY, description = "Experiment instance identifier" ,schema=@Schema()) @Valid @RequestParam(value = "state", required = false) ExperimentStatus state,@Parameter(in = ParameterIn.QUERY, description = "Testbed where the experiment is running" ,schema=@Schema()) @Valid @RequestParam(value = "testbed", required = false) Testbed testbed,@Parameter(in = ParameterIn.QUERY, description = "Username that has generated the experiment" ,schema=@Schema()) @Valid @RequestParam(value = "user", required = false) String user,@Parameter(in = ParameterIn.QUERY, description = "UseCase name associated to the experiment" ,schema=@Schema()) @Valid @RequestParam(value = "usecase", required = false) String usecase,@Parameter(in = ParameterIn.QUERY, description = "Identifier of the Vertical Service instance" ,schema=@Schema()) @Valid @RequestParam(value = "verticalServiceInstanceId", required = false) String verticalServiceInstanceId) {
        log.info("Requested list of experimentLcms");
    	
        List<ExperimentLcm> response = elcmService.getExperimentLcmsInstances(state, testbed, verticalServiceInstanceId, user, usecase);
        log.debug("List of ExperimentLCMs: {}", response.toString());
		return new ResponseEntity<List<ExperimentLcm>>(response, HttpStatus.OK);
    }

    // Return single experiment
    public ResponseEntity<ExperimentLcm> getExperiment(@Parameter(in = ParameterIn.PATH, description = "Experiment Instance identifier", required=true, schema=@Schema()) @PathVariable("id") String id) {
        log.info("Requested experimentLcm with id {}", id);
        
        try {
			ExperimentLcm response = elcmService.getExperimentLcmInstance(id);
			log.debug("ExperimentLCM found: {}", response.toString());
			return new ResponseEntity<ExperimentLcm>(response, HttpStatus.OK);
		} catch (NotExistingEntityException e) {
			log.error("ExperimentLCM with id {} not found", id);
			return new ResponseEntity<ExperimentLcm>(HttpStatus.NOT_FOUND);
		}
    }

}
